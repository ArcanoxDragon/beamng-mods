local M = {}
M.type = "auxiliary"
M.relevantDevice = nil
M.defaultOrder = 100

-- #region Version Stuff

local function parseVersion(version, name)
	name = name or "version"

	if type(version) ~= "string" then
		error('"' .. name .. '" must be a string')
	end

	local versionTable = {}
	local iPart = 1

	for part in string.gmatch(version, "[^.]+") do
		local partNum = tonumber(part)

		if type(partNum) ~= "number" then
			error('Element "' .. iPart .. '" of "' .. name .. '" is not a number')
		end

		table.insert(versionTable, partNum)
		iPart = iPart + 1
	end

	if #versionTable < 3 then
		for i = 1, 3 - #versionTable do
			table.insert(versionTable, 0)
		end
	end

	if #versionTable > 3 then
		error('"' .. name .. '" must have three or fewer elements')
	end

	return versionTable
end

local function compareVersions(a, b)
	local tA = parseVersion(a, "a")
	local tB = parseVersion(b, "b")

	-- Major differences
	if tA[1] < tB[1] then return -3 end
	if tA[1] > tB[1] then return 3 end

	-- Minor differences
	if tA[2] < tB[2] then return -2 end
	if tA[2] > tB[2] then return 2 end

	-- Revision differences
	if tA[3] < tB[3] then return -1 end
	if tA[3] > tB[3] then return 1 end

	-- Exactly the same
	return 0
end

local function assertVersion(required, current)
	local comparison = compareVersions(current, required)

	return comparison >= 0
end

-- #endregion

local function performVersionCheck()
	for moduleId, descriptor in pairs(M.requiredModules) do
		local moduleName = descriptor.displayName or moduleId
		local minVersion = descriptor.minVersion
		local success, moduleObj = pcall(function() return require(descriptor.modulePath) end)

		if success and moduleObj then
			local currentVersion = moduleObj.version

			if not currentVersion then
				log("W", "arcanoxVersionCheck", 'Warning: Module "' .. moduleName .. '" did not export a version property. Defaulting to "0.0.0".')
				currentVersion = "0.0.0"
			end

			local valid = assertVersion(minVersion, currentVersion)

			if not valid then
				gui.message({
					txt = '<span><span style="color: #ff0; font-weight: bold;">NOTICE:</span> Module "' .. moduleName .. '" is out of date.</span>' .. 
						  '<span>Required version: ' .. minVersion .. '.</span>' .. 
						  '<span>Current version: ' .. currentVersion .. '.</span>' ..
						  '<span>Make sure you have mod auto-updates turned on and that there are no other mods shipping "' .. moduleName .. '" on their own.</span>'
				}, 30, "arcanox.versioncheck." .. moduleName)
			end

			log(
				valid and "I" or "E",
				"arcanoxVersionCheck",
				'Version check for "' .. moduleName .. '" ' .. 
					(valid and "PASSED" or "FAILED") .. " [Min: " .. minVersion .. ", Current: " .. currentVersion .. "]"
			)
		else
			log("E", "arcanoxVersionCheck", 'Error: Could not find module "' .. moduleName .. '" at "' .. descriptor.modulePath .. '" during version check')

			if descriptor.resourceId then
				gui.message({
					txt = '<span><span style="color: #ff0; font-weight: bold;">NOTICE:</span> Module "' .. moduleName .. '" is required, but not installed!</span>' .. 
						'<span>Required version: ' .. minVersion .. '.</span>' ..
						'<span>You can install this mod from the <a href="https-external://beamng.com/resources/' .. descriptor.resourceId .. '/">BeamNG Mod Repository</a>.</span>'
				}, 30, "arcanox.versioncheck." .. moduleName)
			else
				gui.message({
					txt = '<span><span style="color: #ff0; font-weight: bold;">NOTICE:</span> Module "' .. moduleName .. '" is required, but not installed!</span>' .. 
						'<span>Required version: ' .. minVersion .. '.</span>' ..
						'<span>Search for this mod on the <a href="https-external://beamng.com/resources/">BeamNG Mod Repository</a>.</span>'
				}, 30, "arcanox.versioncheck." .. moduleName)
			end
		end
	end

	M.performVersionCheck = nop
end

local function updateGFX()
	M.performVersionCheck()
end

local function init(jbeamData)
	M.requiredModules = {}

	for k, v in pairs(jbeamData) do
		if type(v) == "table" and type(v.modulePath) == "string" and type(v.minVersion) == "string" then
			local moduleName = k

			M.requiredModules[moduleName] = v
		end
	end

	M.jbeamData = jbeamData
	M.performVersionCheck = performVersionCheck
end

local function reset()
	init(M.jbeamData)
end

M.performVersionCheck = nop
M.updateGFX = updateGFX
M.init = init
M.initSecondStage = nop
M.reset = reset

return M