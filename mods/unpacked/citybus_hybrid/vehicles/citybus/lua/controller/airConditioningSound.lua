local M = {}
M.type = "auxiliary"
M.relevantDevice = nil
M.defaultOrder = 1100

local engine = nil
local sounds = {
	airConditioning = nil
}

local function updateGFX(dt)
	if not sounds.airConditioning then return end

	local power = electrics.values.hybridSystemOn == 1
	local targetVolume = power and M.baseVolume or 0
	local targetPitch = power and M.basePitch or M.basePitch * M.powerOffPitchCoef
	local volume = M.volumeSmoother:get(targetVolume, dt)
	local pitch = M.pitchSmoother:get(targetPitch, dt)

	obj:setVolumePitch(sounds.airConditioning, volume, pitch)
end

local function init(jbeamData)
	M.baseVolume = jbeamData.baseCompressorVolume or 1.0
	M.basePitch = jbeamData.baseCompressorPitch or 1.0
	M.powerOffPitchCoef = jbeamData.powerOffPitchCoef or 0.75
	M.volumeSmoother = newTemporalSmoothingNonLinear(jbeamData.volumeSmoothing or 2.5)
	M.pitchSmoother = newTemporalSmoothingNonLinear(jbeamData.pitchSmoothing or 2.5)

	if jbeamData.soundNode_nodes and type(jbeamData.soundNode_nodes) == "table" then
		M.soundNode = jbeamData.soundNode_nodes[1]
	end
end

local function initSecondStage()
	engine = powertrain.getDevice("mainEngine")
end

local function initSounds()
	if not engine then return end

	sounds.airConditioning = obj:createSFXSource(
		"vehicles/citybus/sounds/air-compressor.ogg",
		"AudioDefaultLoop3D",
		"airConditioning",
		M.soundNode or engine.engineNodeID
	)
end

M.updateGFX = updateGFX
M.init = init
M.initSecondStage = initSecondStage
M.initSounds = initSounds

return M