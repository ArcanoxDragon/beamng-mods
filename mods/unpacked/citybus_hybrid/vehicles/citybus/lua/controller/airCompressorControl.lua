local M = {}
M.type = "auxiliary"
M.relevantDevice = nil
M.defaultOrder = 1100

local airTank = nil
local compressor = nil

local electricsName = nil

local function updateGFX(dt)
	if type(electrics.values.hybridSystemOn) == "number" and electrics.values.hybridSystemOn < 0.5 then
		electrics.values[electricsName] = 0
	else
		-- Use the compressor's "engaged" state to drive the motor throttle
		electrics.values[electricsName] = compressor.engaged and 1 or 0
	end
end

local function init(jbeamData)
	local airTankName = jbeamData.airTankName or "mainAirTank"

	airTank = energyStorage.getStorage(airTankName)
	if not airTank then
		log("E", "airCompressorControl", ("Air tank not found: %s"):format(airTankName))
		M.updateGFX = nop
		return
	end

	local compressorName = jbeamData.compressorName or "airCompressor"

	compressor = powertrain.getDevice(compressorName)
	if not compressor then
		log("E", "airCompressorControl", ("Compressor not found: %s"):format(compressorName))
		M.updateGFX = nop
		return
	end

	electricsName = jbeamData.electricsName or "airCompressorThrottle"
end

M.updateGFX = updateGFX
M.init = init

return M