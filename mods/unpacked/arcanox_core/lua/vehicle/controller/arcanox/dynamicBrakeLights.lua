-- required modules
local filters = require("arcanox/filters")
local DebugModule = require("arcanox/DebugModule")

local ThresholdGate = filters.ThresholdGate

-- module
local M = {
	type = "auxiliary",
	relevantDevice = nil,
	defaultOrder = 1000,

	-- properties
	jbeamData = nil,
	activeGears = nil,
	lightsGate = nil,
	accelSmoother = nil,
	electricsName = nil,
	electricsOnValue = nil,
}

function M.updateGFX(dt)
	local currentSpeed = electrics.values.wheelspeed
	local acceleration = M.accelSmoother:get(sensors.gy2, dt)

	if M.activeGears[electrics.values.gear] and M.lightsGate:test(acceleration) then
		electrics.values[M.electricsName] = math.max(electrics.values[M.electricsName], M.electricsOnValue)
	end

	if M.debug.enabled then
		M.debug:putValue("Speed (M/S)", currentSpeed)
		M.debug:putValue("Acceleration (M/S/S)", acceleration)
		M.debug:putValue("Latch", M.lightsGate.state)
	end
end

function M.init(jbeamData)
	M.jbeamData = jbeamData

	M.debug = DebugModule:new("brakeLights", jbeamData.enableDebug)

	M.electricsName = jbeamData.electricsName or "brakelights"
	M.electricsOnValue = jbeamData.electricsOnValue or 1

	if type(jbeamData.activeGears) == "string" then
		M.activeGears = {}

		for i = 1, string.len(jbeamData.activeGears) do
			local gear = string.sub(jbeamData.activeGears, i, i)

			M.activeGears[gear] = true
		end
	else
		M.activeGears = {["L"] = true}
	end

	M.lightsGate = ThresholdGate:new(
		jbeamData.decelForBrakeLightsOn  or 1.5,
		jbeamData.decelForBrakeLightsOff or 0.65
	)

	M.accelSmoother = newTemporalSmoothingNonLinear(jbeamData.accelSmoothing or 5.5)
end

return M