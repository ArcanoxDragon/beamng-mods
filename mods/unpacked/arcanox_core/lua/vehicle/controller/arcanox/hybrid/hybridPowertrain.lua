---@class HybridPowertrainController
local M = {}

M.type = "auxiliary"
M.relevantDevice = nil
M.defaultOrder = 1000

local constants = {
	-- Conversion rates
	mpsToMph = 2.23694,
	hpToKw = 0.7457,
	kwToHp = 1.341021,
	torqueToHpCoef = 7120.54016,
	kWhToJ = 3600000,
	sToH = 1.0 / 3600,
	avToRPM = 9.549296596425384,
	rpmToAV = 1.0 / 9.549296596425384,
}

-- lib requires
local mathutil = require("arcanox/mathutil")
local filters = require("arcanox/filters")
local engineutil = require("arcanox/engineutil")
local Pid = require("arcanox/Pid")
local DebugModule = require("arcanox/DebugModule")
local SimpleCurve = require("arcanox/SimpleCurve")
local StateMachine = require("arcanox/StateMachine")

local TimedLatch = filters.TimedLatch
local ThresholdGate = filters.ThresholdGate
local Backlash = filters.Backlash

local min = math.min
local max = math.max
local abs = math.abs
local round = mathutil.round
local maprange = mathutil.maprange
local clamp = mathutil.clamp
local sign = mathutil.sign
local blend = mathutil.blend
local fnum = DebugModule.formatNumber
local withResetMethod = extensions.arcanox_util.withResetMethod

local engine = nil ---@type EngineDevice
local gearbox = nil ---@type EcvtGearbox
local rearMotor = nil ---@type ArcanoxMotor
local battery = nil
local mainTank = nil
local CMU = nil
local brakeHold = nil ---@type BrakeHoldController

-- Drive modes control parameters
local controlParameters = {
	hybrid = {
		throttleCurveExponent = 0.9
	}
}
local initialControlParameters

-- Calculated/initial values
local initialWheelBrakeSplit = {}

--#region State-variable tables

-- Torque data
local torqueData = withResetMethod {
	firstCalculation = true,
	maxElectricTorque = 0,
	fullBatteryElectricTorque = 0,
	maxEngineGeneratorTorque = 0,
	maxEngineOutputTorque = 0,
	maxEngineTorque = 0,
	maxCombinedTorque = 0,
	maxRearAxleTorque = 0,
	engineIdleTorque = 0,
	engineMaxTorqueRpm = 0,
	engineTorqueCurve = {},
	gearboxMaxMG2Torque = 0,
	gearboxMaxMG1TorqueUnchecked = 0,
	gearboxMaxMG2TorqueUnchecked = 0,
	gearboxMaxMG2RegenTorque = 0,
	targetMG1Direction = 1,
	targetMG1Torque = 0,
	targetMG2Torque = 0,
	targetRearMotorTorque = 0,
	requestedEngineTorque = 0,
	requestedTotalOutputTorque = 0,
	requestedElectricOutputTorque = 0,
	initialMaxThrottleMappedTorque = 0,
	maxThrottleMappedTorque = 0,
	gearboxChildGearRatio = 1,
	engineOutputGearRatio = 1,
	engineToSunGearRatio = 1,
	cumulativeMG1GearRatio = 1,
	cumulativeMG2GearRatio = 1,
}

-- Misc. state-related variables
local misc = withResetMethod {
	timeSinceInit = 0,
	timeSincePowerOn = 0,
	gearLockedInPark = false,
	ecvtDriveMode = 0,
	shownDisabledMessage = false,
	watertemp = 0,
	speedMph = 0,
	averageWheelSpeedMph = 0,
	actualSpeedSign = 0,
	desiredSpeedSign = 0,
	brake = 0,
	throttle = 0,
	posThrottle = 0,
	negThrottle = 0,
	throttleAggressiveness = 0,
	creepThrottle = 0,
	gear = "P",
	brakeInput = 0,
	chargeModeEnabled = false,
	isInDriveGear = false,
	isParked = true,
	isStopped = true,
	launchControl = false,
	lastThrottle = 0,
	reducedPropulsion = false,
	inclineAngle = 0,
}

-- Engine demand variables
local engineDemand = withResetMethod {
	isDemanded = false,
	wasDemanded = false,
	reason = "none",
	triggerReason = "none",
	miscDemand = false,
	miscDemandReason = "none",
	waterTempDemand = false, -- TODO: Change to latch?
	autoStopBrakeRequirementSatisfied = false,
}

-- Engine state-related variables
local engineState = withResetMethod {
	current = "stopped",
	last = "stopped",
	runTimeSincePowerOn = 0,
	timeSinceStart = 5,
	timeSinceCanShutOff = 1,
	startupCoef = 0,
	outputTorque = 0,
	combustionTorque = 0,
	loadBiasTorque = 0,
	loadTorque = 0,
	appliedLoadTorque = 0,
	minWorkingRpm = 0,
	minRpm = 0,
	maxRpm = 0,
	minTargetRpm = 0,
	maxTargetRpm = 0,
	targetRpm = 0,
	rpm = 0,
	targetThrottle = 0,
	chargeThrottle = 0,
	startingTime = 0,
	startAttempts = 0,
	hasStartedSinceInit = false,
	powerGenerated = 0,
	isRunning = false,
	isStarting = false,
	isStopping = false,
	starterTorque = 0,
	startupShutdownBrakeHoldTime = 0,
}

-- Braking-related variables
local wheelsWithRegen = {}

local braking = withResetMethod {
	desiredRegen = 0,
	disableRegen = false,
	totalPossibleBrakeTorque = 0,
	totalPropulsedBrakeTorque = 0,
	totalUnpropulsedBrakeTorque = 0,
	currentBrakingCoef = 0,
	lastAverageWheelSpeed = 0,
	timeSinceAppliedBrakes = 0,
	maxRegenTorque = 0,
	uncappedMaxRegenTorque = 0,
	requestedRegenTorqueOnWheels = 0,
	requestedRegenTorque = 0,
	currentFrictionTorque = 0,
	currentRegenTorqueOnWheels = 0,
	neededFrictionTorque = 0,
}

setmetatable(braking, {
	__noreset = {
		totalPossibleBrakeTorque = true,
		totalPropulsedBrakeTorque = true,
		totalUnpropulsedBrakeTorque = true,
	}
})

-- Power-related variables
local power = withResetMethod {
	lastBatteryPower = 0,
	availableMG1Power = 0,
	availableMG2Power = 0,
	availablePriorityEvPower = 0,
	availableSecondaryEvPower = 0,
	requestedChargePower = 0,
	maxBatteryOutputPower = 0,
	availableBatteryPower = 0,
	batteryLevelRaw = 1,
	batteryLevel = 0,
}

-- Debug proxy objects (so updateFixedStep isn't calling DebugModule directly)
local debugValues = withResetMethod {
	requestedEngineOutputTorque = 0,
	engineLagCompensationTorque = 0,
	availablePriorityEvOutputTorque = 0,
	availableSecondaryEvOutputTorque = 0,
	requestedBrakingTorqueOnWheels = 0,
	starterCompensationTorque = 0,
	feedforwardControl = 0,
}

local synergyDebugValues = withResetMethod {
	tO = 0,
	rI = 0,
	rSC = 0,
	crE = 0,
	cr2 = 0,
	av1 = 0,
	av2 = 0,
	pP = 0,
	dP = 0,
	tE = 0,
	t2 = 0,
	clampedT2 = 0,
	t2Delta = 0,
	tEDelta = 0,
	engineOutputTorque = 0,
	electricOutputTorque = 0,
}

--#endregion

-- State machine
local hybridState

---                              ▄██▄
---                              ▀███
---                                 █
---                ▄▄▄▄▄            █
---               ▀▄    ▀▄          █
---           ▄▀▀▀▄ █▄▄▄▄█▄▄ ▄▀▀▀▄  █
---          █  ▄  █        █   ▄ █ █
---          ▀▄   ▄▀        ▀▄   ▄▀ █
---           █▀▀▀            ▀▀▀ █ █
---           █                   █ █
--- ▄▀▄▄▀▄    █  ▄█▀█▀█▀█▀█▀█▄    █ █
--- █▒▒▒▒█    █  █████████████▄   █ █
--- █▒▒▒▒█    █  ██████████████▄  █ █
--- █▒▒▒▒█    █   ██████████████▄ █ █
--- █▒▒▒▒█    █    ██████████████ █ █
--- █▒▒▒▒█    █   ██████████████▀ █ █
--- █▒▒▒▒█   ██   ██████████████  █ █
--- ▀████▀  ██▀█  █████████████▀  █▄█
---   ██   ██  ▀█  █▄█▄█▄█▄█▄█▀  ▄█▀
---   ██  ██    ▀█             ▄▀▓█
---   ██ ██      ▀█▀▄▄▄▄▄▄▄▄▄▀▀▓▓▓█
---   ████        █▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█
---   ███         █▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█
---   ██          █▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█
---   ██          █▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█
---   ██         ▐█▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█
---   ██        ▐█▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█
---   ██       ▐█▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█▌
---   ██      ▐█▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█▌
---   ██     ▐█▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█▌
---   ██    ▐█▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█▌
local function resetAllTheStatefulThings()
	M.driveThrottlePid:reset()
	M.engineStarterPid:reset()

	M.throttleSmoother:reset()
	M.engineStarterSmoother:reset()
	M.engineThrottleSmoother:reset()
	M.mg1MaxPowerSmoother:reset()
	M.mg2MaxPowerSmoother:reset()
	M.priorityPowerSmoother:reset()
	M.engineLoadSmoother:reset()
	M.targetEngineRpmSmoother:set(engine and engine.idleRPM or 0)
	M.engineOutputTorqueSmoother:reset()
	M.engineCombustionTorqueSmoother:reset()
	M.engineStartupSmoother:reset()
	M.inclineAngleSmoother:reset()

	M.latchDriveModeZero:reset()
	M.latchDriveModeOne:reset()
	M.latchCurrentGear:reset()
	M.latchBatteryChargeUp:reset()
	M.latchRegenDisable:reset()
end

local function updateDefaultDebugValues()
	if not M.debug.enabled then
		M.debugInitialized = false
		return
	end

	if not M.debugInitialized then
		debugValues.availablePriorityEvOutputTorque = 0
		debugValues.availableSecondaryEvOutputTorque = 0
		debugValues.requestedBrakingTorqueOnWheels = 0
		debugValues.starterCompensationTorque = 0
		debugValues.feedforwardControl = 0

		-- Put empty strings in all the known debug keys so the order is consistent

		M.debug:clearValues()
		M.debug:putValue("Power", "")
		M.debug:putValue("State (Previous, Current)", "")
		M.debug:putValue("Speed (MPH)", "")
		M.debug:putValue("Incline Angle", "")
		M.debug:putValue("eCVT Drive Mode", "")
		M.debug:putValue("Fuel Remaining (L)", "")
		M.debug:putValue("Engine Run Time", "")
		M.debug:putValue("Engine demanded on", "")
		M.debug:putValue("Engine on reason", "")
		M.debug:putValue("Engine triggered on by", "")
		M.debug:putValue("Battery power (kW)", "")
		M.debug:putValue("Generator power (kW)", "")
		M.debug:putValue("Battery SOC % (raw, usable)", "")
		M.debug:putValue("Input Throttle", "")
		M.debug:putValue("Effective Throttle / Brake", "")
		M.debug:putValue("Throttle % EV thresh.", "")
		M.debug:putValue("Throttle Aggressiveness", "")
		M.debug:putValue("Requested Braking Torque", "")
		M.debug:putValue("Max Regen Torque (Wheels)", "")
		M.debug:putValue("Requested Regen Torque (Wheels)", "")
		M.debug:putValue("Actual Regen Torque (Wheels)", "")
		M.debug:putValue("Friction Braking Torque (Req, Act)", "")
		M.debug:putValue("Total Braking Force (Torque, %)", "")
		M.debug:putValue("Regen % of brake demand", "")
		M.debug:putValue("Friction % of full brakes", "")
		M.debug:putValue("Available Power (Priority/Secondary)", "")
		M.debug:putValue("Available MG1 Power", "")
		M.debug:putValue("Available MG2 Power", "")
		M.debug:putValue("Available EV Torque (Priority/Secondary)", "")
		M.debug:putValue("Available Wheel Torque (Total)", "")
		M.debug:putValue("Requested Wheel Torque (Total, EV, ICE)", "")
		if rearMotor then
			M.debug:putValue("Actual Wheel Torque (Total, EV, ICE)", nil)
			M.debug:putValue("Actual Wheel Torque (Total, EV F, EV R, ICE)", "")
		else
			M.debug:putValue("Actual Wheel Torque (Total, EV, ICE)", "")
			M.debug:putValue("Actual Wheel Torque (Total, EV F, EV R, ICE)", nil)
		end
		M.debug:putValue("Requested Engine Torque", "")
		M.debug:putValue("Actual Engine Torque", "")
		M.debug:putValue("Engine Lag Compensation", "")
		M.debug:putValue("Starter Compensation Torque", "")
		M.debug:putValue("Requested Charge Power (kW)", "")
		M.debug:putValue("Min RPM (Working/Actual)", "")
		M.debug:putValue("Max RPM", "")
		M.debug:putValue("Min Target RPM", "")
		M.debug:putValue("Max Target RPM", "")
		M.debug:putValue("Target RPM", "")
		M.debug:putValue("Actual RPM", "")
		M.debug:putValue("RPM Error", "")
		M.debug:putValue("Engine Load Bias", "")
		M.debug:putValue("Engine Load Torque", "")
		M.debug:putValue("Charge Throttle", "")
		M.debug:putValue("Throttle Feedforward", "")
		M.debug:putValue("Desired Engine Throttle", "")
		M.debug:putValue("Actual Engine Throttle", "")
		M.debug:putValue("Engine Startup Coef", "")
		M.debug:putValue("Engine Engagement Coef", "")
		M.debug:putValue("Engine Ignition Coef", "")
		M.debug:putValue("Engine Load", "")
		M.debug:putValue("Engine Combustion Torque", "")
		M.debug:putValue("Engine Torque", "")
		M.debug:putValue("Engine HP", "")
		M.debug:putValue("Coolant Temp.", "")

		M.synergydebug:clearValues()
		M.synergydebug:putValue("tO - Total Requested Output Torque", "")
		M.synergydebug:putValue("rI - Gearbox:Engine Gear Ratio", "")
		M.synergydebug:putValue("rSC - PSD Sun:Carrier Gear Ratio", "")
		M.synergydebug:putValue("crE - Output:Engine Cumulative Gear Ratio", "")
		M.synergydebug:putValue("cr2 - Output:MG2 Cumulative Gear Ratio", "")
		M.synergydebug:putValue("av1 - MG1 Angular Velocity", "")
		M.synergydebug:putValue("av2 - MG2 Angular Velocity", "")
		M.synergydebug:putValue("pP - Extra EV Power Available", "")
		M.synergydebug:putValue("dP - Desired Net Power (To/From Battery)", "")
		M.synergydebug:putValue("tE - Target Engine Torque", "")
		M.synergydebug:putValue("t2 - Target MG2 Torque", "")
		M.synergydebug:putValue("clampedT2 - Achievable MG2 Torque", "")
		M.synergydebug:putValue("t2Delta - MG2 Torque Deficit", "")
		M.synergydebug:putValue("tEDelta - Engine Compensation Torque", "")
		M.synergydebug:putValue("Engine Output Torque", "")
		M.synergydebug:putValue("Electric Output Torque", "")

		M.debugInitialized = true
	end
end

local function updateDebugValues(dt)
	M.driveThrottlePid:updateDebugValues()
	M.engineStarterPid:updateDebugValues()

	if M.debug.enabled then
		-- Calculate actual torques
		local actualTorque = gearbox.outputTorque1 * torqueData.gearboxChildGearRatio
		local actualTorqueEV = gearbox.motorGenerator2Torque * torqueData.cumulativeMG2GearRatio * torqueData.gearboxChildGearRatio
		local actualTorqueICE = engineState.outputTorque * torqueData.engineOutputGearRatio * torqueData.gearboxChildGearRatio
		local regenAccelPedal = misc.negThrottle * M.offPedalRegenSmoother:value()
		local regenBrakePedalCoef = max(0, (braking.desiredRegen - regenAccelPedal) / braking.desiredRegen)
		local currentRegenTorqueOnWheelsFromBrakePedal = regenBrakePedalCoef * braking.currentRegenTorqueOnWheels
		local regenToTotalTorqueRatio = currentRegenTorqueOnWheelsFromBrakePedal / debugValues.requestedBrakingTorqueOnWheels
		local neededFrictionBrakingCoef = clamp(braking.neededFrictionTorque / braking.totalPossibleBrakeTorque, 0, 1)

		M.debug:putValue("Power", M.powerOn and "On" or "Off")
		M.debug:putValue("State (Previous, Current)", { hybridState.previous or "none", hybridState.current })
		M.debug:putValue("Speed (MPH)", misc.speedMph, 1)
		M.debug:putValue("Incline Angle", misc.inclineAngle)
		M.debug:putValue("eCVT Drive Mode", misc.ecvtDriveMode)
		if mainTank then
			M.debug:putValue("Fuel Remaining (L)", mainTank.remainingVolume)
		else
			M.debug:putValue("Fuel Remaining (L)", "No fuel tank installed")
		end
		M.debug:putValue("Engine Run Time", engineState.timeSinceStart, 1)
		M.debug:putValue("Engine demanded on", tostring(engineDemand.isDemanded))
		M.debug:putValue("Engine on reason", engineDemand.reason)
		M.debug:putValue("Engine triggered on by", engineDemand.triggerReason)
		M.debug:putValue("Battery power (kW)", power.lastBatteryPower, 0)
		M.debug:putValue("Generator power (kW)", gearbox.motorGenerator1Power, 0)
		M.debug:putValue("Battery SOC % (raw, usable)", { battery.remainingRatio * 100, power.batteryLevel * 100 }, 1)
		M.debug:putValue("Input Throttle", (misc.throttle or 0))
		M.debug:putValue("Effective Throttle / Brake", { misc.posThrottle or 0, misc.brake or 0 })
		M.debug:putValue("Throttle % EV thresh.", misc.posThrottle * 100 / M.maxEvOnlyThrottle, 0)
		M.debug:putValue("Throttle Aggressiveness", misc.throttleAggressiveness)
		M.debug:putValue("Requested Braking Torque", debugValues.requestedBrakingTorqueOnWheels, 0)
		M.debug:putValue("Max Regen Torque (Wheels)", braking.maxRegenTorque, 0)
		M.debug:putValue("Requested Regen Torque (Wheels)", braking.requestedRegenTorqueOnWheels, 0)
		M.debug:putValue("Actual Regen Torque (Wheels)", braking.currentRegenTorqueOnWheels, 0)
		M.debug:putValue("Friction Braking Torque (Req, Act)", { braking.neededFrictionTorque, braking.currentFrictionTorque }, 0)
		M.debug:putValue("Total Braking Force (Torque, %)", { braking.currentFrictionTorque + braking.currentRegenTorqueOnWheels, braking.currentBrakingCoef }, 0)
		M.debug:putValue("Regen % of brake demand", regenToTotalTorqueRatio * 100, 0)
		if misc.brakeInput > 0.01 and misc.isInDriveGear and misc.gear ~= "R" then
			M.debug:putValue("Friction % of full brakes", neededFrictionBrakingCoef * 100, 0)
		else
			M.debug:putValue("Friction % of full brakes", misc.brakeInput * 100, 0)
		end
		M.debug:putValue("Available Power (Priority/Secondary)", { power.availablePriorityEvPower, power.availableSecondaryEvPower }, 0)
		M.debug:putValue("Available MG1 Power", power.availableMG1Power, 0)
		M.debug:putValue("Available MG2 Power", power.availableMG2Power, 0)
		M.debug:putValue("Available EV Torque (Priority/Secondary)", { debugValues.availablePriorityEvOutputTorque, debugValues.availableSecondaryEvOutputTorque }, 0)
		M.debug:putValue("Available Wheel Torque (Total)", torqueData.maxThrottleMappedTorque * torqueData.gearboxChildGearRatio, 0)
		M.debug:putValue("Requested Wheel Torque (Total, EV, ICE)", {
			torqueData.requestedTotalOutputTorque * torqueData.gearboxChildGearRatio,
			torqueData.requestedElectricOutputTorque * torqueData.gearboxChildGearRatio,
			debugValues.requestedEngineOutputTorque * torqueData.gearboxChildGearRatio,
		}, 0)
		if rearMotor then
			local rearMotorOutputTorque = rearMotor.motorTorque * rearMotor.cumulativeGearRatio

			actualTorque = actualTorque + rearMotorOutputTorque

			M.debug:putValue("Actual Wheel Torque (Total, EV F, EV R, ICE)", { actualTorque, actualTorqueEV, rearMotorOutputTorque, actualTorqueICE }, 0)
		else
			M.debug:putValue("Actual Wheel Torque (Total, EV, ICE)", { actualTorque, actualTorqueEV, actualTorqueICE }, 0)
		end
		M.debug:putValue("Requested Engine Torque", torqueData.requestedEngineTorque, 0)
		M.debug:putValue("Actual Engine Torque", engineState.outputTorque, 0)
		M.debug:putValue("Engine Lag Compensation", debugValues.engineLagCompensationTorque, 0)
		M.debug:putValue("Starter Compensation Torque", debugValues.starterCompensationTorque, 0)
		M.debug:putValue("Requested Charge Power (kW)", power.requestedChargePower, 0)
		M.debug:putValue("Min RPM (Working/Actual)", { engineState.minWorkingRpm, engineState.minRpm }, 0)
		M.debug:putValue("Max RPM", engineState.maxRpm, 0)
		M.debug:putValue("Min Target RPM", engineState.minTargetRpm, 0)
		M.debug:putValue("Max Target RPM", engineState.maxTargetRpm, 0)
		M.debug:putValue("Target RPM", engineState.targetRpm, 0)
		M.debug:putValue("Actual RPM", engineState.rpm, 0)
		M.debug:putValue("RPM Error", engineState.rpmError, 0)
		M.debug:putValue("Engine Load Bias", engineState.loadBiasTorque, 0)
		M.debug:putValue("Engine Load Torque", engineState.loadTorque + engineState.loadBiasTorque, 0)
		M.debug:putValue("Charge Throttle", engineState.chargeThrottle)
		M.debug:putValue("Throttle Feedforward", debugValues.feedforwardControl)
		M.debug:putValue("Desired Engine Throttle", engineState.targetThrottle)
		M.debug:putValue("Actual Engine Throttle", electrics.values.iceThrottle)
		M.debug:putValue("Engine Startup Coef", engineState.startupCoef)
		M.debug:putValue("Engine Engagement Coef", clamp(M.engineEngagementChargePowerFilter.state / M.engineEngagementChargePowerThreshold))
		if engine then
			M.debug:putValue("Engine Ignition Coef", engine.ignitionCoef)
			M.debug:putValue("Engine Load", engine.engineLoad, 0)
			M.debug:putValue("Engine Combustion Torque", engine.combustionTorque, 0)
			M.debug:putValue("Engine Torque", engine.outputTorque1, 0)
			M.debug:putValue("Engine HP", mathutil.torqueToKw(engine.outputTorque1, engine.outputAV1) * 1.34102, 1)
		else
			M.debug:putValue("Engine Ignition Coef", "")
			M.debug:putValue("Engine Load", "")
			M.debug:putValue("Engine Combustion Torque", "")
			M.debug:putValue("Engine Torque", "")
			M.debug:putValue("Engine HP", "")
		end
		M.debug:putValue("Coolant Temp.", electrics.values.watertemp, 0)

		M.debug:putValue("Target MG1 Torque", torqueData.targetMG1Torque, 0)

		-- Output torque profiling graph
		M.debug:putGraphValue{"Requested Output Torque", torqueData.requestedTotalOutputTorque * torqueData.gearboxChildGearRatio, "N•m"}
		M.debug:putGraphValue{"Actual Output Torque", actualTorque, "N•m"}
		M.debug:putGraphValue{"Requested ICE Torque", torqueData.requestedEngineTorque * torqueData.engineOutputGearRatio * torqueData.gearboxChildGearRatio, "N•m"}
		M.debug:putGraphValue{"ICE Output Torque", gearbox.torqueDiff * torqueData.engineOutputGearRatio * torqueData.gearboxChildGearRatio, "N•m"}
		M.debug:putGraphValue{"MG1 Output Torque", -gearbox.motorGenerator1Torque * torqueData.cumulativeMG1GearRatio * torqueData.gearboxChildGearRatio, "N•m"}
		M.debug:putGraphValue{"MG2 Output Torque", gearbox.motorGenerator2Torque * gearbox.counterMG2GearRatio * torqueData.gearboxChildGearRatio, "N•m"}
		if rearMotor then
		    M.debug:putGraphValue{"MGR Output Torque", rearMotor.motorTorque * rearMotor.cumulativeGearRatio, "N•m"}
		end
		M.debug:putGraphValue{"Zero", 0, color = "#00000080"}

		-- Engine startup profiling graph
		-- M.debug:putGraphValue{"Engine Startup Coef", engineState.startupCoef * M.engineStarterTorque} -- scaled by M.engineStarterTorque to be visible on the graph
		-- M.debug:putGraphValue{"Engine Running State", engineState.isRunning and M.engineStarterTorque or 0} -- scaled by M.engineStarterTorque to be visible on the graph
		-- M.debug:putGraphValue{"Startup/Shutdown Torque", M.engineStarterSmoother:value(), "N•m"}
		-- M.debug:putGraphValue{"Requested Engine Torque", torqueData.requestedEngineTorque, "N•m"}
		-- M.debug:putGraphValue{"Engine Load Torque", engineState.appliedLoadTorque, "N•m"}
		-- M.debug:putGraphValue{"Engine Load Bias Torque", engineState.loadBiasTorque, "N•m"}
		-- M.debug:putGraphValue{"MG1 Torque", gearbox.motorGenerator1Torque, "N•m"}
		-- M.debug:putGraphValue{"Zero", 0, color = "#00000080"}

		-- Engine RPM profiling graph
		-- M.debug:putGraphValue{"Target Engine RPM", engineState.targetRpm, "RPM"}
		-- M.debug:putGraphValue{"Actual Engine RPM", engineState.rpm, "RPM"}

		-- Brake profiling graph
		-- M.debug:putGraphValue{"Requested Brake Torque", debugValues.requestedBrakingTorqueOnWheels, "N•m"}
		-- M.debug:putGraphValue{"Actual Brake Torque", braking.currentFrictionTorque + braking.currentRegenTorqueOnWheels, "N•m"}
		-- M.debug:putGraphValue{"Regen Brake Torque", braking.currentRegenTorqueOnWheels, "N•m"}
		-- M.debug:putGraphValue{"Friction Brake Torque", braking.currentFrictionTorque, "N•m"}
		-- M.debug:putGraphValue{"Total Brake Torque", braking.currentFrictionTorque + braking.currentRegenTorqueOnWheels, "N•m"}

		-- Filter testing/visualization playground
		--[[ if not state.filters then
			local temporalSpring = 10 -- default = 10
			local temporalSpringDamp = 2 -- default = 2
			local temporalSigmoidSmoothing = 20000 -- default = 1
			local temporalSigmoidSmoothingAccel = 2000 -- default = math.huge
			local temporalSmoothingNonLinear = 1 -- default = 1
			local temporalSmoothing = 1 -- default = 1
			local linearSmoothingDt = 1 / 60
			local linearSmoothing = 1 -- default 1
			local exponentialSmoothing = 20

			state.filters = {
				timeValue = 0,
				temporalSpring = newTemporalSpring(temporalSpring, temporalSpringDamp),
				temporalSigmoidSmoothing = newTemporalSigmoidSmoothing(temporalSigmoidSmoothing, temporalSigmoidSmoothingAccel),
				temporalSmoothingNonLinear = newTemporalSmoothingNonLinear(temporalSmoothingNonLinear),
				temporalSmoothing = newTemporalSmoothing(temporalSmoothing),
				linearSmoothing = newLinearSmoothing(linearSmoothingDt, linearSmoothing),
				exponentialSmoothing = newExponentialSmoothing(exponentialSmoothing),
			}
		end

		state.filters.timeValue = state.filters.timeValue + dt

		local timeScale = 5 -- 2 oscillations per second
		local bias = input.throttle * 1000
		local magnitude = 15
		local controlValue = bias + magnitude * math.sin(state.filters.timeValue * timeScale * 2 * math.pi)
		local filteredValue = state.filters.temporalSigmoidSmoothing:get(controlValue, dt)
		local doubleFilteredValue = state.filters.exponentialSmoothing:get(filteredValue)

		-- M.debug:putGraphValue{"temporalSpring", state.filters.temporalSpring:get(controlValue, dt)}
		-- M.debug:putGraphValue{"temporalSigmoidSmoothing", state.filters.temporalSigmoidSmoothing:get(controlValue, dt)}
		-- M.debug:putGraphValue{"temporalSmoothingNonLinear", state.filters.temporalSmoothingNonLinear:get(controlValue, dt)}
		-- M.debug:putGraphValue{"temporalSmoothing", state.filters.temporalSmoothing:getUncapped(controlValue, dt)}
		-- M.debug:putGraphValue{"linearSmoothing", state.filters.linearSmoothing:get(controlValue)}
		-- M.debug:putGraphValue{"exponentialSmoothing", state.filters.exponentialSmoothing:get(controlValue)}
		M.debug:putGraphValue{"Raw Value", controlValue}
		M.debug:putGraphValue{"Filtered Value", filteredValue}
		M.debug:putGraphValue{"Double-Filtered Value", doubleFilteredValue}
		M.debug:putGraphValue{"Bias Line", bias} ]]
	end

	if M.synergydebug.enabled then
		M.synergydebug:putValue("tO - Total Requested Output Torque", synergyDebugValues.tO)
		M.synergydebug:putValue("rI - Gearbox:Engine Gear Ratio", synergyDebugValues.rI)
		M.synergydebug:putValue("rSC - PSD Sun:Carrier Gear Ratio", synergyDebugValues.rSC)
		M.synergydebug:putValue("crE - Output:Engine Cumulative Gear Ratio", synergyDebugValues.crE)
		M.synergydebug:putValue("cr2 - Output:MG2 Cumulative Gear Ratio", synergyDebugValues.cr2)
		M.synergydebug:putValue("av1 - MG1 Angular Velocity", synergyDebugValues.av1)
		M.synergydebug:putValue("av2 - MG2 Angular Velocity", synergyDebugValues.av2)
		M.synergydebug:putValue("pP - Extra EV Power Available", synergyDebugValues.pP)
		M.synergydebug:putValue("dP - Desired Net Power (To/From Battery)", synergyDebugValues.dP)
		M.synergydebug:putValue("tE - Target Engine Torque", synergyDebugValues.tE)
		M.synergydebug:putValue("t2 - Target MG2 Torque", synergyDebugValues.t2)

		M.synergydebug:putValue("clampedT2 - Achievable MG2 Torque", synergyDebugValues.clampedT2)
		M.synergydebug:putValue("t2Delta - MG2 Torque Deficit", synergyDebugValues.t2Delta)
		M.synergydebug:putValue("tEDelta - Engine Compensation Torque", synergyDebugValues.tEDelta)

		M.synergydebug:putValue("Engine Output Torque", synergyDebugValues.engineOutputTorque)
		M.synergydebug:putValue("Electric Output Torque", synergyDebugValues.electricOutputTorque)

		-- Synergy equation profiling
		--[[ if torqueData.maxThrottleMappedTorque > 10000 then
			M.synergydebug:putGraphValue{"tO = Requested Output Torque", synergyDebugValues.tO / 1000, "kN•m"}
		else
			M.synergydebug:putGraphValue{"tO = Requested Output Torque", synergyDebugValues.tO, "N•m"}
		end

		M.synergydebug:putGraphValue{"av1 = MG1 Angular Velocity", synergyDebugValues.av1, "rad/s"}
		M.synergydebug:putGraphValue{"av2 = MG2 Angular Velocity", synergyDebugValues.av2, "rad/s"}
		M.synergydebug:putGraphValue{"pP = EV Priority Power", synergyDebugValues.pP / 1000, "kW"}
		M.synergydebug:putGraphValue{"dP = Battery Power Delta", synergyDebugValues.dP / 1000, "kW"}
		M.synergydebug:putGraphValue{"tE = Requested Engine Torque", synergyDebugValues.tE, "N•m"}
		M.synergydebug:putGraphValue{"t2 = Requested MG2 Torque", synergyDebugValues.t2, "N•m"} ]]
	end
end

local function getProportionalBatteryRatio()
	return maprange(power.batteryLevelRaw, M.minBatteryPowerLevel, M.maxBatteryPowerLevel)
end

local function calculateElectricLimits(dt)
	power.batteryLevelRaw = battery.remainingRatio
	power.batteryLevel = getProportionalBatteryRatio()

	-- Calculate battery power limits
	if hybridState.current == "poweroff" then
		power.maxBatteryOutputPower = 0
		power.availableBatteryPower = 0
		power.availablePriorityEvPower = 0
		power.availableSecondaryEvPower = 0
		engineState.powerGenerated = 0
	else
		local availableBatteryPowerCoef = maprange(power.batteryLevelRaw, M.minBatteryPowerLevel - 0.05, M.minBatteryPowerLevel)

		power.maxBatteryOutputPower = (battery.jbeamData.maxPowerOutput or 1e5) - M.idlePowerDraw
		power.availableBatteryPower = availableBatteryPowerCoef * power.maxBatteryOutputPower

		-- Figure out some general values about what the electric subsystem can currently provide
		if hybridState.current == "evmode" or (engineState.isStarting and hybridState.previous == "evmode") then
			-- in EV mode, or just left EV mode to start engine
			engineState.powerGenerated = 0
			power.availablePriorityEvPower = power.availableBatteryPower
			power.availableSecondaryEvPower = power.availableBatteryPower
		else
			local priorityPowerSpeed
			local priorityPowerBatteryLevel = M.maxPriorityPowerBatteryLevel:getValue(power.batteryLevel * 100)

			if engineState.isRunning then
				priorityPowerSpeed = M.maxEngineAssistPriorityPowerSpeed:getValue(misc.speedMph)
			else
				priorityPowerSpeed = M.maxEvOnlyPriorityPowerSpeed:getValue(misc.speedMph)
			end

			engineState.powerGenerated = max(0, -gearbox.motorGenerator1Power)
			power.availablePriorityEvPower = misc.chargeModeEnabled and 0 or mathutil.minall(power.availableBatteryPower, priorityPowerSpeed, priorityPowerBatteryLevel)
			power.availableSecondaryEvPower = min(power.availableBatteryPower, M.maxSecondaryPowerBatteryLevel:getValue(power.batteryLevel * 100))

			if engineState.isRunning then
				power.availableSecondaryEvPower = min(M.maxEngineAssistSecondaryPowerSpeed:getValue(misc.speedMph), power.availableSecondaryEvPower)
			end
		end
	end

	-- Apply smoothing
	power.availablePriorityEvPower = M.priorityPowerSmoother:get(power.availablePriorityEvPower, dt)
end

local function calculateTorqueStatistics(dt)
	-- Calculate some torque performance statistics

	-- Raw max torques from gearbox and engine
	if torqueData.firstCalculation then
		-- Precalculate some gear ratios
		torqueData.gearboxChildGearRatio = 1

		if gearbox and gearbox.children and #gearbox.children > 0 then
			local gearboxChild = gearbox.children[1]

			torqueData.gearboxChildGearRatio = gearboxChild.cumulativeGearRatio
		end

		torqueData.engineOutputGearRatio = gearbox.gearRatio
		torqueData.engineToSunGearRatio = gearbox.inputGearRatio * gearbox.psd.sunCarrierRatio
		torqueData.cumulativeMG1GearRatio = gearbox.counterRingGearRatio / gearbox.psd.sunRingRatio
		torqueData.cumulativeMG2GearRatio = gearbox.counterMG2GearRatio

		local maxInputTorqueFromEngine = engine and engine.torqueData.maxTorque or 0
		local maxMG1Torque = gearbox.mg1TorqueRating
		local maxInputTorqueLimitedByMG1 = maxMG1Torque / (gearbox.inputGearRatio * gearbox.psd.sunCarrierRatio)
		local maxUsableEngineTorque = min(maxInputTorqueLimitedByMG1, maxInputTorqueFromEngine)
		local maxEngineTorqueToOutput = maxUsableEngineTorque * torqueData.engineOutputGearRatio

		torqueData.maxEngineGeneratorTorque = min(maxMG1Torque, maxUsableEngineTorque * gearbox.inputGearRatio * gearbox.psd.sunCarrierRatio)
		torqueData.maxEngineOutputTorque = maxEngineTorqueToOutput
		torqueData.maxEngineTorque = maxInputTorqueFromEngine

		-- Figure out engine torque characteristics
		if engine then
			local curve = engine.torqueData.curves[engine.torqueData.finalCurveName]
			local idleTorque = curve.torque[engine.idleRPM]

			torqueData.engineIdleTorque = idleTorque
			torqueData.engineMaxTorqueRpm = engine.torqueData.maxTorqueRPM
			torqueData.engineTorqueCurve = curve.torque
		else
			torqueData.engineIdleTorque = 0
			torqueData.engineMaxTorqueRpm = 0
			torqueData.engineTorqueCurve = nil
		end

		-- Figure out reference max torque for throttle mapping
		local overallMaxPower = power.maxBatteryOutputPower
		local overallMaxMG2Torque = gearbox:getMaxMG2Torque(overallMaxPower) * torqueData.cumulativeMG2GearRatio

		torqueData.initialMaxThrottleMappedTorque = torqueData.maxEngineOutputTorque + overallMaxMG2Torque

		if M.maxOutputTorqueVsSpeed then
			torqueData.initialMaxThrottleMappedTorque = min(torqueData.initialMaxThrottleMappedTorque, M.maxOutputTorqueVsSpeed.maxY)
		end

		torqueData.firstCalculation = false
	end

	torqueData.maxThrottleMappedTorque = torqueData.initialMaxThrottleMappedTorque

	local outputTorqueBonus = 0

	if M.outputTorqueBonusVsInclineAngle then
		outputTorqueBonus = M.outputTorqueBonusVsInclineAngle:getValue(misc.inclineAngle * (misc.gear == "R" and -1 or 1)) -- bonus when pointed down in Reverse

		torqueData.maxThrottleMappedTorque = torqueData.maxThrottleMappedTorque + outputTorqueBonus
	end

	local maxAvailableGeneratorPower = hybridState.current == "evmode" and 0 or abs(mathutil.torqueToKw(torqueData.maxEngineGeneratorTorque, gearbox.motorGenerator1AV))
	local maxAvailableMG2Power = power.availablePriorityEvPower + power.availableSecondaryEvPower + maxAvailableGeneratorPower
	local maxAvailableMG2Torque = gearbox:getMaxMG2Torque(maxAvailableMG2Power) * torqueData.cumulativeMG2GearRatio

	torqueData.maxElectricTorque = maxAvailableMG2Torque
	torqueData.fullBatteryElectricTorque = gearbox:getMaxMG2Torque(power.maxBatteryOutputPower) * torqueData.cumulativeMG2GearRatio

	if hybridState.current == "evmode" then
		torqueData.maxCombinedTorque = torqueData.maxElectricTorque
		engineState.outputTorque = 0
		engineState.combustionTorque = 0

		M.engineOutputTorqueSmoother:reset()
		M.engineCombustionTorqueSmoother:reset()
	else
		-- Figure out compound torques

		torqueData.maxCombinedTorque = torqueData.maxEngineOutputTorque + torqueData.maxElectricTorque
		engineState.outputTorque = M.engineOutputTorqueSmoother:get(engine and max(0, engine.outputTorque1) or 0)
		engineState.combustionTorque = M.engineCombustionTorqueSmoother:get(engine and engine.combustionTorque or 0)
	end

	if M.maxOutputTorqueVsSpeed and misc.isInDriveGear and misc.gear ~= "R" then
		torqueData.maxCombinedTorque = min(torqueData.maxCombinedTorque, outputTorqueBonus + M.maxOutputTorqueVsSpeed:getValue(misc.speedMph))
	end

	if M.maxReverseOutputTorqueVsSpeed and misc.gear == "R" then
		torqueData.maxCombinedTorque = min(torqueData.maxCombinedTorque, outputTorqueBonus + M.maxReverseOutputTorqueVsSpeed:getValue(misc.speedMph))
	end

	if misc.reducedPropulsion then
		torqueData.maxCombinedTorque = M.reducedPropulsionTorqueCoef * torqueData.maxCombinedTorque
	end

	torqueData.gearboxMaxMG2Torque = gearbox:getMaxMG2Torque()
	torqueData.gearboxMaxMG1TorqueUnchecked = gearbox:getMaxMG1Torque(true)
	torqueData.gearboxMaxMG2TorqueUnchecked = gearbox:getMaxMG2Torque(true)
	torqueData.gearboxMaxMG2RegenTorque = gearbox:getMaxMG2RegenTorque()
end

local function getTorqueData()
	return torqueData
end

local function printTorqueData()
	M.debug:log("I", "Max Electric Torque: " .. fnum(torqueData.maxElectricTorque))
	M.debug:log("I", "Max Full-Battery Electric Torque: " .. fnum(torqueData.fullBatteryElectricTorque))
	M.debug:log("I", "Max Usable Engine Torque: " .. fnum(torqueData.maxEngineOutputTorque))
	M.debug:log("I", "Max Optimal Total Torque: " .. fnum(torqueData.maxThrottleMappedTorque))
	M.debug:log("I", "Max Engine Idle Torque: " .. fnum(torqueData.engineIdleTorque))
	M.debug:log("I", "Engine Max Torque RPM: " .. fnum(torqueData.engineMaxTorqueRpm))
end

-- #region Engine Functions

local function requestEngine(reason)
	if engineDemand.miscDemand then return end

	engineDemand.miscDemandReason = reason
	engineDemand.miscDemand = true
end

local function isEngineDemandedOn()
	if electrics.values.evMode and electrics.values.evMode > 0 then
		-- engine never starts in "EV" mode
		return false
	end

	if not M.enableAutoStop then
		-- don't perform any additional checks if auto stop is disabled
		engineDemand.reason = "auto stop disabled"
		return true
	end

	local inputThrottle = misc.posThrottle
	local minRunTime

	if misc.watertemp < M.minAutoStopCoolantTempWarmedUp then
		-- if engine is not warmed up, it needs to run longer for each start event
		minRunTime = M.engineMinRunTime * 2
	else
		minRunTime = M.engineMinRunTime
	end

	if misc.chargeModeEnabled then
		-- engine must always run in charge mode
		engineDemand.reason = "charge mode"
		return true
	end

	if misc.watertemp < M.minAutoStopCoolantTemp or engineDemand.waterTempDemand then
		-- engine must always run until it reaches a minimum operating temperature
		engineDemand.reason = "temp"
		engineDemand.waterTempDemand = true
		return true
	end

	if engineDemand.miscDemand then
		-- some other piece of code requested the engine on for some reason
		engineDemand.reason = engineDemand.miscDemandReason
		return true
	end

	if M.latchBatteryChargeUp:test(power.batteryLevel) then
		-- battery level got too low; need to run engine until it charges up a bit
		engineDemand.reason = "battery level"
		return true
	end

	if misc.gear == "R" then
		-- any checks after this are bypassed if in Reverse; engine should be off
		-- except for critical situations in Reverse.
		engineDemand.reason = "none (forced off due to reverse)"
		return false
	end

	if misc.speedMph > M.maxEvOnlySpeed then
		-- vehicle moving too fast for engine-off driving
		engineDemand.reason = "speed"
		return true
	end

	if misc.inclineAngle > M.maxEvOnlyPitch then
		-- a "max pitch" was set for this vehicle; engine should remain running if
		-- vehicle is pitched up too much, e.g. if a heavy vehicle needs immediate
		-- hill start torque from the engine
		engineDemand.reason = "vehicle pitch"
		return true
	end

	if misc.throttleAggressiveness > M.maxEvOnlyThrottleAggressiveness then
		-- driver snapped the throttle; start engine sooner in preparation for
		-- heavy acceleration
		engineDemand.reason = "throttle aggressiveness"
		return true
	end

	if M.autoStopBrakeThreshold then
		if engineState.isRunning and (not engineDemand.autoStopBrakeRequirementSatisfied or braking.timeSinceAppliedBrakes < M.autoStopBrakeTime) then
			-- if the vehicle has a "minimum brake application" threshold for auto-stop,
			-- make sure the brakes have been applied long enough before allowing engine
			-- to stop
			if misc.gear ~= "P" then
				engineDemand.reason = "brakes"
				return true
			end
		end
	end

	if engineState.isRunning then
		if inputThrottle > M.autoStopThrottleThreshold then
			-- engine should remain on once it's started until the driver reaches
			-- a near-coast
			engineDemand.reason = "accelerating with engine on"
			return true
		end
	else
		if misc.gear ~= "R" and (electrics.values.kw or 0) > M.maxEvOnlyPriorityPowerSpeed:getValue(misc.speedMph) then
			-- in forward drive gears, engine will start if MG2 draws more power than
			-- a particular threshold for the current vehicle speed
			engineDemand.reason = "priority power exceeded"
			return true
		end
	end

	-- These checks come last so they don't override other reasons
	local vehicleStartupEngineDelayElapsed = M.vehicleStartupEngineDelay >= 0 and misc.timeSincePowerOn > M.vehicleStartupEngineDelay
	local vehicleStartupDelayedEngineStart = (engineState.isRunning or vehicleStartupEngineDelayElapsed) and engineState.runTimeSincePowerOn < M.vehicleStartupEngineRuntime
	local vehicleStartupBeganAccelerating = (electrics.values.kw or 0) > 10 and engineState.runTimeSincePowerOn == 0.0 -- only a small amount of acceleration is required when the vehicle starts up

	if vehicleStartupDelayedEngineStart or vehicleStartupBeganAccelerating then
		-- this vehicle is set to start the engine a certain amount of time after
		-- the vehicle is started, and the engine must run a certain amount of time
		-- before being allowed to auto-stop
		engineDemand.reason = "vehicle startup"
		return true
	end
	if (hybridState.current == "startup" or hybridState.current == "running") and engineState.timeSinceStart < minRunTime then
		-- engine must run for a certain amount of time this start cycle before being
		-- allowed to auto-stop
		engineDemand.reason = "time since start"
		return true
	end

	engineDemand.reason = "none"
	return false -- if none of the previous checks requested the engine, it should be off
end

local function stopEngine()
	if not engine then return end

	electrics.values.iceThrottle = 0
	engine.ignitionCoef = 0 -- Kill engine
	engine.isDisabled = true -- Prevent the user from manually restarting the engine
	engineState.timeSinceCanShutOff = M.autoStopDelayTime
	engineState.startupCoef = 0
	M.engineStartupSmoother:reset()
	engine:deactivateStarter()
end

local function handleFailedEngine()
	if not engine then return end

	if not misc.shownDisabledMessage then
		gui.message({txt = "Engine has failed; propulsion power reduced."}, 5, "vehicle.hybrid.general", "build")
		misc.shownDisabledMessage = true
		misc.reducedPropulsion = true
		engineState.timeSinceCanShutOff = M.autoStopDelayTime
		electrics.values.ev = 1
		engine:disable()
	end
end

local function getEngineShutdownTorque()
	local stationaryCarrierSunAV = gearbox.psd:getSunAV(nil, 0.0)
	local sunAVError = (gearbox.motorGenerator1AV - stationaryCarrierSunAV) / 100
	local engineShutdownLoad = clamp(sunAVError, 0, 1) * M.engineSpooldownTorque

	return engineShutdownLoad
end

-- #endregion Engine Functions

local function updateEngineDemand()
	if engineDemand.isDemanded and not engineDemand.wasDemanded then
		engineDemand.triggerReason = engineDemand.reason
	elseif not engineDemand.isDemanded and hybridState.current ~= "startup" then
		engineDemand.triggerReason = "none"
	end

	engineDemand.wasDemanded = engineDemand.isDemanded
end

local function updateValues(dt)
	if not gearbox or not battery then
		return
	end

	-----------------------
	-- Update torqueData --
	-----------------------
	torqueData.targetRearMotorTorque = 0

	-----------------------------
	-- Update custom electrics --
	-----------------------------
	if misc.isInDriveGear then
		electrics.values.kw = mathutil.torqueToKw(gearbox.rawOutputTorque, gearbox.outputAV1) -- propulsion power kw

		if rearMotor then
			-- add rear motor power to total propulsion power
			electrics.values.kw = electrics.values.kw + mathutil.torqueToKw(rearMotor.rawOutputTorque, rearMotor.outputAV1)
		end
	else
		electrics.values.kw = 0
	end

	------------------
	-- Update state --
	------------------
	local throttleAggressiveness = (input.throttle - misc.lastThrottle) / dt

	misc.gear = M.latchCurrentGear:get(electrics.values.gear, dt)
	misc.lastThrottle = input.throttle
	misc.isInDriveGear = misc.gear ~= "P" and misc.gear ~= "N"
	misc.speedMph = M.vehicleSpeedSmoother:get(electrics.values.wheelspeed * constants.mpsToMph, dt)
	misc.isParked = misc.speedMph < 0.5 and (misc.gear == "P" or electrics.values.parkingbrake == 1)
	misc.isStopped = misc.speedMph < 0.5 and misc.brakeInput > 0.1
	misc.launchControl = (misc.brakeInput >= 0.25 or electrics.values.parkingbrake == 1) and misc.throttle > 0.02
	misc.watertemp = electrics.values.watertemp
	misc.actualSpeedSign = mathutil.sign(electrics.values.avgWheelAV)
	misc.desiredSpeedSign = misc.gear == "R" and -1 or 1
	misc.brakeInput = input.brake
	misc.throttleAggressiveness = M.throttleAggressivenessSmoother:get(throttleAggressiveness)
	misc.inclineAngle = M.inclineAngleSmoother:get(obj:getDirectionVector().z * 180 / math.pi, dt)

	------------------------
	-- Update engineState --
	------------------------
	engineState.rpm = electrics.values.rpm

	-- Figure out if engine is truly running (if it stalled somehow, the throttle PID will quickly shoot up to 1.0 but the engine's combustion torque will be negative from friction)
	local engineTorqueStall = (electrics.values.iceThrottle or 0) >= 0.99 and (engine.combustionTorque or 0) <= 0

	engineState.isRunning = engine and not (engine.isStalled or engine.isDisabled or engine.ignitionCoef < 1e-2 or electrics.values.rpm < engine.idleRPM - 100 or engineTorqueStall)
	engineState.isStarting = hybridState.current == "startup"
	engineState.isStopping = not engineState.isRunning and not engineState.isStarting and engineState.rpm >= 2
	engineState.last = engineState.current

	if not engineState.isRunning then
		M.engineStartupSmoother:reset()
	end

	local isEngineStarted = engineState.isRunning and not engineState.isStarting

	engineState.startupCoef = M.engineStartupSmoother:getCapped(isEngineStarted and 1 or 0, dt)

	if engineState.isStarting then
		engineState.current = "starting"
	elseif engineState.isStopping then
		engineState.current = "stopping"
	else
		engineState.current = engineState.isRunning and "running" or "stopped"
	end
end

local function updateValuesGFX(dt)
	M.timeSinceVehicleLoaded = M.timeSinceVehicleLoaded + dt

	-- update engineDemand
	engineDemand.isDemanded = isEngineDemandedOn() -- Have to do this before resetting miscDemand in next two lines, in case miscDemand was set last tick
	engineDemand.miscDemandReason = "none"
	engineDemand.miscDemand = false

	------------------------
	--- Update electrics ---
	------------------------
	electrics.values.batteryLevel = power.batteryLevel
	electrics.values.engineOnKwThreshold = engine and M.maxEvOnlyPriorityPowerSpeed:getValue(misc.speedMph) or 0
	electrics.values.hybridSystemOn = M.powerOn and 1 or 0

	-- Critical battery level indicator
	electrics.values.battery = power.batteryLevelRaw < M.criticalBatteryPowerLevel

	-- "EV" indicator
	local engineIsBroken = engine and engine.isBroken or false
	local isEngineEffectivelyDemanded = engineDemand.isDemanded or engineState.timeSinceCanShutOff < M.autoStopDelayTime

	if not M.powerOn or (not engineIsBroken and isEngineEffectivelyDemanded) then
		electrics.values.ev = 0
	else
		electrics.values.ev = 1
	end

	-- "Check Engine" light (might be triggered later too)
	electrics.values.checkengine = misc.reducedPropulsion

	-- update average wheel speed in GFX because we don't need it updated super frequently and it is potentially expensive
	local averageWheelSpeed = 0

	for i = 0, wheels.wheelCount - 1, 1 do
		local wd = wheels.wheels[i]

		if wd then
			averageWheelSpeed = averageWheelSpeed + wd.wheelSpeed / wheels.wheelCount
		end
	end

	misc.averageWheelSpeedMph = averageWheelSpeed * constants.mpsToMph

	-- update some throttle stuff
	local throttleRate = M.throttleRateVsAggressiveness:getValue(abs(misc.throttleAggressiveness))

	-- Calculate some throttle input variables
	misc.throttle = electrics.values.throttleOverride or M.throttleSmoother:get(input.throttle, dt * throttleRate)

	local throttleOffset

	if misc.gear == "L" then
		throttleOffset = maprange(braking.maxRegenTorque, 0, braking.uncappedMaxRegenTorque, 0.02, 0.25)
	else
		throttleOffset = M.evCoastRegen
	end

	misc.posThrottle = maprange(misc.throttle, throttleOffset, 1) ^ (1 / M.throttleCurveExponent)
	misc.negThrottle = maprange(misc.throttle, throttleOffset, 0)

	-- Update engine runtime
	if engineState.isRunning and hybridState.current ~= "startup" then
		engineState.runTimeSincePowerOn = engineState.runTimeSincePowerOn + dt
		engineState.timeSinceStart = engineState.timeSinceStart + dt

		if misc.watertemp >= M.minAutoStopCoolantTemp + 2.5 then
			engineDemand.waterTempDemand = false
		end
	else
		engineState.timeSinceStart = 0
	end

	-- Update power-on time
	if M.powerOn then
		misc.timeSincePowerOn = misc.timeSincePowerOn + dt
	else
		misc.timeSincePowerOn = 0
	end

	-- Track brake application time for auto-stop purposes
	engineDemand.autoStopBrakeRequirementSatisfied = (electrics.values.parkingbrake or 0) >= 0.5
	                                                 or (electrics.values.brakeHoldActive or 0) >= 0.5
													 or braking.currentBrakingCoef >= (M.autoStopBrakeThreshold or -math.huge)

	if engineDemand.autoStopBrakeRequirementSatisfied and misc.speedMph <= M.maxEvOnlySpeed then
		braking.timeSinceAppliedBrakes = braking.timeSinceAppliedBrakes + dt
	else
		braking.timeSinceAppliedBrakes = 0
	end
end

local function updateBattery(dt)
	local idlePowerDraw = M.powerOn and M.idlePowerDraw or 0
	local idleEnergyConsumption = idlePowerDraw * 1000 * dt
	local gearboxEnergyConsumption = gearbox.spentEnergyLastUpdate
	local rearMotorEnergyConsumption = rearMotor and rearMotor.spentEnergyLastUpdate or 0
	local batteryEnergyDelta = idleEnergyConsumption + gearboxEnergyConsumption + rearMotorEnergyConsumption -- Joules

	-- Apply battery efficiency losses
	if batteryEnergyDelta > 1e-20 then
		-- Power coming out of battery; divide by efficiency to pull a bit more
		batteryEnergyDelta = batteryEnergyDelta / M.batteryEfficiency
	elseif batteryEnergyDelta < -1e-20 then
		-- Power going into battery; multiply by efficiency to push a bit less
		batteryEnergyDelta = batteryEnergyDelta * M.batteryEfficiency
	end

	power.lastBatteryPower = M.batteryPowerSmoother:get(batteryEnergyDelta / dt / 1000, dt)
	battery.storedEnergy = mathutil.clamp(battery.storedEnergy - batteryEnergyDelta, 0, battery.energyCapacity)
end

local function getPowerLimits()
	local maxKw

	if M.kwGaugeMaxPower then
		maxKw = M.kwGaugeMaxPower
	else
		if battery and battery.jbeamData and (battery.jbeamData.maxPowerOutput or battery.jbeamData.maxPowerInput) then
			local maxOutput = battery.jbeamData.maxPowerOutput or 0
			local maxInput = battery.jbeamData.maxPowerInput or 0

			maxKw = max(maxOutput, maxInput)
		else
			maxKw = gearbox and max(gearbox.mg1PowerRating, gearbox.mg2PowerRating) or 0
		end
	end

	local ecoKw

	if M.kwGaugeEcoPower then
		ecoKw = M.kwGaugeEcoPower
	else
		ecoKw = electrics.values.engineOnKwThreshold
	end

	local regenKw

	if M.kwGaugeRegenPower then
		regenKw = M.kwGaugeRegenPower
	else
		regenKw = maxKw
	end

	return maxKw, ecoKw, regenKw
end

local function updateGUI(dt)
	local maxKw, ecoKw, regenKw = getPowerLimits()
	local mode = "Off"

	if hybridState.current == "init" then
		mode = "---"
	elseif M.powerOn then
		if electrics.values.chargeMode > 0 then
			mode = "Charge"
		elseif electrics.values.evMode > 0 then
			mode = "EV"
		else
			mode = "HV"
		end
	end

	if streams.willSend("hybrid") then
		local str = {
			soc = power.batteryLevel,
			kw = electrics.values.kw,
			batteryKw = power.lastBatteryPower,
			ecoKw = ecoKw,
			maxKw = maxKw,
			regenKw = regenKw,
			minSoc = 0.0,
			maxSoc = 1.0,
			powerOn = M.powerOn,
			ev = electrics.values.ev > 0,
			mode = mode,
		}

		gui.send("hybrid", str)
	end
end

local function updateWhileBraking(dt)
	-- Updating while brake pedal is applied

	if not misc.isInDriveGear or misc.gear == "R" then
		-- No regen in P, R, or N

		misc.brake = misc.brakeInput
		electrics.values.effectiveBrake = electrics.values.brake
	else
		-- Blend friction/regen brakes in any other gear

		local wheelsSlipping = false
		local actualFrictionTorque = 0
		local averageWheelSpeed = 0

		for _, wheel in pairs(wheels.wheels) do
			actualFrictionTorque = actualFrictionTorque + wheel.desiredBrakingTorque
			wheel.totalTorqueCoef = wheel.brakeTorque / braking.totalPossibleBrakeTorque

			if wheel.canRegen then
				wheel.propulsedTorqueCoef = wheel.brakeTorque / braking.totalPropulsedBrakeTorque
			else
				wheel.unpropulsedTorqueCoef = wheel.brakeTorque / braking.totalUnpropulsedBrakeTorque
			end

			averageWheelSpeed = averageWheelSpeed + (wheel.wheelSpeed / wheels.wheelCount)
		end

		for _, wheel in ipairs(wheelsWithRegen) do
			if wheel.lastSlip > M.maxWheelSlipCoefForRegen then
				wheelsSlipping = true
			end
		end

		braking.lastAverageWheelSpeed = averageWheelSpeed
		braking.disableRegen = M.latchRegenDisable:test(wheelsSlipping, dt)
		M.latchRegenDisable.timeToLatch = braking.disableRegen and M.regenWheelslipReenableTime or M.regenWheelslipDisableTime

		-- Combine regen input from brake pedal and throttle pedal (one-pedal driving)
		local regenPercentOfBrakingForce = braking.maxRegenTorque / braking.totalPossibleBrakeTorque
		local regenBrakePedal = maprange(misc.brakeInput, 0, regenPercentOfBrakingForce, 0, 1)
		local regenAccelPedal = misc.negThrottle * M.offPedalRegenSmoother:get(misc.gear == "L" and 1 or 0, dt)

		braking.desiredRegen = clamp(regenBrakePedal + regenAccelPedal, 0, 1)
		braking.requestedRegenTorqueOnWheels = braking.desiredRegen * braking.maxRegenTorque
		misc.brake = min(1, misc.brakeInput + regenAccelPedal * regenPercentOfBrakingForce)

		-- Calculate amount of friction brakes needed to cover brake demand
		local requestedBrakingTorqueOnWheels = misc.brakeInput * braking.totalPossibleBrakeTorque
		local regenBrakePedalCoef = max(0, (braking.desiredRegen - regenAccelPedal) / braking.desiredRegen)
		local currentRegenTorqueOnWheelsFromBrakePedal = regenBrakePedalCoef * braking.currentRegenTorqueOnWheels
		local neededFrictionBrakingTorque = max(0, requestedBrakingTorqueOnWheels - currentRegenTorqueOnWheelsFromBrakePedal)

		braking.currentFrictionTorque = actualFrictionTorque
		braking.currentBrakingCoef = (braking.currentFrictionTorque + braking.currentRegenTorqueOnWheels) / braking.totalPossibleBrakeTorque
		braking.neededFrictionTorque = neededFrictionBrakingTorque

		electrics.values.brakelights = max(electrics.values.brakelights, 1)
		electrics.values.effectiveBrake = neededFrictionBrakingTorque / braking.totalPossibleBrakeTorque

		debugValues.requestedBrakingTorqueOnWheels = requestedBrakingTorqueOnWheels
	end
end

local function updateWheelsIntermediate(dt)
	local parkingbrakeInput = input.parkingbrake or 0

	if not misc.isInDriveGear or misc.gear == "R" then
		return
	end

	-- Apply more friction brakes on wheels without regen to balance them
	for i, wd in pairs(wheels.wheels) do
		local thisWheelCurrentTorque = wd.desiredMainBrakingTorque

		if thisWheelCurrentTorque >= 1e-30 and not braking.disableRegen then
			local thisWheelNeededTorque

			if wd.canRegen then
				local thisWheelRegenAdjust = min(braking.currentRegenTorqueOnWheels, braking.totalUnpropulsedBrakeTorque) * (wd.propulsedTorqueCoef or 1)

				thisWheelNeededTorque = max(0, braking.neededFrictionTorque - thisWheelRegenAdjust) * (wd.totalTorqueCoef or 1)
			else
				-- Unpropulsed-only braking until friction brakes = regen brakes
				local thisWheelRegenBalance = min(braking.currentRegenTorqueOnWheels, braking.neededFrictionTorque) * (wd.unpropulsedTorqueCoef or 1)
				-- Then balanced normally amongst propulsed and unpropulsed
				local thisWheelNormalBalance = max(0, braking.neededFrictionTorque - braking.currentRegenTorqueOnWheels) * (wd.totalTorqueCoef or 1)

				thisWheelNeededTorque = thisWheelRegenBalance + thisWheelNormalBalance
			end

			wd.desiredMainBrakingTorque = min(thisWheelCurrentTorque, thisWheelNeededTorque)
			wd.desiredBrakingTorque = max(wd.desiredMainBrakingTorque, wd.parkingTorque * parkingbrakeInput * wd.defaultBrakeInputUsageCoef)
		end
	end
end

local function updateBrakesGlobal(dt)
	-- If there is a maximum power input on the battery, we can't exceed it while regenerating
	-- Instead, we have to figure out the maximum regen coefficient that we can apply without
	-- exceeding the battery's max power input
	local canRegen = not (
		hybridState.current == "poweroff" or
		hybridState.current == "shutdown" or
		braking.disableRegen
	)
	local maxAllowedRegenTorque = min(M.maxRegenTorque, torqueData.gearboxMaxMG2RegenTorque * gearbox.counterMG2GearRatio)

	braking.maxRegenTorque = canRegen and maxAllowedRegenTorque * torqueData.gearboxChildGearRatio or 0
	braking.uncappedMaxRegenTorque = braking.maxRegenTorque

	if battery.jbeamData.maxPowerInput then
		local batteryPowerInputCoef = maprange(power.batteryLevelRaw, 0.9, 0.95, 1, 0)
		local batteryPowerInputAvailable = battery.jbeamData.maxPowerInput * batteryPowerInputCoef - engineState.powerGenerated

		if misc.chargeModeEnabled then
			batteryPowerInputAvailable = batteryPowerInputAvailable - M.chargeModePower
		end

		-- Calculate what percentage of regen we can use, taking into account
		-- the maximum amount of power the battery can currently accept, minus
		-- however much power the engine is putting into the battery
		local mg2AV = max(abs(gearbox.motorGenerator2AV), gearbox.regenFadeMinAV)
		local maxMG2Torque = mathutil.kwToTorque(battery.jbeamData.maxPowerInput, mg2AV)
		local maxMG2RegenTorque = maxMG2Torque * gearbox.counterMG2GearRatio * torqueData.gearboxChildGearRatio

		braking.maxRegenTorque = min(braking.maxRegenTorque, maxMG2RegenTorque)

		if rearMotor then
			local rearAV = max(abs(rearMotor.motorAV), rearMotor.motorMinFullRegenAV)
			local maxRearTorque = mathutil.kwToTorque(battery.jbeamData.maxPowerInput, rearAV)
			local maxRearRegenTorque = maxRearTorque * rearMotor.cumulativeGearRatio

			braking.maxRegenTorque = min(braking.maxRegenTorque, maxRearRegenTorque)
		end
	end

	local regenTorqueFromMG2 = 0
	local regenTorqueFromRear = 0

	if gearbox:getMaxMG2RegenCoef() > 1e-2 then
		regenTorqueFromMG2 = max(0, gearbox.motorGenerator2Torque * -misc.actualSpeedSign) * gearbox.counterMG2GearRatio * torqueData.gearboxChildGearRatio
	end
	if rearMotor then
		regenTorqueFromRear = max(0, rearMotor.outputTorque1 * -misc.actualSpeedSign) * rearMotor.cumulativeGearRatio
	end

	braking.currentRegenTorqueOnWheels = regenTorqueFromMG2 + regenTorqueFromRear

	if brakeHold then
		if misc.gear == "P" then
			if engineState.isStarting or engineState.isStopping then
				engineState.startupShutdownBrakeHoldTime = 0.5
			elseif engineState.startupShutdownBrakeHoldTime > 0 then
				engineState.startupShutdownBrakeHoldTime = engineState.startupShutdownBrakeHoldTime - dt
			end
		else
			engineState.startupShutdownBrakeHoldTime = 0
		end

		if engineState.startupShutdownBrakeHoldTime > 0 then
			brakeHold.request("hybrid.engine.startupShutdown") -- prevent vehicle from rocking back/forth in park when engine starts or stops
		else
			brakeHold.releaseRequest("hybrid.engine.startupShutdown")
		end
	end

	if misc.brakeInput > 0 then
		updateWhileBraking(dt)
	else
		braking.disableRegen = false
		braking.currentFrictionTorque = 0
		braking.currentBrakingCoef = braking.currentRegenTorqueOnWheels / braking.totalPossibleBrakeTorque
		electrics.values.effectiveBrake = electrics.values.brake

		if misc.isInDriveGear then
			-- Regenerative braking when letting off accelerator (tiny bit in D, full in L)
			local regenPercentOfBrakingForce = braking.maxRegenTorque / braking.totalPossibleBrakeTorque
			local regenAccelPedal = misc.negThrottle * M.offPedalRegenSmoother:get(misc.gear == "L" and 1 or M.evCoastRegen, dt)

			misc.brake = misc.negThrottle * regenAccelPedal * regenPercentOfBrakingForce
			braking.desiredRegen = misc.negThrottle * regenAccelPedal * gearbox:getMaxMG2RegenCoef()
			braking.requestedRegenTorqueOnWheels = braking.desiredRegen * braking.maxRegenTorque
		else
			misc.brake = 0
			braking.desiredRegen = 0
			braking.requestedRegenTorqueOnWheels = 0

			M.offPedalRegenSmoother:reset()
		end
	end

	braking.requestedRegenTorque = braking.requestedRegenTorqueOnWheels / torqueData.gearboxChildGearRatio
end

local function updateTargetEngineRpm(dt)
	if not engine then return end

	-- Calculate min/max target RPMs
	local minTargetRpm = engineState.minRpm
	local maxTargetRpm = engineState.maxRpm

	if not engineState.isStarting then
		if not misc.isInDriveGear or misc.launchControl then
			-- Apply a direct link between throttle pedal and minimum RPM

			minTargetRpm = maprange(misc.posThrottle, 0, 1, minTargetRpm, misc.launchControl and M.maxLaunchControlRpm or maxTargetRpm)
		end

		if M.useEngineTorqueCurve and M.engineRpmVsTorque then
			-- "engineRpmVsTorque" acts as a minimum bound for the torque curve calculation
			local minimumRpm = M.engineRpmVsTorque:getValue(torqueData.requestedEngineTorque)

			minTargetRpm = max(minTargetRpm, minimumRpm)
		end
	end

	if engineState.isStarting and type(M.engineStartupMaxRpm) == "number" then
		maxTargetRpm = min(maxTargetRpm, M.engineStartupMaxRpm)
	end

	local maxMG1AVForMaxPowerTorque = gearbox.mg1PowerRating * 1000 / torqueData.maxEngineGeneratorTorque

	if misc.ecvtDriveMode == 0 then -- don't allow engine to rev up so far that MG1 can't apply the required torque
		local maxInputAVForMaxPowerTorque = gearbox.psd:getCarrierAV(maxMG1AVForMaxPowerTorque, nil) * gearbox.inputGearRatio

		maxTargetRpm = max(minTargetRpm, min(maxTargetRpm, maxInputAVForMaxPowerTorque * constants.avToRPM))
	else -- make sure engine is revved up far enough that MG1 can apply the required torque
		local minInputAVForMaxPowerTorque = gearbox.psd:getCarrierAV(-maxMG1AVForMaxPowerTorque, nil) * gearbox.inputGearRatio

		minTargetRpm = min(maxTargetRpm, max(minTargetRpm, minInputAVForMaxPowerTorque * constants.avToRPM))
	end

	engineState.minTargetRpm = minTargetRpm
	engineState.maxTargetRpm = maxTargetRpm

	-- Calculate actual target RPM
	local targetRpm

	if engineState.isRunning or engineState.isStarting then
		if M.useEngineTorqueCurve then
			-- Use auto RPM calc based on the engine's torque curve
			targetRpm = engineutil.getBestRpmForTorque(engine, torqueData.requestedEngineTorque)
		else
			targetRpm = M.engineRpmVsTorque:getValue(torqueData.requestedEngineTorque)
		end

		-- If "minRpmVsThrottle" is present, apply it too
		if M.minRpmVsThrottle then
			targetRpm = max(targetRpm, M.minRpmVsThrottle:getValue(misc.posThrottle * 100))
		end

		if misc.launchControl then
			targetRpm = min(M.maxLaunchControlRpm, targetRpm)
		end

		targetRpm = clamp(targetRpm, minTargetRpm, maxTargetRpm)
	else
		targetRpm = 0
	end

	-- Apply smoother to target RPM
	engineState.targetRpm = M.targetEngineRpmSmoother:get(targetRpm, dt)

	-- Apply engine startup smoothing
	if engineState.timeSinceStart < M.engineStartupIdleTime and M.engineStartupIdleTime > 1e-30 then
		local idleTimeCoef = clamp(engineState.timeSinceStart / M.engineStartupIdleTime)

		engineState.targetRpm = blend(engineState.minRpm, engineState.targetRpm, idleTimeCoef)
	end
end

local function updateEngineLoadTorque(dt)
	local rpmError = 0
	local loadTorque = 0
	local loadBiasTorque = 0

	if engine then
		-- Calculate load bias based on RPM error

		rpmError = engineState.targetRpm - engineState.rpm

		local rpmErrorCoef = clamp(rpmError / (rpmError > 0 and M.engineLoadRpmToleranceOut or M.engineLoadRpmToleranceIn), -1, 1)
		local loadBiasCoef

		if misc.ecvtDriveMode == 1 then
			loadBiasCoef = rpmError > 0 and M.engineLoadBiasCoefOutModeOne or M.engineLoadBiasCoefInModeOne
		else
			loadBiasCoef = rpmError > 0 and M.engineLoadBiasCoefOut or M.engineLoadBiasCoefIn
		end

		local currentMaxEngineTorque = torqueData.engineTorqueCurve[round(max(engineState.rpm, engine.idleRPM))] or 0
		local currentMaxEngineLoad = currentMaxEngineTorque * torqueData.engineToSunGearRatio

		loadBiasTorque = currentMaxEngineLoad * clamp(loadBiasCoef * rpmErrorCoef, -1, engineState.startupCoef)

		-- Calculate load torque
		local clampedEngineTorque = min(torqueData.requestedEngineTorque, currentMaxEngineTorque)

		loadTorque = clampedEngineTorque * torqueData.engineToSunGearRatio

		-- Limit engine load based on certain criteria
		local maxEngineLoadStall = maprange(electrics.values.rpm, engine.idleRPM - 50, engine.idleRPM)

		if loadTorque > 0 then
			loadTorque = loadTorque * maxEngineLoadStall
		end
	end

	engineState.rpmError = rpmError
	engineState.loadBiasTorque = loadBiasTorque
	engineState.loadTorque = M.engineLoadSmoother:get(loadTorque)
	engineState.appliedLoadTorque = (engineState.loadTorque * engineState.startupCoef) - engineState.loadBiasTorque
end

local function updateEngineThrottle(dt)
	if not (engineState.isRunning or engineState.isStarting) or not M.powerOn then
		M.driveThrottlePid:reset()
	elseif misc.gear == "N" then
		engineState.targetThrottle = misc.throttle
	else
		-- Engine throttle is controlled by the desired output torque of the engine
		-- Engine RPM is controlled by the throttle pedal
		-- Actual load on the engine is biased to achieve the desired RPM

		if torqueData.requestedEngineTorque < 0.1 and not engineState.isStarting then
			M.driveThrottlePid:reset(true)
		end

		local feedforwardCoef = engineState.rpmError > 0 and M.engineThrottleFeedforwardCoefOut or M.engineThrottleFeedforwardCoefIn
		local feedforwardControl = feedforwardCoef * engineState.rpmError
		local pidControl = M.driveThrottlePid:get(torqueData.requestedEngineTorque * engineState.startupCoef, engineState.combustionTorque, dt)
		local rpmThrottleLimit = maprange(engineState.rpm, M.engineMaxRpm, M.engineMaxRpm + M.engineLoadRpmToleranceIn, 1, 0)

		if engineState.isStarting then
			engineState.targetThrottle = M.engineStartupThrottle
		else
			engineState.targetThrottle = min(feedforwardControl + pidControl, rpmThrottleLimit)
		end

		-- determine by how much we should de-load the engine, if at all

		local maxDeloadCoefChargePower = maprange(power.requestedChargePower, 5, 0)
		local maxDeloadCoefSpeed = maprange(misc.speedMph, 1, 10)
		local maxDeloadCoefEngineTorque = maprange(torqueData.requestedEngineTorque, 5, 1)
		local deloadCoef = mathutil.minall(maxDeloadCoefChargePower, maxDeloadCoefSpeed, maxDeloadCoefEngineTorque)

		if deloadCoef > 1e-15 then -- blend with deloadCoef
			engineState.targetThrottle = blend(engineState.targetThrottle, 0, deloadCoef)
		end

		debugValues.feedforwardControl = feedforwardControl
	end

	electrics.values.iceThrottle = M.engineThrottleSmoother:get(engineState.targetThrottle)
end

local function updateGearbox(dt)
	local engineStartupShutdownTorque = 0

	-- Figure out if we need to apply torque to the engine to start or stop it
	if engineState.current ~= engineState.last then
		if engineState.isStopping then
			-- Reset the starter smoother to the engine load torque. When the engine transitions to
			-- stopping, this will cause the transition to be smooth since the load torque will
			-- abruptly jump to 0

			M.engineStarterSmoother:set(-engineState.loadTorque)
		elseif engineState.isStarting then
			M.engineStarterSmoother:reset()
		end
	end

	if engineState.isStarting then
		engineStartupShutdownTorque = engineState.starterTorque
	elseif engineState.isStopping then
		-- Shut down the engine faster by applying load to it as it spins down
		engineStartupShutdownTorque = -getEngineShutdownTorque()
	elseif engineState.startupCoef >= 0.99 then
		M.engineStarterSmoother:reset()
	end

	engineStartupShutdownTorque = M.engineStarterSmoother:get(engineStartupShutdownTorque, dt)

	if M.powerOn then
		local neededMG1Torque = engineStartupShutdownTorque
		local neededMG2Torque = 0

		if engine and M.powerOn and abs(engineStartupShutdownTorque) > 1e-20 and misc.speedMph > 1 then
			-- While the engine is starting or stopping, MG2 should try and offset the torque
			-- caused by MG1 while it's turning the engine over or bringing it to 0 RPM
			local mg2TorqueCompensationICE = -gearbox.torqueDiff * gearbox.inputGearRatio * gearbox.psd.ringCarrierRatio * gearbox.counterRingGearRatio / gearbox.counterMG2GearRatio
			local mg2TorqueCompensationMG1 = (-engineStartupShutdownTorque / gearbox.psd.sunRingRatio) * gearbox.counterRingGearRatio / gearbox.counterMG2GearRatio
			-- local mg2TorqueCompensation = mg2TorqueCompensationICE - mg2TorqueCompensationMG1
			local mg2TorqueCompensation = mg2TorqueCompensationICE
			-- local mg2TorqueCompensation = mg2TorqueCompensationMG1
			local directionModifier = misc.gear == "R" and -1 or 1

			if engineStartupShutdownTorque > 0 then
				mg2TorqueCompensation = max(0, mg2TorqueCompensation)
			else
				mg2TorqueCompensation = min(0, mg2TorqueCompensation)
			end

			neededMG2Torque = neededMG2Torque + mg2TorqueCompensation * directionModifier
			debugValues.starterCompensationTorque = mg2TorqueCompensation
		else
			debugValues.starterCompensationTorque = 0
		end

		if hybridState.current == "evmode" then
			-- Use both motors to achieve the requested propulsion torque

			local neededMG2TorqueToOutput = torqueData.requestedElectricOutputTorque - braking.requestedRegenTorque
			local achievableMG2TorqueToOutput = clamp(neededMG2TorqueToOutput, -braking.maxRegenTorque, torqueData.maxElectricTorque)
			local neededMG1CompensationToOutput = misc.gear == "R" and 0 or max(0, neededMG2TorqueToOutput - achievableMG2TorqueToOutput)

			neededMG1Torque = neededMG1Torque - neededMG1CompensationToOutput / torqueData.cumulativeMG1GearRatio
			neededMG2Torque = neededMG2Torque + achievableMG2TorqueToOutput / gearbox.counterMG2GearRatio

			if type(M.maxEvModeMG1Torque) == "number" then
				neededMG1Torque = min(M.maxEvModeMG1Torque, neededMG1Torque)
			end
		else
			local neededMG2TorqueToOutput = torqueData.requestedElectricOutputTorque - braking.requestedRegenTorque
			local achievableMG2TorqueToOutput = clamp(neededMG2TorqueToOutput, -braking.maxRegenTorque, torqueData.maxElectricTorque)

			-- Determine MG1 torque based on engine load and engine starting/stopping
			neededMG1Torque = neededMG1Torque - engineState.appliedLoadTorque

			-- Determine MG2 torque based on acceleration/deceleration demand
			neededMG2Torque = neededMG2Torque + achievableMG2TorqueToOutput / gearbox.counterMG2GearRatio
		end

		-- Determine which drive mode we should be in next based on whether or not MG1 can provide generation drag
		local modeZeroLatchTest = gearbox.psd.sunGearAV > gearbox.generatorMinFullLoadAV * 3
		local modeOneLatchTest = gearbox.psd.sunGearAV <= gearbox.generatorMinFullLoadAV * 1.5

		if M.latchDriveModeZero:test(modeZeroLatchTest, dt) then
			if misc.ecvtDriveMode == 1 then
				M.latchDriveModeOne:reset()
				misc.ecvtDriveMode = 0
			end
		end
		if M.latchDriveModeOne:test(modeOneLatchTest, dt) then
			if misc.ecvtDriveMode == 0 then
				M.latchDriveModeZero:reset()
				misc.ecvtDriveMode = 1
			end
		end

		-- Figure out how to apply MG1 torque
		if misc.ecvtDriveMode == 0 then
			-- MG1 is spinning forwards fast enough for generator drag
			-- negative torque = power generation, positive torque = power consumption

			torqueData.targetMG1Direction = 1
			torqueData.targetMG1Torque = neededMG1Torque
		else
			-- MG1 spinning too slow for generator drag, or spinning backwards

			if gearbox.psd.sunGearAV <= -gearbox.generatorMinFullLoadAV * 1.5 then
				-- negative torque = power consumption, positive torque = power generation

				torqueData.targetMG1Torque = -neededMG1Torque
				torqueData.targetMG1Direction = -1
			else
				-- negative and positive torque = power consumption

				torqueData.targetMG1Torque = abs(neededMG1Torque)
				torqueData.targetMG1Direction = sign(neededMG1Torque)
			end
		end

		-- MG2 torque is applied directly
		torqueData.targetMG2Torque = neededMG2Torque
	else
		local neededMG1Torque = engineStartupShutdownTorque

		torqueData.targetMG1Torque = abs(neededMG1Torque)
		torqueData.targetMG1Direction = sign(neededMG1Torque)
		torqueData.targetMG2Torque = 0
	end
end

local function getRequestedChargePower(dt)
	local chargePowerStopped = 0
	local chargePowerDriving = 0

	-- Calculate charge power if stopped
	if misc.launchControl then
		chargePowerStopped = M.idlePowerDraw + M.chargeModePower
	elseif M.latchBatteryChargeUp:get() then
		chargePowerStopped = M.idlePowerDraw + max(M.chargeupMinPower, misc.isParked and M.maxChargePowerParked or 0.0)
	else
		local chargePowerParked = maprange(power.batteryLevel, 0.5, max(0, M.batteryChargeLowRatio), M.minChargePowerParked, M.maxChargePowerParked)

		chargePowerStopped = M.idlePowerDraw + chargePowerParked
	end

	if misc.chargeModeEnabled then
		chargePowerStopped = max(chargePowerStopped, M.chargeModePower)
	end

	-- Calculate charge power if driving
	local chargePowerThrottleCoef = M.chargePowerCoefVsThrottle:getValue(misc.posThrottle * 100)
	local chargePowerBatteryCoef = M.chargePowerCoefVsBatteryLevel:getValue(power.batteryLevel * 100)
	local chargePowerRegenCoef = maprange(braking.desiredRegen, 0, 1, 1, 0)
	local chargePowerReverseCoef = misc.gear == "R" and 0 or 1
	local priorityPowerExceededBy = M.engineEngagementChargePowerFilter:get(electrics.values.kw - power.availablePriorityEvPower)
	local chargePowerEngineEngagedCoef = clamp(priorityPowerExceededBy / M.engineEngagementChargePowerThreshold)
	local chargePowerDrivingCoef = chargePowerThrottleCoef * chargePowerBatteryCoef * chargePowerRegenCoef * chargePowerReverseCoef * chargePowerEngineEngagedCoef
	local maxChargePowerDriving = M.maxChargePowerVsSpeed:getValue(misc.speedMph) * chargePowerDrivingCoef

	if misc.chargeModeEnabled then
		local chargeModeThrottleCoef = maprange(misc.posThrottle, 0.9, 1, 1, 0)
		local chargeModeCoef = chargeModeThrottleCoef * chargePowerRegenCoef * chargePowerReverseCoef

		maxChargePowerDriving = max(maxChargePowerDriving, M.chargeModePower * chargeModeCoef)
	end

	chargePowerDriving = M.idlePowerDraw + maxChargePowerDriving

	-- Blend the two together
	local overallChargePowerCoef = maprange(power.batteryLevel, 1, 0.95)

	return overallChargePowerCoef * maprange(misc.speedMph, 0, M.maxCreepSpeed, chargePowerStopped, chargePowerDriving)
end

local function updateRequestedTorqueInDriveGear(dt)
	-- Update while in a driving gear (anything other than Park or Neutral)

	local availablePriorityEvOutputTorque = 0
	local availableSecondaryEvOutputTorque = 0

	-- Map throttle to total capable torque output
	torqueData.requestedTotalOutputTorque = min(misc.posThrottle * torqueData.maxThrottleMappedTorque, torqueData.maxCombinedTorque)

	if misc.launchControl then
		torqueData.requestedTotalOutputTorque = min(M.maxLaunchControlTorque, torqueData.requestedTotalOutputTorque)
	end

	-- Calculate available EV torque and distribute torque demand between motors and engine
	local mg1AVForPowerEquations = max(abs(gearbox.motorGenerator1AV), gearbox.minPowerAV)
	local mg2AVForPowerEquations = max(abs(gearbox.motorGenerator2AV), gearbox.minPowerAV)

	if hybridState.current == "evmode" then
		local gearboxMaxMG1Torque = gearbox:getMaxMG1Torque(power.availablePriorityEvPower, mg1AVForPowerEquations)
		local availableMG1OutputTorque = gearboxMaxMG1Torque * torqueData.cumulativeMG1GearRatio
		local availableMG2OutputTorque = gearbox:getMaxMG2Torque(power.availablePriorityEvPower, mg2AVForPowerEquations) * gearbox.counterMG2GearRatio

		availablePriorityEvOutputTorque = availableMG1OutputTorque + availableMG2OutputTorque
		availableSecondaryEvOutputTorque = availablePriorityEvOutputTorque
	else
		local availablePriorityPower = max(0, power.availablePriorityEvPower - power.requestedChargePower)
		local availableSecondaryPower = max(0, power.availableSecondaryEvPower - power.requestedChargePower)

		availablePriorityEvOutputTorque = gearbox:getMaxMG2Torque(availablePriorityPower, mg2AVForPowerEquations) * gearbox.counterMG2GearRatio
		availableSecondaryEvOutputTorque = gearbox:getMaxMG2Torque(availableSecondaryPower, mg2AVForPowerEquations) * gearbox.counterMG2GearRatio
	end

	local torqueStillNeeded = torqueData.requestedTotalOutputTorque * gearbox.throttleFactor -- TCS will set throttleFactor to limit output power during slip

	if misc.brakeInput < 0.1 and electrics.values.parkingbrake < 1 and misc.gear ~= "P" and misc.gear ~= "N" then
		local maxCreepThrottle = maprange(misc.speedMph, 0, M.maxCreepSpeed, M.evCreepThrottle, 0)

		misc.creepThrottle = min(maxCreepThrottle, misc.creepThrottle + maxCreepThrottle * dt / 0.5)
	else
		misc.creepThrottle = 0
	end

	local creepTorque = misc.creepThrottle * torqueData.maxThrottleMappedTorque

	torqueData.requestedElectricOutputTorque = creepTorque

	if engine then
		-- TOO MANY CALCULATIONS, SEND HELP

		if torqueStillNeeded > torqueData.maxElectricTorque then
			requestEngine("electric torque exhausted")
		end

		-- Try and distribute torque demand to engine in "balanced mode" if it's running,
		-- where the power consumption of MG2 equals the power generated by MG1, with a
		-- bias depending on the SOC of the hybrid battery

		local tO  -- N•m   - Desired output torque
		local rI  --       - Input gear ratio (carrier : engine)
		local rSC --       - Sun:Carrier gear ratio
		local crE --       - Cumulative Output:Engine gear ratio
		local cr2 --       - Cumulative Output:MG2 gear ratio
		local av1 -- rad/s - Signed angular velocity of MG1
		local av2 -- rad/s - Abs angular velocity of MG2
		local pP  -- kW    - Max priority power available to achieve requested torque w/ electric power
		local dP  -- kW    - Desired battery power delta (+ is drain, - is charge)
		local tE  -- N•m   - Desired engine torque (raw, into gearbox)
		local t2  -- N•m   - Desired MG2 torque

		-- Set up variable values
		tO = max(0, torqueStillNeeded)
		rI = gearbox.inputGearRatio
		rSC = gearbox.psd.sunCarrierRatio
		crE = torqueData.engineOutputGearRatio
		cr2 = torqueData.cumulativeMG2GearRatio
		av1 = gearbox.motorGenerator1AV
		av2 = max(1, abs(gearbox.motorGenerator2AV))
		pP = min(torqueStillNeeded, availablePriorityEvOutputTorque) / gearbox.counterMG2GearRatio * av2
		dP = pP - (power.requestedChargePower * engineState.startupCoef * 1000)

		-- Clamp MG1 AV to minimum it would be if engine was idling right now
		local engineIdleAV1 = gearbox.psd:getSunAV(nil, engineState.minRpm * constants.rpmToAV / gearbox.inputGearRatio)

		av1 = max(av1, engineIdleAV1)

		-- warning: super ugly equations below
		-- tE and t2 equations are derived from the following:
		--   Net output torque: tO = tE * crE + t2 * cr2
		--   Net power delta:   dP = t2 * av2 - tE * rI * rSC * av1

		-- Perform initial calculations
		-- local useEngineTorque = engineState.isStarting or engineState.isRunning
		local useEngineTorque = engineState.isRunning

		tE = clamp((tO - cr2 * dP / av2) / (crE + ((rI * rSC * cr2 * av1) / av2)), 0, torqueData.maxEngineTorque)

		if useEngineTorque then
			t2 = (dP + tE * rI * rSC * av1) / av2
		else
			t2 = dP / av2
		end

		-- Clamp t2 to our known bounds and recalculate tE, in case MG2 runs out of torque
		-- △tE can be recalculated using another derivation from above:
		--   △tE = (△tO - △t2 * cr2) / crE where △t2 in this case is the delta between clamped and unclamped
		--   (△tO is always zero; we want tO to remain the same)
		local clampedT2 = clamp(t2, -torqueData.gearboxMaxMG2RegenTorque, torqueData.gearboxMaxMG2Torque)
		local t2Delta = min(0, clampedT2 - t2)
		local tEDelta = -t2Delta * cr2 / crE
		local clampedTE = clamp(tE + tEDelta, 0, torqueData.maxEngineTorque)

		torqueData.requestedEngineTorque = clampedTE

		local engineOutputTorque = crE * torqueData.requestedEngineTorque
		local electricOutputTorque = cr2 * clampedT2

		torqueData.requestedElectricOutputTorque = torqueData.requestedElectricOutputTorque + electricOutputTorque

		-- Count engine's assumed output torque towards the torque deficit as well as electric
		torqueStillNeeded = max(0, torqueStillNeeded - torqueData.requestedElectricOutputTorque - (useEngineTorque and engineOutputTorque or 0))

		-- While the engine is working on revving up, the main drive motor should compensate for it
		-- (using secondary power) so that the driver feels a more immediate acceleration response.
		-- It's okay if we do this even if the battery is low (it's a short period of time it will be drawing power)
		if M.engineLagCompensationEnabled and misc.gear ~= "R" and engineState.isRunning then
			local minLagCompensation = -torqueData.gearboxMaxMG2RegenTorque * torqueData.cumulativeMG2GearRatio
			local maxLagCompensation = torqueData.fullBatteryElectricTorque

			if type(M.maxEngineLagCompensationTorque) == "number" then
				maxLagCompensation = min(maxLagCompensation, M.maxEngineLagCompensationTorque)
			end

			local engineOutputTorqueDiscrepancy = (torqueData.requestedEngineTorque - engine.outputTorque1) * torqueData.engineOutputGearRatio
			local engineLagCompensation = clamp(engineOutputTorqueDiscrepancy, minLagCompensation, maxLagCompensation)

			torqueData.requestedElectricOutputTorque = torqueData.requestedElectricOutputTorque + engineLagCompensation
			debugValues.engineLagCompensationTorque = engineLagCompensation
		else
			debugValues.engineLagCompensationTorque = 0
		end

		-- Populate debug proxy objects
		if M.debug.enabled then
			debugValues.requestedEngineOutputTorque = engineOutputTorque
		end

		if M.synergydebug.enabled then
			synergyDebugValues.tO = tO
			synergyDebugValues.rI = rI
			synergyDebugValues.rSC = rSC
			synergyDebugValues.crE = crE
			synergyDebugValues.cr2 = cr2
			synergyDebugValues.av1 = av1
			synergyDebugValues.av2 = av2
			synergyDebugValues.pP = pP
			synergyDebugValues.dP = dP
			synergyDebugValues.tE = tE
			synergyDebugValues.t2 = t2
			synergyDebugValues.clampedT2 = clampedT2
			synergyDebugValues.t2Delta = t2Delta
			synergyDebugValues.tEDelta = tEDelta
			synergyDebugValues.engineOutputTorque = engineOutputTorque
			synergyDebugValues.electricOutputTorque = electricOutputTorque
		end
	else
		-- No engine; just electric torque

		torqueData.requestedElectricOutputTorque = min(torqueStillNeeded, availablePriorityEvOutputTorque) + creepTorque
		torqueStillNeeded = max(0, torqueStillNeeded - torqueData.requestedElectricOutputTorque)

		-- Populate debug proxy objects
		if M.debug.enabled then
			debugValues.requestedEngineOutputTorque = 0
			debugValues.engineLagCompensationTorque = 0
		end

		if M.synergydebug.enabled then
			synergyDebugValues.tO = 0
			synergyDebugValues.rI = 0
			synergyDebugValues.rSC = 0
			synergyDebugValues.crE = 0
			synergyDebugValues.cr2 = 0
			synergyDebugValues.av1 = 0
			synergyDebugValues.av2 = 0
			synergyDebugValues.pP = 0
			synergyDebugValues.dP = 0
			synergyDebugValues.tE = 0
			synergyDebugValues.t2 = 0
			synergyDebugValues.clampedT2 = 0
			synergyDebugValues.t2Delta = 0
			synergyDebugValues.tEDelta = 0
			synergyDebugValues.engineOutputTorque = 0
			synergyDebugValues.electricOutputTorque = 0
		end
	end

	-- Distribute as much remaining torque demand as we can to secondary EV power
	local secondaryTorqueApplied = min(torqueStillNeeded, availableSecondaryEvOutputTorque)

	torqueData.requestedElectricOutputTorque = min(torqueData.requestedElectricOutputTorque + secondaryTorqueApplied, torqueData.maxElectricTorque)
	torqueStillNeeded = torqueStillNeeded - secondaryTorqueApplied

	if engine then
		-- Make sure engine starts if we're out of electric power
		if misc.gear ~= "R" and (torqueStillNeeded > 1e-20 or torqueData.requestedElectricOutputTorque > torqueData.maxElectricTorque) then
			requestEngine("electric torque exhausted")
		end
	end

	debugValues.availablePriorityEvOutputTorque = availablePriorityEvOutputTorque
	debugValues.availableSecondaryEvOutputTorque = availableSecondaryEvOutputTorque
end

local function updateRequestedTorqueInPark(dt)
	if engineState.isRunning then
		local throttleTorque = maprange(misc.posThrottle, M.evCreepThrottle, 1, 0, M.maxParkEngineTorqueCoef * torqueData.maxEngineTorque)

		-- Apply charging torque in park
		local chargeTorqueMG1 = mathutil.kwToTorque(power.requestedChargePower / M.batteryEfficiency, gearbox.motorGenerator1AV)
		local chargeTorqueEngine = abs(gearbox.psd:getCarrierTorqueFromSunTorque(chargeTorqueMG1)) / gearbox.inputGearRatio

		torqueData.requestedEngineTorque = throttleTorque + chargeTorqueEngine
	else
		torqueData.requestedEngineTorque = 0
	end

	-- Populate debug proxy objects
	if M.debug.enabled then
		debugValues.requestedEngineOutputTorque = 0
		debugValues.engineLagCompensationTorque = 0
	end

	if M.synergydebug.enabled then
		synergyDebugValues.tO = 0
		synergyDebugValues.rI = 0
		synergyDebugValues.rSC = 0
		synergyDebugValues.av1 = 0
		synergyDebugValues.av2 = 0
		synergyDebugValues.pP = 0
		synergyDebugValues.dP = 0
		synergyDebugValues.tE = 0
		synergyDebugValues.t2 = 0
		synergyDebugValues.clampedT2 = 0
		synergyDebugValues.t2Delta = 0
		synergyDebugValues.tEDelta = 0
		synergyDebugValues.engineOutputTorque = 0
		synergyDebugValues.electricOutputTorque = 0
	end
end

local function updateHybridLogic(dt)
	-- Reset torque request variables
	torqueData.requestedTotalOutputTorque = 0
	torqueData.requestedElectricOutputTorque = 0
	torqueData.requestedEngineTorque = 0

	if engine then
		-- Calculate RPM range for engine based on required sun gear speeds
		engineState.minWorkingRpm = maprange(misc.speedMph, 0, M.maxCreepSpeed, M.engineMinIdleRpm, engine.idleRPM)
		engineState.minRpm = max(engineState.minWorkingRpm, (gearbox:getMinCarrierAV() * gearbox.inputGearRatio) * constants.avToRPM)
		engineState.maxRpm = min(M.engineMaxRpm, (gearbox:getMaxCarrierAV() * gearbox.inputGearRatio) * constants.avToRPM)
		engineState.maxRpm = max(engineState.minRpm, engineState.maxRpm)
	else
		engineState.minWorkingRpm = 0
		engineState.minRpm = 0
		engineState.maxRpm = 0
		engineState.maxRpm = 0
	end

	if misc.gear ~= "N" then
		power.requestedChargePower = getRequestedChargePower(dt)
	else
		power.requestedChargePower = 0
	end

	if misc.isInDriveGear then
		updateRequestedTorqueInDriveGear(dt)

		if misc.gear ~= "R" and not engineDemand.miscDemand then
			if misc.posThrottle > M.maxEvOnlyThrottle then
				requestEngine("ev throttle limit exceeded")
			end
			if misc.launchControl then
				requestEngine("launch control")
			end
		end
	elseif misc.gear ~= "N" then
		updateRequestedTorqueInPark(dt)

		if misc.posThrottle > 0.02 then
			requestEngine("throttle in park")
		end
	end

	updateTargetEngineRpm(dt)
	updateEngineLoadTorque(dt)
	updateEngineThrottle(dt)
end

local function updateRearMotor(dt)
	rearMotor:setMotorPowerOverride(min(rearMotor.motorPowerRating, power.availableMG2Power))

	if misc.isInDriveGear then
		local netOutputTorque = torqueData.requestedTotalOutputTorque - braking.requestedRegenTorque
		local netWheelTorque = netOutputTorque * torqueData.gearboxChildGearRatio
		local maxTorqueSplit = netWheelTorque >= 0
		                       and M.maxRearTorqueSplitVsSpeed:getValue(misc.averageWheelSpeedMph)
							   or M.maxRearRegenTorqueSplitVsSpeed:getValue(misc.averageWheelSpeedMph)
		local torqueSign = sign(netWheelTorque)
		local desiredRearWheelTorque = maxTorqueSplit * abs(netWheelTorque)
		local maxRearWheelTorque = rearMotor:getMaxMotorTorque(torqueSign < 0) * rearMotor.cumulativeGearRatio
		local maxWheelTorqueToTakeFromMG2 = abs(torqueData.targetMG2Torque) * gearbox.counterMG2GearRatio * torqueData.gearboxChildGearRatio
		local maxWheelTorqueToMove = min(maxRearWheelTorque, maxWheelTorqueToTakeFromMG2)
		local wheelTorqueToMove = min(desiredRearWheelTorque, maxWheelTorqueToMove)
		local rawTorqueToTakeFromMG2 = torqueSign * wheelTorqueToMove / (gearbox.counterMG2GearRatio * torqueData.gearboxChildGearRatio)
		local rawTorqueToSendToRear = torqueSign * wheelTorqueToMove / rearMotor.cumulativeGearRatio

		torqueData.targetMG2Torque = torqueData.targetMG2Torque - rawTorqueToTakeFromMG2
		torqueData.targetRearMotorTorque = torqueData.targetRearMotorTorque + rawTorqueToSendToRear
	else
		torqueData.targetRearMotorTorque = 0
	end
end

local function updateMotors(dt)
	local mg1PowerGenerated = -min(0, gearbox.motorGenerator1Power)
	local mg2PowerGenerated = -min(0, gearbox.motorGenerator2Power)

	-- Calculate motor power restrictions
	power.availableMG1Power = M.mg1MaxPowerSmoother:get(min(gearbox.mg1PowerRating, power.availableBatteryPower + mg2PowerGenerated), dt)
	power.availableMG2Power = M.mg2MaxPowerSmoother:get(min(gearbox.mg2PowerRating, power.availableBatteryPower + mg1PowerGenerated), dt)

	-- Hard-limit the amount of power MG1 and MG2 are allowed to consume (so they don't overdraw the battery)
	if engineState.isStarting then
		gearbox:setMG1PowerOverride(maprange(power.batteryLevelRaw, M.criticalBatteryPowerLevel, M.minBatteryPowerLevel, 0, gearbox.mg1PowerRating))
	else
		gearbox:setMG1PowerOverride(power.availableMG1Power)
	end

	gearbox:setMG2PowerOverride(power.availableMG2Power)

	-- Update AWD if present
	if rearMotor then
		-- If rear motor is present, it has the same max power as MG2
		updateRearMotor(dt)
	end

	-- Prevent negative torque demand if battery is too full to protect it
	local batteryOverchargeCoef = maprange(power.batteryLevel, 1, 0.95)

	if torqueData.targetMG1Torque < 0 then
		torqueData.targetMG1Torque = torqueData.targetMG1Torque * batteryOverchargeCoef
	end
	if torqueData.targetMG2Torque < 0 then
		torqueData.targetMG2Torque = torqueData.targetMG2Torque * batteryOverchargeCoef
	end

	-- Update requested torques
	gearbox:setMG1TorqueDirection(torqueData.targetMG1Direction)
	gearbox:setRequestedMG1Torque(torqueData.targetMG1Torque)
	gearbox:setRequestedMG2Torque(torqueData.targetMG2Torque)

	if rearMotor then
		rearMotor:setRequestedOutputTorque(torqueData.targetRearMotorTorque)
		rearMotor:setMotorDirection(misc.gear == "R" and -1 or 1)
	end
end

local function updateFixedStep(dt)
	calculateElectricLimits(dt)
	calculateTorqueStatistics(dt)
	updateValues(dt)

	hybridState:update(dt)

	-- Once we know we have all the required components for running,
	-- we can start to perform our (very complex and abundant) hybrid
	-- calculations

	if M.powerOn then
		updateHybridLogic(dt)
	end

	updateBrakesGlobal(dt)
	updateGearbox(dt)
	updateMotors(dt)
end

local function updateGFX(dt)
	updateDefaultDebugValues()

	if misc.gear == "N" then
		M.driveThrottlePid:reset()

		if engine then
			M.targetEngineRpmSmoother:set(engine.idleRPM)
		end
	end

	if engine then
		if power.batteryLevelRaw < M.criticalBatteryPowerLevel and M.hasAuxiliaryStarterMotor then
			-- Allow falling back to 12V starter when vehicle is powered on

			engine.starterDisabled = false
		else
			-- prevent engine starter from working (since the eCVT starts it)

			engine:deactivateStarter()
			engine.starterDisabled = true
		end
	end

	if not M.powerSwitch and hybridState.current ~= "poweroff" and hybridState.current ~= "shutdown" then
		hybridState:changeTo("shutdown")
	end

	updateValuesGFX(dt)
	updateBattery(dt)
	updateGUI(dt)
	updateEngineDemand()

	hybridState:updateGFX(dt)

	updateDebugValues(dt)
end

local function applyControlParameters()
	M.throttleCurveExponent = controlParameters.hybrid.throttleCurveExponent
end

local function init(jbeamData)
	M.timeSinceVehicleLoaded = 0

	M.jbeamData = jbeamData

	-- Debug utilities
	local mainDebugName
	local synergyDebugName

	if jbeamData.name and jbeamData.name ~= "hybridPowertrain" then
		mainDebugName = string.format("Hybrid Powertrain - %s", jbeamData.name)
		synergyDebugName = string.format("Hybrid Synergy Logic - %s", jbeamData.name)
	else
		mainDebugName = "Hybrid Powertrain"
		synergyDebugName = "Hybrid Synergy Logic"
	end

	if M.debug then M.debug:dispose() end
	if M.synergydebug then M.synergydebug:dispose() end

	M.debug = DebugModule:new(mainDebugName, jbeamData.enableDebug)
	M.synergydebug = DebugModule:new(synergyDebugName, jbeamData.enableSynergyDebug)
	M.debugInitialized = false

	-- Power/startup parameters
	local startPoweredOn = (electrics.values.ignitionLevel or 0) >= 2

	M.startEvMode = jbeamData.startEvMode == true
	M.forceEvMode = jbeamData.forceEvMode == true
	M.startPoweredOff = not startPoweredOn
	M.powerSwitch = startPoweredOn
	M.powerOn = startPoweredOn
	M.initializeTime = jbeamData.initializeTime or 1.25
	M.vehicleStartupEngineDelay = jbeamData.vehicleStartupEngineDelay or -1
	M.vehicleStartupEngineRuntime = jbeamData.vehicleStartupEngineRuntime or 15
	M.hasAuxiliaryStarterMotor = jbeamData.hasAuxiliaryStarterMotor or false

	-- EV-mode parameters
	M.evCreepThrottle = jbeamData.evCreepThrottle or 0.25
	M.maxCreepSpeed = jbeamData.maxCreepSpeed or 5
	M.maxEvOnlySecondaryPower = jbeamData.maxEvOnlySecondaryPower or 25 -- kW
	M.maxEvOnlySpeed = jbeamData.maxEvOnlySpeed or 65 -- MPH
	M.maxEvOnlyThrottle = jbeamData.maxEvOnlyThrottle or 0.65 -- ratio
	M.maxEvOnlyThrottleAggressiveness = jbeamData.maxEvOnlyThrottleAggressiveness or 20
	M.maxEvOnlyPitch = jbeamData.maxEvOnlyPitch or 180 -- degrees pitch. 180 by default; will never happen (effectively disabled)
	M.maxEvModeMG1Torque = jbeamData.maxEvModeMG1Torque

	-- Regen parameters
	M.maxRegenTorque = jbeamData.maxRegenTorque or 1000
	M.regenWheelslipDisableTime = jbeamData.regenWheelslipDisableTime or 0.5
	M.regenWheelslipReenableTime = jbeamData.regenWheelslipReenableTime or 1.0
	M.maxWheelSlipCoefForRegen = jbeamData.maxWheelSlipCoefForRegen or 1
	M.evCoastRegen = jbeamData.evCoastRegen or 0.05

	-- Power/battery parameters
	M.idlePowerDraw = jbeamData.idlePowerDraw or 1 -- kW
	M.kwGaugeEcoPower = jbeamData.kwGaugeEcoPower -- kW
	M.kwGaugeMaxPower = jbeamData.kwGaugeMaxPower -- kW
	M.kwGaugeRegenPower = jbeamData.kwGaugeRegenPower -- kW
	M.minBatteryPowerLevel = jbeamData.minBatteryPowerLevel or 0.25 -- ratio; low end of max-kW slope
	M.maxBatteryPowerLevel = jbeamData.maxBatteryPowerLevel or 0.85 -- ratio; high end of max-kW slope
	M.criticalBatteryPowerLevel = jbeamData.criticalBatteryPowerLevel or 0.05 -- raw state-of-charge
	M.batteryChargeLowRatio = jbeamData.batteryChargeLowRatio or 0.15
	M.batteryChargeHighRatio = jbeamData.batteryChargeHighRatio or 0.25
	M.minChargePowerParked = jbeamData.minChargePowerParked or 0
	M.maxChargePowerParked = jbeamData.maxChargePowerParked or 5
	M.chargeupMinPower = jbeamData.chargeupMinPower or 3
	M.allowChargeMode = jbeamData.allowChargeMode ~= false
	M.chargeModePower = jbeamData.chargeModePower or 10
	M.chargeModeUpperThreshold = jbeamData.chargeModeUpperThreshold or 0.8

	-- Engine auto-stop parameters
	M.enableAutoStop = jbeamData.enableAutoStop ~= false
	M.autoStopDelayTime = jbeamData.autoStopDelayTime or 0.05
	M.autoStopBrakeThreshold = jbeamData.autoStopBrakeThreshold
	M.autoStopBrakeTime = jbeamData.autoStopBrakeTime or 0
	M.engineMinRunTime = jbeamData.engineMinRunTime or 2 -- seconds
	M.minAutoStopCoolantTemp = jbeamData.minAutoStopCoolantTemp or 30
	M.minAutoStopCoolantTempWarmedUp = jbeamData.minAutoStopCoolantTempWarmedUp or 57
	M.autoStopThrottleThreshold = jbeamData.autoStopThrottleThreshold or 0.005

	-- Engine parameters
	M.engineMinIdleRpm = jbeamData.engineMinIdleRpm
	M.engineMaxRpm = jbeamData.engineMaxRpm
	M.maxLaunchControlRpm = jbeamData.maxLaunchControlRpm or M.engineMaxRpm
	M.useEngineTorqueCurve = jbeamData.useEngineTorqueCurve
	M.engineStartupIdleTime = jbeamData.engineStartupIdleTime or 0.15
	M.engineStartupMaxRpm = jbeamData.engineStartupMaxRpm
	M.engineStartupRpmThreshold = jbeamData.engineStartupRpmThreshold or 100
	M.engineStartupThrottle = jbeamData.engineStartupThrottle or 0.065
	M.engineStartupLoadThreshold = jbeamData.engineStartupLoadThreshold or 0.02
	M.engineStarterTorque = jbeamData.engineStarterTorque or 50
	M.engineSpooldownTorque = jbeamData.engineSpooldownTorque or 20
	M.idealEngineLoad = jbeamData.idealEngineLoad
	M.engineLoadRpmToleranceIn = jbeamData.engineLoadRpmToleranceIn or jbeamData.engineLoadRpmTolerance or 300
	M.engineLoadRpmToleranceOut = jbeamData.engineLoadRpmToleranceOut or jbeamData.engineLoadRpmTolerance or 300
	M.engineLoadBiasCoefIn = jbeamData.engineLoadBiasCoefIn or jbeamData.engineLoadBiasCoef or 0.75
	M.engineLoadBiasCoefOut = jbeamData.engineLoadBiasCoefOut or jbeamData.engineLoadBiasCoef or 0.65
	M.engineLoadBiasCoefInModeOne = jbeamData.engineLoadBiasCoefInModeOne or M.engineLoadBiasCoefIn
	M.engineLoadBiasCoefOutModeOne = jbeamData.engineLoadBiasCoefOutModeOne or M.engineLoadBiasCoefOut
	M.engineEngagementChargePowerThreshold = jbeamData.engineEngagementChargePowerThreshold or 20 -- kW; related to priorityPower and determines how/when engine engages for torque when running
	M.engineLagCompensationEnabled = jbeamData.engineLagCompensationEnabled ~= false
	M.maxEngineLagCompensationTorque = jbeamData.maxEngineLagCompensationTorque
	M.maxParkEngineTorqueCoef = jbeamData.maxParkEngineTorqueCoef or 0.5

	-- Throttle parameters
	M.engineThrottleFeedforwardCoefOut = jbeamData.engineThrottleFeedforwardCoefOut or jbeamData.engineThrottleFeedforwardCoef or (1 / 2000)
	M.engineThrottleFeedforwardCoefIn = jbeamData.engineThrottleFeedforwardCoefIn or jbeamData.engineThrottleFeedforwardCoef or (1 / 2000)

	if jbeamData.throttleCurveExponent then
		controlParameters.hybrid.throttleCurveExponent = jbeamData.throttleCurveExponent
	end

	-- Output torque parameters
	M.maxLaunchControlTorque = jbeamData.maxLaunchControlTorque or 50
	M.reducedPropulsionTorqueCoef = jbeamData.reducedPropulsionTorqueCoef or 0.5

	-- Startup/shutdown sound parameters
	M.startupSampleName = jbeamData.startupSampleName or "file:>vehicles>hybrid_common>sounds>hybrid_startup.ogg"
	M.startupSoundBaseVolume = jbeamData.startupSoundBaseVolume or 0.25
	M.shutdownSampleName = jbeamData.shutdownSampleName or "file:>vehicles>hybrid_common>sounds>hybrid_shutdown.ogg"
	M.shutdownSoundBaseVolume = jbeamData.shutdownSoundBaseVolume or 0.25
	M.interiorStartupSampleName = jbeamData.interiorStartupSampleName
	M.interiorStartupSoundBaseVolume = jbeamData.interiorStartupSoundBaseVolume or M.startupSoundBaseVolume
	M.interiorShutdownSampleName = jbeamData.interiorShutdownSampleName
	M.interiorShutdownSoundBaseVolume = jbeamData.interiorShutdownSoundBaseVolume or M.shutdownSoundBaseVolume

	if jbeamData.interiorSoundNode_nodes and type(jbeamData.interiorSoundNode_nodes) == "table" then
		M.interiorSoundNode = jbeamData.interiorSoundNode_nodes[1]

		if type(M.interiorSoundNode) ~= "number" then
			M.debug:log("W", ("interiorSoundNode %q not found"):format(M.interiorSoundNode))
			M.interiorSoundNode = nil
		end
	end

	-- Various curves
	if jbeamData.engineRpmVsTorque then
		M.engineRpmVsTorque = SimpleCurve:new{jbeamData.engineRpmVsTorque, "torque", "rpm"}
	end

	if jbeamData.minRpmVsThrottle then
		M.minRpmVsThrottle = SimpleCurve:new{jbeamData.minRpmVsThrottle, "throttle", "minRpm"}
	end

	M.throttleRateVsAggressiveness = SimpleCurve:new{jbeamData.throttleRateVsAggressiveness, "aggressiveness", "rate", {
		{"aggressiveness", "rate"},
		{ alpha = 0 },
		{1, 1},
		{2, 15},
	}}
	M.maxChargePowerVsSpeed = SimpleCurve:new{jbeamData.maxChargePowerVsSpeed, "speed", "maxChargePower", {
		{"speed", "maxChargePower"},
		{0, 5},
		{25, 25},
	}}
	M.chargePowerCoefVsThrottle = SimpleCurve:new{jbeamData.chargePowerCoefVsThrottle, "throttle", "chargePowerCoef", {
		{"throttle", "chargePowerCoef"},
		{0, 0},
		{3, 1},
		{85, 1},
		{100, 0},
	}}
	M.chargePowerCoefVsBatteryLevel = SimpleCurve:new{jbeamData.chargePowerCoefVsBatteryLevel, "batteryLevel", "chargePowerCoef", {
		{"batteryLevel", "chargePowerCoef"},
		{30, 1},
		{45, 0},
	}}
	M.maxEvOnlyPriorityPowerSpeed = SimpleCurve:new{jbeamData.maxEvOnlyPriorityPowerSpeed, "speed", "power", {
		{"speed", "power"},
		{25, 30},
		{35, 20},
	}}
	M.maxEngineAssistPriorityPowerSpeed = SimpleCurve:new{jbeamData.maxEngineAssistPriorityPowerSpeed, "speed", "power", {
		{"speed", "power"},
		{15, 5},
		{30, 15},
	}}
	M.maxEngineAssistSecondaryPowerSpeed = SimpleCurve:new{jbeamData.maxEngineAssistSecondaryPowerSpeed, "speed", "power", {
		{"speed", "power"},
		{0, 20},
		{25, 20},
	}}
	M.maxPriorityPowerBatteryLevel = SimpleCurve:new{jbeamData.maxPriorityPowerBatteryLevel, "batteryLevel", "power", {
		{"batteryLevel", "power"},
		{20, 0},
		{50, 35},
		{60, 35},
		{75, 50},
	}}
	M.maxSecondaryPowerBatteryLevel = SimpleCurve:new{jbeamData.maxSecondaryPowerBatteryLevel, "batteryLevel", "power", {
		{"batteryLevel", "power"},
		{0, 0},
		{20, 40}
	}}
	M.maxRearTorqueSplitVsSpeed = SimpleCurve:new{jbeamData.maxRearTorqueSplitVsSpeed, "speed", "torqueSplit", {
		{"speed", "torqueSplit"},
		{25, 0.65},
		{60, 0.25},
	}}
	M.maxRearRegenTorqueSplitVsSpeed = SimpleCurve:new{jbeamData.maxRearRegenTorqueSplitVsSpeed, "speed", "torqueSplit", {
		{"speed", "torqueSplit"},
		{25, 0.3},
		{60, 0.1},
	}}

	if jbeamData.maxOutputTorqueVsSpeed then
		M.maxOutputTorqueVsSpeed = SimpleCurve:new{jbeamData.maxOutputTorqueVsSpeed, "speed", "maxOutputTorque"}
	end
	if jbeamData.maxReverseOutputTorqueVsSpeed then
		M.maxReverseOutputTorqueVsSpeed = SimpleCurve:new{jbeamData.maxReverseOutputTorqueVsSpeed, "speed", "maxOutputTorque"}
	end
	if jbeamData.outputTorqueBonusVsInclineAngle then
		M.outputTorqueBonusVsInclineAngle = SimpleCurve:new{jbeamData.outputTorqueBonusVsInclineAngle, "inclineAngle", "outputTorqueBonus"}
	end

	-- PIDs
	local throttleDeltaSmoother

	if type(jbeamData.throttleDeltaSmoother) == "string" then
		if jbeamData.throttleDeltaSmoother == "temporal" then
			-- have to make a proxy object since, for some reason, temporalSmoothing clamps to [-1, 1] by default
			local filter = newTemporalSmoothing(jbeamData.throttleDeltaSmoothing or 5)
			local filterProxy = {}
			filterProxy.__index = filterProxy
			setmetatable(filterProxy, getmetatable(filter))

			function filterProxy:get(sample, rate)
				return self:getUncapped(sample, rate)
			end

			setmetatable(filter, filterProxy)
			throttleDeltaSmoother = filter
		elseif jbeamData.throttleDeltaSmoother == "temporalNonLinear" then
			throttleDeltaSmoother = newTemporalSmoothingNonLinear(jbeamData.throttleDeltaSmoothing or 5)
		elseif jbeamData.throttleDeltaSmoother == "exponential" then
			throttleDeltaSmoother = newExponentialSmoothing(jbeamData.throttleDeltaSmoothing or 100)
		end
	end

	M.driveThrottlePid = Pid:new{
		kPIn = jbeamData.throttlePidPFactorIn or jbeamData.throttlePidPFactor or 0.3,
		kPOut = jbeamData.throttlePidPFactorOut or jbeamData.throttlePidPFactor or 0.3,
		kIIn = jbeamData.throttlePidIFactorIn or jbeamData.throttlePidIFactor or 1.2,
		kIOut = jbeamData.throttlePidIFactorOut or jbeamData.throttlePidIFactor or 1.5,
		kD = jbeamData.throttlePidDFactor or 0.02,
		deltaSmoother = throttleDeltaSmoother,
		scale = 1 / (jbeamData.throttlePidScale or 200),
		integralLockDeltaThreshold = jbeamData.throttlePidIntegralLockDeltaThreshold,
		maxError = jbeamData.throttlePidMaxError,
		minOutput = -1,
		maxOutput = 1,
		name = "throttle",
		debug = jbeamData.throttleDebug,
	}
	M.engineStarterPid = Pid:new{
		kP = jbeamData.engineStarterPidPFactor or 1,
		kI = jbeamData.engineStarterPidIFactor or 15,
		deltaSmoother = newTemporalSmoothingNonLinear(30),
		scale = 1 / M.engineStartupRpmThreshold,
		integralLockDeltaThreshold = jbeamData.engineStarterPidILockDeltaThreshold or 8,
		integralLockErrorThreshold = jbeamData.engineStarterPidILockErrorThreshold,
		integralLockAutoCenter = true,
		minOutput = 0,
		maxOutput = 1,
		name = "engineStarter",
		debug = jbeamData.engineStarterPidDebug,
	}

	-- Lots of temporal smoothers!
	M.throttleAggressivenessSmoother = newExponentialSmoothing(100)
	M.throttleSmoother = newTemporalSmoothingNonLinear(
		jbeamData.throttleSmoothingIn or 3,
		jbeamData.throttleSmoothingOut or 3
	)
	M.offPedalRegenSmoother = newTemporalSmoothingNonLinear(3)
	M.vehicleSpeedSmoother = newTemporalSmoothingNonLinear(jbeamData.vehicleSpeedSmoothing or 20)
	M.engineStarterSmoother = newTemporalSmoothingNonLinear(
		jbeamData.engineStarterSmoothing or jbeamData.engineStarterSmoothingIn or 100,
		jbeamData.engineStarterSmoothing or jbeamData.engineStarterSmoothingOut or 25
	)
	M.engineThrottleSmoother = newExponentialSmoothing(jbeamData.engineThrottleSmoothing or 50)
	M.mg1MaxPowerSmoother = newTemporalSmoothingNonLinear(jbeamData.motorPowerSmoothing or 25)
	M.mg2MaxPowerSmoother = newTemporalSmoothingNonLinear(jbeamData.motorPowerSmoothing or 25)
	M.priorityPowerSmoother = newTemporalSmoothingNonLinear(jbeamData.priorityPowerSmoothing or 4)
	M.engineLoadSmoother = newExponentialSmoothing(jbeamData.engineLoadSmoothing or 15)
	M.targetEngineRpmSmoother = newTemporalSmoothingNonLinear(
		jbeamData.targetEngineRpmSmoothingIn or jbeamData.targetEngineRpmSmoothing or 5,
		jbeamData.targetEngineRpmSmoothingOut or jbeamData.targetEngineRpmSmoothing or 5
	)
	M.engineOutputTorqueSmoother = newExponentialSmoothing(jbeamData.actualEngineTorqueSmoothing or 25)
	M.engineCombustionTorqueSmoother = newExponentialSmoothing(jbeamData.actualEngineTorqueSmoothing or 25)
	M.engineStartupSmoother = newTemporalSmoothing(jbeamData.engineStartupSmoothing or 2)
	M.inclineAngleSmoother = newTemporalSmoothingNonLinear(10)
	M.batteryPowerSmoother = newTemporalSmoothingNonLinear(5)

	-- State-machine devices
	M.latchDriveModeZero = TimedLatch:new(jbeamData.modeZeroLatchTime or 0.25)
	M.latchDriveModeOne = TimedLatch:new{jbeamData.modeOneLatchTime or 0.25, immediateLatchTo = {true}}
	M.latchCurrentGear = TimedLatch:new{jbeamData.gearChangeLatchTime or 0.5, electrics.values.gear or "P", {"L"}, {"L"}}
	M.latchBatteryChargeUp = ThresholdGate:new(M.batteryChargeLowRatio, M.batteryChargeHighRatio)
	M.latchRegenDisable = TimedLatch:new(M.regenWheelslipDisableTime)
	M.engineEngagementChargePowerFilter = Backlash:new(M.engineEngagementChargePowerThreshold)

	applyControlParameters()
end

local function reset()
	M.init(M.jbeamData)
	M.initSecondStage()
end

local function overrideSetStarter(enabled)
	if enabled then
		M.powerSwitch = true
	end
end

local function overrideSetIgnition(enabled)
	if not enabled then
		M.powerSwitch = false
	end
end

local function tryEnableChargeMode()
	local function query()
		if not (M.allowChargeMode and M.chargeModePower > 0) then
			return false
		end

		if misc.chargeModeEnabled then
			return true
		end

		if power.batteryLevel < M.chargeModeUpperThreshold - 0.05 then
			misc.chargeModeEnabled = true
			gui.message({txt = "Charge mode enabled"}, 5, "vehicle.hybrid.chargeMode", "flash_on")
			return true
		else
			gui.message({txt = "Charge mode unavailable; battery full"}, 5, "vehicle.hybrid.chargeMode", "flash_off")
		end

		return false
	end

	local result = query()

	-- Ensure deterministic state
	electrics.values.chargeMode = result and 1 or 0

	return result
end

local function resumeNormalState()
	hybridState:changeTo(engineState.isRunning and "running" or "stopped")
end

--- Creates the state machine state definitions for the hybrid controller
---@return table<string, StateMachineState>
local function createStates()
	return {
		default = {
			update = function(self) -- use "update" and not "enter" so that we don't transition to "init" until first actual update
				self.machine:changeTo("init")
			end,
		},

		-- Initialization state; occurs any time the vehicle powers on
		init = {
			init = function(self)
				self.initializeTime = 0
			end,
			enter = function(self)
				-- reset our state tables
				torqueData:reset()
				misc:reset()
				engineDemand:reset()
				engineState:reset()
				braking:reset()
				power:reset()
				debugValues:reset()
				synergyDebugValues:reset()

				if M.timeSinceVehicleLoaded >= 0.25 then -- Don't play sounds right when vehicle loads
					-- play startup sound
					obj:playSFXOnce(M.startupSampleName, gearbox.transmissionNodeID, M.startupSoundBaseVolume, 1)

					if M.interiorStartupSampleName and extensions.arcanox_camhelper.isCameraInside() then
						local soundNode = M.interiorSoundNode or gearbox.transmissionNodeID

						-- play interior startup sound
						obj:playSFXOnce(M.interiorStartupSampleName, soundNode, M.interiorStartupSoundBaseVolume, 1)
					end

					self.initializeTime = 0
				else -- if vehicle just loaded, initialize immediately
					self.initializeTime = M.initializeTime
				end
			end,
			updateGFX = function(self, dt)
				self.initializeTime = self.initializeTime + dt
				electrics.values.checkengine = true
				electrics.values.hazard = true
				electrics.values.battery = true
				electrics.values.evMode = 0
				electrics.values.chargeMode = 0

				if self.initializeTime >= M.initializeTime then
					if not M.powerOn then
						M.powerOn = true
					end

					electrics.values.evMode = input.evMode or 0
					misc.gearLockedInPark = false

					if M.startEvMode or M.forceEvMode then
						electrics.values.evMode = 1
						self.machine:changeTo("evmode")
					elseif power.batteryLevelRaw < M.criticalBatteryPowerLevel then
						-- attempt to start engine

						if engine and not engineState.isRunning and engine.hasFuel and M.hasAuxiliaryStarterMotor then
							if engine.starterEngagedCoef < 1 then
								engine.ignitionCoef = 0
								engine.isDisabled = false
								engine.starterDisabled = false
								engine.idleStartCoef = 1
								engineState.startingTime = 0
								self.initializeTime = 0
								engine:activateStarter()
							else
								engineState.startingTime = engineState.startingTime + dt

								if engineState.startingTime > 3 then
									-- Something's not right; retry at init

									engine.ignitionCoef = 0
									engineState.startAttempts = engineState.startAttempts + 1
									engine:deactivateStarter()
									self.initializeTime = 0
								end
							end
						else
							resumeNormalState()
							engineState.runTimeSincePowerOn = 0.0
						end
					else
						if engineState.startAttempts > 0 then
							-- immediately try and restart the engine

							engine.isDisabled = false
							self.machine:changeTo("startup")
						else
							resumeNormalState()
							engineState.runTimeSincePowerOn = 0.0
						end
					end
				else
					-- Prevent shifting while initializing

					if misc.gear == "P" then
						misc.gearLockedInPark = true
					end

					if misc.gearLockedInPark and misc.gear ~= "P" then
						controller.mainController.shiftToGearIndex(1)
					end

					electrics.values.checkengine = true
				end
			end
		},

		-- EV-only running mode; engine is locked out until battery gets critically low
		evmode = {
			canEnter = function(self)
				if power.batteryLevel > 0.026 then
					return true
				else
					gui.message({txt = "Battery too low; EV mode not available"}, 5, "vehicle.hybrid.evMode", "battery_alert")
					electrics.values.evMode = 0
					return false
				end
			end,
			enter = function(self)
				electrics.values.evMode = 1
				electrics.values.chargeMode = 0
				gui.message({txt = "EV mode enabled"}, 2, "vehicle.hybrid.evMode", "ev_station")
				stopEngine()
			end,
			updateGFX = function(self, dt)
				electrics.values.ev = 1

				if M.forceEvMode then
					electrics.values.evMode = 1
				else
					if electrics.values.chargeMode == 1 then
						if tryEnableChargeMode() then
							resumeNormalState()
						end
					elseif electrics.values.evMode ~= 1 then
						resumeNormalState()
					end

					if power.batteryLevel < 0.05 then
						self.machine:changeTo("startup")
						gui.message({txt = "Battery too low; EV mode disabled"}, 2, "vehicle.hybrid.evMode", "battery_alert")
					end
				end
			end,
			leave = function(self)
				electrics.values.evMode = 0
				gui.message({txt = "EV mode disabled"}, 2, "vehicle.hybrid.evMode", "local_gas_station")
			end,
		},

		-- Running with engine off (i.e. temporary EV or idle-stop mode)
		stopped = {
			enter = function(self)
				stopEngine()
				misc.chargeModeEnabled = false
				M.engineStartupSmoother:reset()
			end,
			updateGFX = function(self, dt)
				if electrics.values.evMode == 1 then
					self.machine:changeTo("evmode")
					return
				elseif electrics.values.chargeMode == 1 then
					tryEnableChargeMode()
				end

				-- Check if engine is demanded on
				if misc.gear ~= "N" and engine and engineDemand.isDemanded then
					if engine.isBroken or misc.reducedPropulsion or not engine.hasFuel then
						handleFailedEngine()
					else
						engine.isDisabled = false -- allow engine to restart

						if electrics.values.rpm < engine.idleRPM then
							-- start engine
							self.machine:changeTo("startup")
						else
							-- RPM is high enough; don't need to start it
							resumeNormalState()
						end
					end
				end
			end,
		},

		-- Starting the engine
		startup = {
			init = function(self)
				self.startingTime = 0
				self.startAttempts = 0
				self.ignitionLatch = false
				self.ignitionSmoother = newTemporalSmoothingNonLinear(7.5)
			end,
			canEnter = function(self)
				return engine and true or false
			end,
			enter = function(self)
				self.startingTime = 0
				self.startAttempts = 0
				self.ignitionLatch = false
				self.ignitionSmoother:reset()
				engineState.timeSinceStart = 0
				engineState.starterTorque = 0
				engine.isDisabled = false
				engine.throttleSmoother:set(0)
				M.engineStarterPid:reset()
			end,
			updateGFX = function(self, dt)
				if self.startAttempts >= 3 then
					handleFailedEngine()
					resumeNormalState()
				else
					engine.idleStartCoef = 0 -- prevent rev-up as soon as engine catches
					self.startingTime = self.startingTime + dt

					if self.startingTime <= 3 then
						local engineHasStarted = engineState.isRunning and M.engineStarterPid.output <= 0 and M.engineStarterPid.integral <= 0 and not M.engineStarterPid.dynamicIntegralLock

						if engineHasStarted then
							resumeNormalState()
						elseif misc.gear == "N" then
							-- Starting interrupted by the driver shifting to Neutral
							self.machine:changeTo("stopped")
						end
					elseif self.startingTime > 4 then
						self.startAttempts = self.startAttempts + 1
						self.startingTime = 0 -- try again next time around
					end
				end
			end,
			update = function(self, dt)
				if self.startingTime <= 3 then
					local targetStarterTorque = M.engineStarterPid:get(engineState.targetRpm, engineState.rpm, dt) * M.engineStarterTorque

					engineState.starterTorque = targetStarterTorque

					engine.idleAVStartOffsetSmoother:reset()
				else
					engineState.starterTorque = 0
				end

				-- enable ignition once RPM is settled
				local rpmSettled = abs(M.engineStarterPid.delta) < 2

				if self.ignitionLatch or (engineState.rpm >= engineState.targetRpm - 5) then
					engine.ignitionCoef = 1
					self.ignitionLatch = true
				else
					engine.ignitionCoef = 0

					if not rpmSettled then
						M.driveThrottlePid:reset()
					end
				end
			end,
			leave = function()
				engineState.starterTorque = 0
			end
		},

		-- Running with engine on
		running = {
			updateGFX = function(self, dt)
				if not engineState.isRunning then
					self.machine:changeTo("startup")
					return
				end

				if electrics.values.evMode == 1 then
					if self.machine:changeTo("evmode") then
						return
					end
				end

				if misc.chargeModeEnabled then
					if electrics.values.chargeMode ~= 1 then
						misc.chargeModeEnabled = false
						electrics.values.chargeMode = 0
						gui.message({txt = "Charge mode disabled"}, 5, "vehicle.hybrid.chargeMode", "flash_off")
					end

					if power.batteryLevel >= M.chargeModeUpperThreshold then
						misc.chargeModeEnabled = false
						electrics.values.chargeMode = 0
						gui.message({txt = "Charge mode disabled (charge complete)"}, 5, "vehicle.hybrid.chargeMode", "battery_full")
					end
				end

				if electrics.values.chargeMode == 1 then
					tryEnableChargeMode()
				end

				if misc.gear == "N" or engineDemand.isDemanded then -- engine keeps state in N; treat it like engine demand
					engineState.timeSinceCanShutOff = 0
				else
					engineState.timeSinceCanShutOff = engineState.timeSinceCanShutOff + dt

					if engineState.timeSinceCanShutOff > M.autoStopDelayTime then
						self.machine:changeTo("stopped")
					end
				end
			end,
		},

		-- Vehicle is shutting down and preparing to enter "power off" state
		shutdown = {
			enter = function(self)
				if M.timeSinceVehicleLoaded >= 0.25 then
					-- play shutdown sound
					obj:playSFXOnce(M.shutdownSampleName, gearbox.transmissionNodeID, M.shutdownSoundBaseVolume, 1)

					-- play interior shutdown sound if present
					if M.interiorShutdownSampleName and extensions.arcanox_camhelper.isCameraInside() then
						local soundNode = M.interiorSoundNode or gearbox.transmissionNodeID

						obj:playSFXOnce(M.interiorShutdownSampleName, soundNode, M.interiorShutdownSoundBaseVolume, 1)
					end
				end

				-- Immediately power off engine
				engineState.timeSinceCanShutOff = M.autoStopDelayTime
				engineState.hasStartedSinceInit = false
				stopEngine()
			end,
			updateGFX = function(self, dt)
				-- Initially reset some variables
				electrics.values.evMode = 0
				electrics.values.chargeMode = 0
				electrics.values.iceThrottle = 0
				electrics.values.checkengine = false
				electrics.values.hazard = false
				electrics.values.battery = false
				misc.chargeModeEnabled = false
				misc.shownDisabledMessage = false
				misc.gearLockedInPark = false

				M.powerOn = false
				M.powerSwitch = false -- Prevent restarting until vehicle fully shut down

				if not engineState.isRunning and not engineState.isStopping then
					self.machine:changeTo("poweroff")
				end
			end,
		},

		-- Vehicle is completely powered off
		poweroff = {
			enter = function(self)
				-- reset electrics
				electrics.values.evMode = 0
				electrics.values.chargeMode = 0
				electrics.values.iceThrottle = 0
				electrics.values.checkengine = false
				electrics.values.hazard = false
				electrics.values.battery = false

				-- reset our state tables
				torqueData:reset()
				misc:reset()
				engineDemand:reset()
				engineState:reset()
				braking:reset()
				power:reset()
				debugValues:reset()
				synergyDebugValues:reset()
			end,
			updateGFX = function(self, dt)
				gearbox:setMG1PowerOverride(0)
				gearbox:setMG2PowerOverride(0)

				if rearMotor then
					rearMotor:setMotorPowerOverride(0)
				end

				if engine then
					engine:deactivateStarter()
					engine.ignitionCoef = 0
					engine.isDisabled = true
				end

				if M.powerSwitch then
					-- Power up vehicle

					if misc.gear == "P" then
						self.machine:changeTo("init")
					else
						M.powerSwitch = false
						gui.message({txt = "Shift to Park before starting vehicle"}, 2, "vehicle.hybrid.starting", "local_parking")
					end
				end
			end,
		},
	}
end

local function initSecondStage()
	-- Initialize state machine
	hybridState = StateMachine:new(createStates())

	-- make sure the camhelper extension loads and has time to update
	extensions.load("arcanox_camhelper")

	engine = powertrain.getDevice("mainEngine")
	gearbox = powertrain.getDevice("gearbox")
	rearMotor = powertrain.getDevice("rearMotor")
	battery = energyStorage.getStorage("tractionBattery")
	mainTank = energyStorage.getStorage("mainTank")
	brakeHold = controller.getController("brakeHold")

	local hasError = false

	if not engine or engine.type ~= "combustionEngine" then
		M.debug:log("W", "Engine not found")
		engine = nil
	end
	if engine and M.useEngineTorqueCurve ~= true and not M.engineRpmVsTorque then
		error("engineRpmVsTorque must be provided if useEngineTorqueCurve is not set to true")
	end
	if not gearbox then
		M.debug:log("E", "Gearbox not found; cannot continue")
		hasError = true
	end
	if rearMotor then
		M.debug:log("D", "Discovered rear electric motor; AWD enabled!")
	end
	if not battery then
		M.debug:log("E", "Battery not found; cannot continue")
		hasError = true
	end
	if not mainTank then
		M.debug:log("W", "Main fuel tank not found")
	end
	if gearbox.type ~= "ecvtGearbox" then
		M.debug:log("E", "Gearbox must be of type ecvtGearbox; cannot continue")
		hasError = true
	end

	if hasError then
		M.updateGFX = nop
		return
	end

	local cmuControllers = controller.getControllersByType("drivingDynamics/CMU")

	if cmuControllers and #cmuControllers == 1 then
		CMU = cmuControllers[1]
	else
		M.debug:log("W", "Vehicle does not have a valid ESC/CMU")
	end

	-- Figure out wheels that can have regenerative braking
	wheelsWithRegen = {}

	for _, wheel in pairs(powertrain.getChildWheels(gearbox)) do
		if not tableContains(wheelsWithRegen, wheel) then
			table.insert(wheelsWithRegen, wheel)
		end
	end

	if rearMotor then
		for _, wheel in pairs(powertrain.getChildWheels(rearMotor)) do
			if not tableContains(wheelsWithRegen, wheel) then
				table.insert(wheelsWithRegen, wheel)
			end
		end
	end

	-- Override setStarter and setEngineIgnition
	if controller.mainController then
		controller.mainController.setStarter = overrideSetStarter
		controller.mainController.setEngineIgnition = overrideSetIgnition
	end

	M.batteryEfficiency = battery.jbeamData.efficiency or 0.98
	M.minBatteryPowerLevel = battery.jbeamData.minWorkingSoc or M.minBatteryPowerLevel
	M.maxBatteryPowerLevel = battery.jbeamData.maxWorkingSoc or M.maxBatteryPowerLevel
	M.criticalBatteryPowerLevel = battery.jbeamData.criticalSoc or M.criticalBatteryPowerLevel

	if engine then
		if not M.powerOn then
			engine.outputAV1 = 0
		end

		local engineTorqueData = engine:getTorqueData()

		if not M.jbeamData.engineMaxRpm then
			M.engineMaxRpm = engineTorqueData.maxPowerRPM
		end

		if not M.engineMinIdleRpm then
			M.engineMinIdleRpm = engine.idleRPM + 200
		end

		if not M.idealEngineLoad then
			M.idealEngineLoad = engineutil.calculateBestEfficiencyLoad(engine)
		end

		engineutil.setIdealLoadForEngine(engine, M.idealEngineLoad)
	end

	-- prefill electric values
	electrics.values.evMode = 0
	electrics.values.chargeMode = 0
	electrics.values.batteryLevel = 0

	-- reset our state tables
	torqueData:reset()
	misc:reset()
	engineDemand:reset()
	engineState:reset()
	braking:reset()
	power:reset()
	debugValues:reset()
	synergyDebugValues:reset()

	calculateElectricLimits(0)
	calculateTorqueStatistics(0)
	printTorqueData()

	-- make torqueData recalculate everything again in update loop
	torqueData.firstCalculation = true

	braking.totalPossibleBrakeTorque = 0
	braking.totalPropulsedBrakeTorque = 0
	braking.totalUnpropulsedBrakeTorque = 0

	for i = 0, wheels.wheelCount - 1, 1 do
		local wd = wheels.wheels[i]

		wd.canRegen = tableContains(wheelsWithRegen, wd)
		braking.totalPossibleBrakeTorque = braking.totalPossibleBrakeTorque + wd.brakeTorque

		if wd.canRegen then
			braking.totalPropulsedBrakeTorque = braking.totalPropulsedBrakeTorque + wd.brakeTorque
		else
			braking.totalUnpropulsedBrakeTorque = braking.totalUnpropulsedBrakeTorque + wd.brakeTorque
		end

		if not initialWheelBrakeSplit[i] then
			initialWheelBrakeSplit[i] = {wd.brakeInputSplit, wd.brakeSplitCoef}
		end
	end

	if engine and M.startPoweredOff then
		-- TODO: may not be needed in newer game versions?
		engine.outputAV = 0
	end

	resetAllTheStatefulThings()

	-- Pre-fill debug table with empty strings to set up proper order
	updateDefaultDebugValues()

	initialControlParameters = deepcopy(controlParameters)
end

local function setParameters(parameters)
	if not CMU then return end

	CMU.applyParameter(controlParameters, initialControlParameters, parameters, "hybrid.throttleCurveExponent")

	applyControlParameters()
end

M.updateFixedStep = updateFixedStep
M.updateWheelsIntermediate = updateWheelsIntermediate
M.updateGFX = updateGFX
M.init = init
M.initSecondStage = initSecondStage
M.reset = reset
M.getTorqueData = getTorqueData
M.printTorqueData = printTorqueData
M.getPowerLimits = getPowerLimits
M.setParameters = setParameters

return M
