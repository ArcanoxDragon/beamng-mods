---@class BrakeHoldController
local M = {}

M.type = "auxiliary"
M.relevantDevice = nil
M.defaultOrder = 2000

local mathutil = require("arcanox/mathutil")

local abs = math.abs
local max = math.max
local sign = mathutil.sign
local maprange = mathutil.maprange

local gearBlacklist = {}

local brakeHoldAmount = 0

local hillStartAssist = {
	enabled = false,
	wheelTorqueToRelease = nil,
	lastCoef = 0,
	minPitch = 0,
	maxPitch = 0,
}

local autoHold = {
	enabled = false,
	driverEnabled = false,
	lastDriverEnabled = false,
	active = false,
	timer = 0.0,
	applyTime = 1.0,
	brakePedalThreshold = 0.1,
	throttlePedalThreshold = 0.03,
}

local externalRequests = {}
local externalRequestActive = false

local smoothers = { inclineAngleSmoother = nil }

local function request(scope)
	if not externalRequests[scope] then
		externalRequestActive = true
		externalRequests[scope] = true
		-- log("D", "brakeHold.request", ("Brake hold requested for scope %q"):format(scope))
	end
end

local function releaseRequest(scope)
	if externalRequests[scope] then
		externalRequests[scope] = nil
		-- log("D", "brakeHold.releaseRequest", ("Brake hold request released for scope %q"):format(scope))
	end

	local requestsStillActive = false

	for _, v in pairs(externalRequests) do
		if v then
			requestsStillActive = true
			break
		end
	end

	externalRequestActive = requestsStillActive
end

local function updateHillStartAssist(dt)
	local actualSpeedSign = sign(electrics.values.avgWheelAV)
	local desiredSpeedSign = electrics.values.gear == "R" and -1 or 1
	local speed = electrics.values.wheelspeed
	local inclineAngle = smoothers.inclineAngleSmoother:get(obj:getDirectionVector().z * 180 / math.pi, dt)

	local hillAssistAngleCoef = maprange(inclineAngle * desiredSpeedSign, hillStartAssist.minPitch, hillStartAssist.maxPitch)
	local hillAssistTorqueCoef = hillStartAssist.wheelTorqueToRelease and maprange(abs(wheels.wheelTorque), 0, hillStartAssist.wheelTorqueToRelease, 1, 0) or 1
	local hillAssistSpeedApplyCoef = maprange(speed * actualSpeedSign * desiredSpeedSign, 1e-10, -0.05, 0, 1)
	local hillAssistSpeedReleaseCoef = maprange(speed * actualSpeedSign * desiredSpeedSign, 1e-5, 0.1, 1, 0)
	local hillAssistApplyCoef = hillAssistAngleCoef * hillAssistSpeedApplyCoef * hillAssistTorqueCoef
	local hillAssistReleaseCoef = 1 - (hillAssistAngleCoef * hillAssistSpeedReleaseCoef * hillAssistTorqueCoef)
	local hillAssistCoef

	if hillAssistReleaseCoef < 0.99 then -- gradually apply as the coef increases, but release all at once
		hillAssistCoef = max(hillStartAssist.lastCoef, hillAssistApplyCoef)
	else
		hillAssistCoef = 0
	end

	hillStartAssist.lastCoef = hillAssistCoef

	return hillAssistCoef
end

local function updateAutoBrakeHoldInput()
	local driverEnabled = (electrics.values.brakeHold or 0) > 0.5
	local brake = input.brake or 0

	if not driverEnabled and autoHold.active then
		if brake < autoHold.brakePedalThreshold then
			gui.message({ txt = "Depress brake pedal when disabling Auto Brake-Hold" }, 5, "vehicle.brakeHold.toggle", "warning")
			electrics.values.brakeHold = 1
			driverEnabled = true
		end
	end

	if driverEnabled ~= autoHold.lastDriverEnabled then
		gui.message({ txt = "Auto Brake-Hold " .. (driverEnabled and "Enabled" or "Disabled") }, 5, "vehicle.brakeHold.toggle", "info")
	end

	autoHold.lastDriverEnabled = autoHold.driverEnabled
	autoHold.driverEnabled = driverEnabled
end

local function updateAutoBrakeHold(dt)
	local throttle = input.throttle or 0
	local brake = input.brake or 0
	local speed = electrics.values.wheelspeed or 0

	if not autoHold.driverEnabled or speed > 0.25 then
		autoHold.active = false
	elseif autoHold.active then
		if throttle > autoHold.throttlePedalThreshold then
			autoHold.active = false
		end
	else
		if brake > autoHold.brakePedalThreshold then
			autoHold.timer = autoHold.timer + dt
		else
			autoHold.timer = 0
		end

		if autoHold.timer >= autoHold.applyTime then
			autoHold.active = true
			autoHold.timer = 0
			gui.message({ txt = "Brakes are being held" }, 2, "vehicle.brakeHold.autoHold", "info")
		end
	end

	return autoHold.active and 1 or 0
end

local function updateGFX(dt)
	updateAutoBrakeHoldInput()

	local brakeHoldCoef = 0

	if externalRequestActive then
		brakeHoldCoef = 1
	end

	if gearBlacklist[electrics.values.gear] then
		hillStartAssist.lastCoef = 0
		autoHold.active = false
		autoHold.timer = 0
	else
		if hillStartAssist.enabled then
			local hillStartAssistCoef = updateHillStartAssist(dt)

			brakeHoldCoef = max(brakeHoldCoef, hillStartAssistCoef)
		end

		if autoHold.enabled then
			local autoBrakeHoldCoef = updateAutoBrakeHold(dt)

			brakeHoldCoef = max(brakeHoldCoef, autoBrakeHoldCoef)
		else
			autoHold.active = false
		end
	end

	electrics.values.brakeHoldActive = autoHold.active and 1 or 0

	if brakeHoldCoef > 1e-10 then
		electrics.values.brakeOverride = max(input.brake or 0, brakeHoldCoef * brakeHoldAmount)
	else
		electrics.values.brakeOverride = nil
	end
end

local function reset()
	externalRequests = {}
	externalRequestActive = false

	hillStartAssist.lastCoef = 0

	autoHold.active = false
	autoHold.timer = 0

	electrics.values.brakeHoldActive = 0
end

local function init(jbeamData)
	brakeHoldAmount = jbeamData.brakeHoldAmount or 0.15

	if jbeamData.gearBlacklist and type(jbeamData.gearBlacklist) == "table" then
		gearBlacklist = tableValuesAsLookupDict(jbeamData.gearBlacklist)
	else
		gearBlacklist = tableValuesAsLookupDict { "P", "N" }
	end

	hillStartAssist.wheelTorqueToRelease = jbeamData.wheelTorqueToRelease
	hillStartAssist.enabled = jbeamData.useHillStartAssist ~= false
	hillStartAssist.minPitch = jbeamData.minHillStartAssistPitch or 3 -- degrees
	hillStartAssist.maxPitch = jbeamData.maxHillStartAssistPitch or 12 -- degrees

	autoHold.enabled = jbeamData.allowAutoBrakeHold or false
	autoHold.applyTime = jbeamData.autoHoldDelayTime or 1.0
	autoHold.brakePedalThreshold = jbeamData.autoHoldBrakePedalThreshold or 0.1
	autoHold.throttlePedalThreshold = jbeamData.autoHoldThrottlePedalThreshold or 0.03

	smoothers.inclineAngleSmoother = newTemporalSmoothingNonLinear(10)
end

local function initSecondStage()
end

M.updateGFX = updateGFX
M.reset = reset
M.init = init
M.initSecondStage = initSecondStage

M.request = request
M.releaseRequest = releaseRequest

return M
