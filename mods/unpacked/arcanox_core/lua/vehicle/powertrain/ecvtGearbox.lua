-- This Source Code Form is subject to the terms of the bCDDL, v. 1.1.
-- If a copy of the bCDDL was not distributed with this
-- file, You can obtain one at http://beamng.com/bCDDL-1.1.txt

local M = {}
local planetary = require("arcanox/planetaryGearset")
local mathutil = require("arcanox/mathutil")
local DebugModule = require("arcanox/DebugModule")
local TuningUtility = require("arcanox/TuningUtility")
local constants = {
	rpmToAV = 0.104719755,
	avToRPM = 9.549296596425384,
	torqueToHpCoef = 7120.54016,
	torquePowerCoef = 9.5488
}

M.outputPorts = {[1] = true}
-- "viscouscoupling" below is a hack-around to allow a second gearbox to be piggybacked onto this one for insane hax
M.deviceCategories = {gearbox = true, viscouscoupling = true, clutchlike = true}
M.requiredExternalInertiaOutputs = {1}

local max = math.max
local min = math.min
local abs = math.abs
local sqrt = math.sqrt
local clamp = mathutil.clamp
local sign = mathutil.sign
local maprange = mathutil.maprange

---@class EcvtGearbox : PowertrainDevice
---@field parentOutputAVName string
local ecvt = {}
ecvt.__index = ecvt

local function formatAv(av)
	return { av * constants.avToRPM, av }
end

---@param self EcvtGearbox
---@param dt number
local function updateDebug(self, dt)
	local mg1MaxTorque = self:getMaxMG1Torque(self.motorGenerator1TorqueDemand < 0)
	local mg2MaxTorque = self:getMaxMG2Torque(self.motorGenerator2TorqueDemand < 0)

	local ringTorqueFromMG1 = -self.psd:getRingTorqueFromSunTorque(self.motorGenerator1Torque)
	local ringTorqueFromClutch = -self.psd:getRingTorqueFromCarrierTorque((self.torqueDiff + self.spragTorqueDiff) * self.inputGearRatio)
	local finalTorqueFromRing = ringTorqueFromClutch * self.counterRingGearRatio
	local finalTorqueFromMG2 = self.motorGenerator2Torque * self.counterMG2GearRatio
	local cumulativeGearRatio = 1

	if self.children and #self.children > 0 then
		local child = self.children[1]

		cumulativeGearRatio = child.cumulativeGearRatio
	end

	self.debug:putValue("Input RPM, AV", formatAv(self.inputAV), 0)
	self.debug:putValue("Clutch RPM, AV", formatAv(self.psd.carrierAV * self.inputGearRatio), 0)

	self.debug:putValue("Sun Gear RPM, AV", formatAv(self.psd.sunGearAV), 0)
	self.debug:putValue("Carrier RPM, AV", formatAv(self.psd.carrierAV), 0)
	self.debug:putValue("Ring Gear RPM, AV", formatAv(self.psd.ringGearAV), 0)
	self.debug:putValue("MG1 RPM, AV", formatAv(self.motorGenerator1AV), 0)
	self.debug:putValue("MG2 RPM, AV", formatAv(self.motorGenerator2AV), 0)

	self.debug:putValue("MG1 Max Torque", mg1MaxTorque)
	self.debug:putValue("MG1 Torque", self.motorGenerator1Torque, 0)
	self.debug:putValue("MG1 Torque Demand", self.motorGenerator1TorqueDemand, 0)
	self.debug:putValue("MG1 Power", self.motorGenerator1Power, 0)

	self.debug:putValue("MG2 Max Torque", mg2MaxTorque)
	self.debug:putValue("MG2 Torque", self.motorGenerator2Torque, 0)
	self.debug:putValue("MG2 Torque Demand", self.motorGenerator2TorqueDemand, 0)
	self.debug:putValue("MG2 Power", self.motorGenerator2Power, 0)

	self.debug:putValue("MG1->Ring Torque", ringTorqueFromMG1, 0)
	self.debug:putValue("Clutch->Ring Torque", ringTorqueFromClutch, 0)
	self.debug:putValue("Ring->Final Torque", finalTorqueFromRing, 0)
	self.debug:putValue("MG2->Final Torque", finalTorqueFromMG2, 0)
	self.debug:putValue("Output Torque (Final)", self.outputTorque1, 0)
	self.debug:putValue("Output Torque (Wheels)", self.outputTorque1 * cumulativeGearRatio, 0)

	self.debug:putValue("Clutch Lock Torque", self.clutchLockTorque, 0)
	self.debug:putValue("Clutch Lock Spring", self.clutchLockSpring, 0)
	self.debug:putValue("Clutch Stiffness", self.clutchStiffness)
	self.debug:putValue("Clutch AV Diff", self.inputAV - self.psd.carrierAV * self.inputGearRatio)
	self.debug:putValue("Clutch Angle (Current, Max)", { self.clutchAngle, self.maxClutchAngle })
	self.debug:putValue("Clutch Torque", self.torqueDiff, 0)

	self.debug:putValue("Sprag Lock Torque", self.spragLockTorque, 0)
	self.debug:putValue("Sprag Lock Spring", self.spragLockSpring, 0)
	self.debug:putValue("Sprag Stiffness", self.spragStiffness)
	self.debug:putValue("Sprag Angle (Current, Max)", { self.spragAngle, self.maxSpragAngle })
	self.debug:putValue("Sprag Torque", self.spragTorqueDiff, 0)

	-- local selfPower = self.spentEnergyLastUpdate / dt / 1000
	local enginePower = self.debugValues.engineEnergy / dt / 1000
	local mg1Power = self.debugValues.mg1Energy / dt / 1000
	local mg2Power = self.debugValues.mg2Energy / dt / 1000
	local netPower = self.debugValues.netEnergy / dt / 1000
	local outputPower = self.debugValues.outputEnergy / dt / 1000
	local deltaPower = self.debugValues.deltaEnergy / dt / 1000

	self.debug:putValue("Engine Power (kW)", enginePower, 0)
	self.debug:putValue("MG1 Power (kW)", mg1Power, 0)
	self.debug:putValue("MG2 Power (kW)", mg2Power, 0)
	self.debug:putValue("Net Power (kW)", netPower, 0)
	self.debug:putValue("Output Power (kW)", outputPower, 0)
	self.debug:putValue("Delta Power (kW)", deltaPower, 0)

	-- Power graph
	-- self.debug:putGraphValue{"Engine Power", enginePower, "kW"}
	-- self.debug:putGraphValue{"MG1 Power", mg1Power, "kW"}
	-- self.debug:putGraphValue{"MG2 Power", mg2Power, "kW"}
	-- self.debug:putGraphValue{"Net Power", netPower, "kW"}
	-- self.debug:putGraphValue{"Output Power", outputPower, "kW"}
	-- self.debug:putGraphValue{"Delta Power", deltaPower, "kW"}
	-- self.debug:putGraphValue{"Battery Power Delta", -selfPower, "kW"}
	-- self.debug:putGraphValue{"Output - Battery", outputPower - selfPower, "kW"}
	-- self.debug:putGraphValue{"Magic Energy Sum", self.debugValues.magicEnergyRunningTotal / 1000, "kJ"}
	-- self.debug:putGraphValue{"Zero", 0, color = "#00000080"}

	-- Clutch graph
	self.debugValues.maxAvDiff = max(self.debugValues.maxAvDiff, abs(self.debugValues.avDiff))

	-- local avDiff = self.debugValues.avDiff / self.debugValues.maxAvDiff
	-- local springTorque = clamp(self.clutchAngle * self.clutchLockSpring, -self.clutchLockTorque, self.clutchLockTorque)
	-- local dampingTorque = clamp(self.clutchLockDamp * avDiff, -self.clutchLockTorque, self.clutchLockTorque)

	-- self.debugValues.maxDampingTorque = max(self.debugValues.maxDampingTorque, abs(dampingTorque))

	-- self.debug:putGraphValue{"Clutch Torque %", self.torqueDiff / self.clutchLockTorque, "%"}
	-- self.debug:putGraphValue{"Clutch Angle", self.clutchAngle / self.maxClutchAngle, "%"}
	-- self.debug:putGraphValue{"Clutch AV Diff", avDiff, "%"}
	-- self.debug:putGraphValue{"Damping Torque %", dampingTorque / self.debugValues.maxDampingTorque, "%"}
	-- self.debug:putGraphValue{"Spring Torque %", springTorque / self.clutchLockTorque, "%"}
	-- self.debug:putGraphValue{"Zero", 0, color = "#00000080"}

	self.debugValues.engineEnergy = 0
	self.debugValues.mg1Energy = 0
	self.debugValues.mg2Energy = 0
	self.debugValues.netEnergy = 0
	self.debugValues.outputEnergy = 0
	self.debugValues.deltaEnergy = 0
end

function ecvt:velocityUpdate(dt)
	-- propagate AV changes from parent
	self.inputAV = self.parent[self.parentOutputAVName]

	-- These are really just aliases
	self.motorGenerator1AV = self.psd.sunGearAV
	self.motorGenerator2AV = self.outputAV1 * self.counterMG2GearRatio
end

function ecvt:torqueUpdate(dt)
	-- ring gear is connected directly to output reduction gears
	local ringGearAV = self.outputAV1 * self.counterRingGearRatio

	-- built-in clutch (input-side is connected to carrier through gear reduction)
	local clutchAV = self.psd.carrierAV * self.inputGearRatio

	local avDiff = self.inputAV - clutchAV

	self.clutchAngle = clamp(self.clutchAngle + avDiff * dt * self.clutchStiffness, -self.maxClutchAngle, self.maxClutchAngle)

	local clutchLockTorque = self.clutchLockTorque

	self.torqueDiff = clamp(self.clutchAngle * self.clutchLockSpring + self.clutchLockDamp * avDiff, -clutchLockTorque, clutchLockTorque)

	-- One-way sprag preventing reverse engine rotation
	local spragAvDiff = -clutchAV

	self.spragAngle = clamp(self.spragAngle + spragAvDiff * dt * self.spragStiffness, -self.maxSpragAngle, self.maxSpragAngle)

	local spragLockTorque = self.spragLockTorque

	self.spragTorqueDiff = clamp(self.spragAngle * self.spragLockSpring + self.spragLockDamp * spragAvDiff, -self.spragFriction, spragLockTorque)

	local reverseGearRatioCoef = self.reverseGearRatioCoef

	-- determine net torque on the sun gear; it's the "furthest" entity from the input along that torque path, so we use it as the target
	-- for the virtual angular velocity
	local carrierTorqueFromClutch = (self.torqueDiff + self.spragTorqueDiff) * self.inputGearRatio
	local sunGearTorqueFromClutch = carrierTorqueFromClutch * self.psd.sunCarrierRatio
	local sunGearTorqueFromMG1 = self.motorGenerator1Torque
	local netSunGearTorque = sunGearTorqueFromClutch + sunGearTorqueFromMG1 - (self.friction * clamp(self.psd.sunGearAV, -1, 1) + self.dynamicFriction * self.psd.sunGearAV)
	local newSunGearAV = self.psd.sunGearAV + netSunGearTorque * dt * self.invCumulativeSunGearInertia

	self.psd:updateAVs(newSunGearAV, nil, ringGearAV)

	-- reset values for this tick
	self.motorGenerator1Torque = 0
	self.motorGenerator2Torque = 0

	local maxMG1Torque = self:getMaxMG1Torque()
	local maxMG2Torque = self:getMaxMG2Torque()
	local maxMG1RegenTorque = self:getMaxMG1RegenTorque()
	local maxMG2RegenTorque = self:getMaxMG2RegenTorque()
	local mg2TorqueCoef = 0

	if self.mode ~= "neutral" then
		if self.motorGenerator1TorqueDemand < 0 then
			self.motorGenerator1Torque = -sign(self.motorGenerator1AV) * min(-self.motorGenerator1TorqueDemand, maxMG1RegenTorque)
		else
			self.motorGenerator1Torque = self.motorGenerator1TorqueDirection * min(self.motorGenerator1TorqueDemand, maxMG1Torque)
		end

		if self.motorGenerator2TorqueDemand < 0 then -- regenerative braking
			self.motorGenerator2Torque = -sign(self.motorGenerator2AV) * min(-self.motorGenerator2TorqueDemand, maxMG2RegenTorque)
			mg2TorqueCoef = abs(self.motorGenerator2Torque / maxMG2RegenTorque)
		else
			self.motorGenerator2Torque = reverseGearRatioCoef * min(self.motorGenerator2TorqueDemand, maxMG2Torque)
			mg2TorqueCoef = abs(self.motorGenerator2Torque / maxMG2Torque)
		end
	end

	-- motor powers
	local mg1PowerCoef = self.motorGenerator1TorqueDemand < 0 and 1 or self:getPowerCoef(self.motorGenerator1AV)
	local mg2PowerCoef = self.motorGenerator2TorqueDemand < 0 and 1 or self:getPowerCoef(self.motorGenerator2AV)

	self.motorGenerator1Power = mg1PowerCoef * abs(mathutil.torqueToKw(self.motorGenerator1Torque, max(self.minPowerAV, abs(self.motorGenerator1AV)))) * sign(self.motorGenerator1TorqueDemand)
	self.motorGenerator2Power = mg2PowerCoef * abs(mathutil.torqueToKw(self.motorGenerator2Torque, max(self.minPowerAV, abs(self.motorGenerator2AV)))) * sign(self.motorGenerator2TorqueDemand)

	local grossWork = dt * (self.motorGenerator1Power + self.motorGenerator2Power) * 1000 -- joules, not kJ, so multiply kW by 1000

	self.spentEnergy = self.spentEnergy + grossWork

	-- output torque is the same as ring gear torque, which is a sum of
	-- the torque of MG2 plus the ratio-adjusted torques from the input
	-- and MG1. If the input AV is near-zero and MG1 is applying torque
	-- that, without a sprag, would cause it to spin backwards, the sprag
	-- causes this torque to transfer to the ring gear directly from MG1
	-- using the stationary carrier as a reduction.
	local ringTorqueFromCarrier = -self.psd:getRingTorqueFromCarrierTorque(carrierTorqueFromClutch)
	local finalTorqueFromRing = ringTorqueFromCarrier * self.counterRingGearRatio
	local finalTorqueFromMG2 = self.motorGenerator2Torque * self.counterMG2GearRatio

	self.lastOutputTorque = self.outputTorque1
	self.lastOutputTorqueFromInput = self.outputTorqueFromInput
	self.lastOutputTorqueFromMG = self.outputTorqueFromMG2

	self.rawOutputTorque = finalTorqueFromRing + finalTorqueFromMG2
	self.outputTorque1 = self.rawOutputTorque - (self.friction * clamp(self.outputAV1, -1, 1) + self.dynamicFriction * self.outputAV1)
	self.outputTorqueFromInput = finalTorqueFromRing
	self.outputTorqueFromMG2 = finalTorqueFromMG2

	-- artificial "throttle" value for ESC system to look at
	self.throttle = mg2TorqueCoef

	if abs(self.outputAV1) < 100 and self.mode == "park" then
		self.parkEngaged = 1
	end

	self.parkClutchAngle = min(max(self.parkClutchAngle + self.outputAV1 * dt, -self.maxParkClutchAngle), self.maxParkClutchAngle)

	local parkClutchFreePlay = self.parkLockFreePlay
	local absFreeParkClutchAngle = max(abs(self.parkClutchAngle) - parkClutchFreePlay, 0)

	local parkLockDampAV = min(abs(self.parkClutchAngle) / parkClutchFreePlay, 1) * self.outputAV1
	local parkLockTorque = -(min(1, absFreeParkClutchAngle) * absFreeParkClutchAngle * fsign(self.parkClutchAngle) * self.parkLockSpring * self.lockCoef + self.parkLockDamp * parkLockDampAV)

	self.outputTorque1 = self.outputTorque1 + parkLockTorque * self.parkEngaged

	-- sounds
	self.motorSoundAVMG1 = self.motorSoundAVSmootherMG1:get(self.motorGenerator1AV)
	self.motorSoundAVMG2 = self.motorSoundAVSmootherMG2:get(self.motorGenerator2AV)
	self.gearWhineAVPSD = self.gearWhineAVSmootherPSD:get(self.psd.ringGearAV)
	self.motorSoundTorqueMG1 = self.motorSoundTorqueSmootherMG1:get(self.motorGenerator1Torque)
	self.motorSoundTorqueMG2 = self.motorSoundTorqueSmootherMG2:get(self.motorGenerator2Torque)
	self.gearWhineTorquePSD = self.gearWhineTorqueSmootherPSD:get(finalTorqueFromRing)

	if self.debug then
		-- Update debug energies
		local engineEnergy = self.parent.outputTorque1 * self.parent.outputAV1 * dt
		local mg1Energy = self.motorGenerator1Torque * self.motorGenerator1AV * dt
		local mg2Energy = self.motorGenerator2Torque * self.motorGenerator2AV * dt
		local netEnergy = engineEnergy + mg1Energy + mg2Energy
		local outputEnergy = self.outputTorque1 * self.outputAV1 * dt
		local deltaEnergy = outputEnergy - netEnergy
		local magicEnergy = outputEnergy - grossWork - engineEnergy

		self.debugValues.avDiff = avDiff
		self.debugValues.engineEnergy = self.debugValues.engineEnergy + engineEnergy
		self.debugValues.mg1Energy = self.debugValues.mg1Energy + mg1Energy
		self.debugValues.mg2Energy = self.debugValues.mg2Energy + mg2Energy
		self.debugValues.netEnergy = self.debugValues.netEnergy + netEnergy
		self.debugValues.outputEnergy = self.debugValues.outputEnergy + outputEnergy
		self.debugValues.deltaEnergy = self.debugValues.deltaEnergy + deltaEnergy
		self.debugValues.magicEnergyRunningTotal = max(0, self.debugValues.magicEnergyRunningTotal + magicEnergy)
	end
end

function ecvt:validate()
	local maxEngineTorque
	local maxEngineAV
	local engineInertia

	if self.parent.deviceCategories.engine then
		local engine = self.parent --[[@as EngineDevice]]
		local torqueData = engine:getTorqueData()

		if not self.transmissionNodeID then
			self.transmissionNodeID = engine.engineNodeID
		end

		maxEngineTorque = torqueData.maxTorque
		maxEngineAV = engine.maxAV
		engineInertia = engine.inertia
	else
		log("W", "ecvtGearbox.validate", "Parent device is not an engine.")
		log("W", "ecvtGearbox.validate", "Actual parent:")
		log("W", "ecvtGearbox.validate", powertrain.dumpsDeviceData(self.parent))

		if not self.transmissionNodeID then
			self.transmissionNodeID = sounds.engineNode
		end

		maxEngineTorque = 100
		maxEngineAV = 6000 * constants.rpmToAV
		engineInertia = 0.15
	end

	if type(self.transmissionNodeID) ~= "number" then
		self.transmissionNodeID = nil
	end

	self.clutchLockTorque = self.clutchLockTorque or (maxEngineTorque * 1.25 + maxEngineAV * engineInertia)

	local maxMG1TorqueToSprag = self.mg1TorqueRating / self.psd.sunCarrierRatio / self.inputGearRatio
	local maxMG1AVAtSprag = self.psd:getCarrierAV(self.maxMG1AV, 0) * self.inputGearRatio
	local maxSpragAV = max(maxMG1AVAtSprag, maxEngineAV)

	self.spragLockTorque = self.spragLockTorque or (maxMG1TorqueToSprag * 1.25 + maxSpragAV * engineInertia)

	local maxExpectedRingTorqueInput = maxEngineTorque * self.inputGearRatio * self.psd.ringCarrierRatio
	local maxExpectedRingTorqueMG1 = self.mg1TorqueRating / self.psd.sunRingRatio

	self.maxExpectedFinalTorquePSD = max(maxExpectedRingTorqueInput, maxExpectedRingTorqueMG1) * self.counterRingGearRatio
	self.invMaxExpectedFinalTorquePSD = 1 / self.maxExpectedFinalTorquePSD
	self.maxExpectedFinalTorqueMG2 = self.mg2TorqueRating * self.counterMG2GearRatio
	self.invMaxExpectedFinalTorqueMG2 = 1 / self.maxExpectedFinalTorqueMG2

	self:initTuningParameters()

	return true
end

function ecvt:setMode(mode)
	self.mode = mode
	self.reverseGearRatioCoef = mode == "reverse" and -1 or 1
	self.parkEngaged = 0

	if mode == "park" then
		self.parkClutchAngle = 0
	end
end

function ecvt:selectUpdates()
	if self.debug.enabled then
		self.updateDebug = updateDebug
	else
		self.updateDebug = nop
	end
end

function ecvt:setGearRatio(ratio)
end

function ecvt:setRequestedMG1Torque(requestedTorque)
	self.motorGenerator1TorqueDemand = clamp(requestedTorque, -self.mg1TorqueRating, self.mg1TorqueRating)
end

function ecvt:setRequestedMG2Torque(requestedTorque)
	self.motorGenerator2TorqueDemand = clamp(requestedTorque, -self.mg2TorqueRating, self.mg2TorqueRating)
end

function ecvt:setMG1PowerOverride(maxPowerOverride)
	self.maxMG1PowerOverride = min(maxPowerOverride, self.mg1PowerRating)
end

function ecvt:setMG2PowerOverride(maxPowerOverride)
	self.maxMG2PowerOverride = min(maxPowerOverride, self.mg2PowerRating)
end

function ecvt:setMG1TorqueDirection(direction)
	self.motorGenerator1TorqueDirection = (direction == -1) and -1 or 1
end

function ecvt:setLock(enabled)
	self.lockCoef = enabled and 0 or 1
end

function ecvt:calculateInertia()
	local outputInertia = 0
	local cumulativeGearRatio = 1
	local maxCumulativeGearRatio = 1

	if self.children and #self.children > 0 then
		local child = self.children[1]

		outputInertia = child.cumulativeInertia
		cumulativeGearRatio = child.cumulativeGearRatio
		maxCumulativeGearRatio = child.maxCumulativeGearRatio
	end

	-- calculate gear ratio
	self.cumulativeGearRatio = cumulativeGearRatio * self.gearRatio
	self.maxCumulativeGearRatio = maxCumulativeGearRatio * self.gearRatio

	-- internal inertia of PSD
	self.psd:calculateInertia()

	-- calculate inertias
	self.outputInertia = outputInertia
	self.cumulativeInertia = outputInertia / self.gearRatio / self.gearRatio
	self.invCumulativeInertia = 1 / self.cumulativeInertia

	local inputToSunRatio = self.inputGearRatio * self.psd.sunCarrierRatio

	self.inputToSunInertia = self.psd.carrierInertia * inputToSunRatio * inputToSunRatio
	self.invInputToSunInertia = 1 / self.inputToSunInertia
	self.cumulativeSunGearInertia = self.inputToSunInertia + self.psd.sunGearInertia
	self.invCumulativeSunGearInertia = 1 / self.cumulativeSunGearInertia

	-- built-in clutch spring
	local engineInertia

	if self.parent.deviceCategories.engine then
		engineInertia = self.parent.inertia
	else
		engineInertia = 0.15
	end

	local sunToInputInertia = self.psd.sunGearInertia / inputToSunRatio / inputToSunRatio
	local carrierToInputInertia = self.psd.carrierInertia / self.inputGearRatio / self.inputGearRatio
	local outputToInputInertia = self.cumulativeInertia

	self.clutchInertia = min(engineInertia * 0.5, sunToInputInertia + carrierToInputInertia + outputToInputInertia)
	self.invClutchInertia = 1 / self.clutchInertia

	self.clutchLockSpring = self.clutchLockSpringBase or (powertrain.stabilityCoef * powertrain.stabilityCoef * self.clutchInertia * self.clutchLockSpringCoef) --Nm/rad
	self.clutchLockDamp = self.clutchLockDampRatio * sqrt(self.clutchLockSpring * self.clutchInertia)

	-- one-way engine sprag spring
	self.spragLockSpring = self.spragLockSpringBase or (powertrain.stabilityCoef * powertrain.stabilityCoef * self.clutchInertia * self.spragLockSpringCoef) --Nm/rad
	self.spragLockDamp = self.spragLockDampRatio * sqrt(self.spragLockSpring * self.clutchInertia)

	self.parkLockSpring = self.parkLockSpringBase or (powertrain.stabilityCoef * powertrain.stabilityCoef * outputInertia * 0.5) --Nm/rad
	self.parkLockDamp = self.parkLockDampRatio * sqrt(self.parkLockSpring * outputInertia)

	--^2 spring but linear spring after 1 rad
	self.maxClutchAngle = sqrt(self.clutchLockTorque / self.clutchLockSpring) + max(self.clutchLockTorque / self.clutchLockSpring - 1, 0)
	self.maxSpragAngle = sqrt(self.spragLockTorque / self.spragLockSpring) + max(self.spragLockTorque / self.spragLockSpring - 1, 0)
	self.maxParkClutchAngle = self.parkLockTorque / self.parkLockSpring

	self:initTuningParameters()
end

function ecvt:getMaxMG1Torque(powerOverrideOrIgnore, av)
	local mg1AV = av and abs(av) or max(abs(self.motorGenerator1AV), self.minPowerAV)
	local maxPower = self.mg1PowerRating

	if type(powerOverrideOrIgnore) == "number" then
		maxPower = min(powerOverrideOrIgnore, maxPower)
	elseif type(self.maxMG1PowerOverride) == "number" and not (type(powerOverrideOrIgnore) == "boolean" and powerOverrideOrIgnore == true) then
		maxPower = self.maxMG1PowerOverride
	end

	local maxTorque = mathutil.kwToTorque(maxPower, mg1AV)

	return min(maxTorque, self.mg1TorqueRating)
end

function ecvt:getMaxMG1RegenTorque(maxPower, av)
	local maxTorque = self:getMaxMG1Torque(maxPower or true)

	return self:getMaxMG1RegenCoef(av) * maxTorque
end

function ecvt:getMaxMG1RegenCoef(av)
	local mg1AV = av and abs(av) or abs(self.motorGenerator1AV)

	return maprange(mg1AV, 0, self.generatorMinFullLoadAV)
end

function ecvt:getMaxMG2Torque(powerOverrideOrIgnore, av)
	local mg2AV = av and abs(av) or max(abs(self.motorGenerator2AV), self.minPowerAV)
	local maxPower = self.mg2PowerRating

	if type(powerOverrideOrIgnore) == "number" then
		maxPower = min(powerOverrideOrIgnore, maxPower)
	elseif type(self.maxMG2PowerOverride) == "number" and not (type(powerOverrideOrIgnore) == "boolean" and powerOverrideOrIgnore == true) then
		maxPower = self.maxMG2PowerOverride
	end

	local maxTorque = mathutil.kwToTorque(maxPower, mg2AV)

	return min(maxTorque, self.mg2TorqueRating)
end

function ecvt:getMaxMG2RegenTorque(maxPower, av)
	local maxTorque = self:getMaxMG2Torque(maxPower or true)

	return self:getMaxMG2RegenCoef(av) * maxTorque
end

function ecvt:getMaxMG2RegenCoef(av)
	local mg2AV = av and abs(av) or abs(self.motorGenerator2AV)

	return maprange(mg2AV, self.regenFadeMinAV, self.regenFadeMaxAV)
end

function ecvt:getPowerCoef(motorAv)
	return maprange(abs(motorAv), 0, self.minPowerAV, self.startingPowerCoef, 1)
end

function ecvt:getMinCarrierAV()
	return self.psd:getCarrierAV(self.minMG1AV, nil)
end

function ecvt:getMaxCarrierAV()
	return self.psd:getCarrierAV(self.maxMG1AV, nil)
end

function ecvt:updateSounds(dt)
	-- motor inverter sounds
	local motorAVMG1 = abs(self.motorSoundAVMG1)
	local motorTorqueMG1 = abs(self.motorSoundTorqueMG1)
	local motorPowerMG1 = mathutil.torqueToKw(motorTorqueMG1, motorAVMG1)

	local inverterEffortMG1 = min(1, motorPowerMG1 / self.inverterMaxVolumeKwMG1)
	local inverterVolumeMG1 = max(maprange(motorPowerMG1, 0, self.inverterMinVolumeKwMG1) * self.inverterMinVolumeMG1, maprange(motorPowerMG1, self.inverterMinVolumeKwMG1, self.inverterMaxVolumeKwMG1) * self.inverterVolumeCoefMG1)
	local inverterPitchMG1 = maprange(motorAVMG1, self.inverterPitchMinAVMG1, self.inverterPitchMaxAVMG1, self.inverterMinPitchMG1, self.inverterMaxPitchMG1, false)

	local motorAVMG2 = abs(self.motorSoundAVMG2)
	local motorTorqueMG2 = abs(self.motorSoundTorqueMG2)
	local motorPowerMG2 = mathutil.torqueToKw(motorTorqueMG2, motorAVMG2)

	local inverterEffortMG2 = min(1, motorPowerMG2 / self.inverterMaxVolumeKwMG2)
	local inverterVolumeMG2 = max(maprange(motorPowerMG2, 0, self.inverterMinVolumeKwMG2) * self.inverterMinVolumeMG2, maprange(motorPowerMG2, self.inverterMinVolumeKwMG2, self.inverterMaxVolumeKwMG2) * self.inverterVolumeCoefMG1)
	local inverterPitchMG2 = maprange(motorAVMG2, self.inverterPitchMinAVMG2, self.inverterPitchMaxAVMG2, self.inverterMinPitchMG2, self.inverterMaxPitchMG2, false)

	self.inverterSoundLoopMG1:setVolumePitch(inverterVolumeMG1, inverterPitchMG1, inverterEffortMG1, 1)
	self.inverterSoundLoopMG2:setVolumePitch(inverterVolumeMG2, inverterPitchMG2, inverterEffortMG2, 1)

	-- gear whine sounds
	local motorSoundTorqueMG2 = motorTorqueMG2 * self.counterMG2GearRatio
	local gearWhineLoadMG2 = motorSoundTorqueMG2 * self.invMaxExpectedFinalTorqueMG2
	local gearWhineVolumeMG2 = max(self.gearWhineMinVolume, maprange(gearWhineLoadMG2, 1e-2, 1) * self.gearWhineVolumeCoef)
	local gearWhinePitchMG2 = maprange(motorAVMG2, self.gearWhinePitchMinAV, self.gearWhinePitchMaxAV, self.gearWhineMinPitch, self.gearWhineMaxPitch, false)

	local gearWhineAVPSD = abs(self.gearWhineAVPSD)
	local gearWhineTorquePSD = abs(self.gearWhineTorquePSD)
	local gearWhineLoadPSD = gearWhineTorquePSD * self.invMaxExpectedFinalTorquePSD
	local gearWhineVolumePSD = max(self.gearWhineMinVolume, maprange(gearWhineLoadPSD, 1e-2, 1) * self.gearWhineVolumeCoef)
	local gearWhinePitchPSD = maprange(gearWhineAVPSD, self.gearWhinePitchMinAV, self.gearWhinePitchMaxAV, self.gearWhineMinPitch, self.gearWhineMaxPitch, false)

	self.gearWhineSoundLoopMG2:setVolumePitch(gearWhineVolumeMG2, gearWhinePitchMG2, gearWhineLoadMG2, 1)
	self.gearWhineSoundLoopPSD:setVolumePitch(gearWhineVolumePSD, gearWhinePitchPSD, gearWhineLoadPSD, 1)
end

function ecvt:updateGFX(dt)
	self.spentEnergyLastUpdate = self.spentEnergy
	self.spentEnergy = 0

	self:updateDebug(dt)
end

function ecvt:initSounds(jbeamData)
	local inverterSoundNodeID

	if jbeamData.inverterSoundNode_nodes and type(jbeamData.inverterSoundNode_nodes) == "table" then
		inverterSoundNodeID = jbeamData.inverterSoundNode_nodes[1]
	end

	if type(inverterSoundNodeID) ~= "number" then
		inverterSoundNodeID = self.transmissionNodeID or sounds.engineNode
	end

	-- Motor inverter sounds
	local inverterSoundSample = jbeamData.inverterSampleName or "vehicles/hybrid_common/sounds/inverter.ogg"

	self.inverterSoundLoopMG1 = sounds.createSoundObj(inverterSoundSample, "AudioDefaultLoop3D", "InverterMG1", inverterSoundNodeID)
	self.inverterSoundLoopMG2 = sounds.createSoundObj(inverterSoundSample, "AudioDefaultLoop3D", "InverterMG2", inverterSoundNodeID)

	-- MG1 inverter parameters
	self.inverterMinVolumeKwMG1 = jbeamData.inverterMinVolumeKwMG1 or jbeamData.inverterMinVolumeKw or 4
	self.inverterMaxVolumeKwMG1 = jbeamData.inverterMaxVolumeKwMG1 or jbeamData.inverterMaxVolumeKw or 50
	self.inverterVolumeCoefMG1 = jbeamData.inverterVolumeCoefMG1 or jbeamData.inverterVolumeCoef or 0.1
	self.inverterMinVolumeMG1 = jbeamData.inverterMinVolumeMG1 or jbeamData.inverterMinVolume or 0.00625
	self.inverterPitchMinAVMG1 = jbeamData.inverterPitchMinAVMG1 or jbeamData.inverterPitchMinAV or 1
	self.inverterPitchMaxAVMG1 = jbeamData.inverterPitchMaxAVMG1 or jbeamData.inverterPitchMaxAV or 1200
	self.inverterMinPitchMG1 = jbeamData.inverterMinPitchMG1 or jbeamData.inverterMinPitch or 0.0125
	self.inverterMaxPitchMG1 = jbeamData.inverterMaxPitchMG1 or jbeamData.inverterMaxPitch or 2

	-- MG2 inverter parameters
	self.inverterMinVolumeKwMG2 = jbeamData.inverterMinVolumeKwMG2 or jbeamData.inverterMinVolumeKw or 4
	self.inverterMaxVolumeKwMG2 = jbeamData.inverterMaxVolumeKwMG2 or jbeamData.inverterMaxVolumeKw or 120
	self.inverterVolumeCoefMG2 = jbeamData.inverterVolumeCoefMG2 or jbeamData.inverterVolumeCoef or 0.25
	self.inverterMinVolumeMG2 = jbeamData.inverterMinVolumeMG2 or jbeamData.inverterMinVolume or 0.05
	self.inverterPitchMinAVMG2 = jbeamData.inverterPitchMinAVMG2 or jbeamData.inverterPitchMinAV or 1
	self.inverterPitchMaxAVMG2 = jbeamData.inverterPitchMaxAVMG2 or jbeamData.inverterPitchMaxAV or 1800
	self.inverterMinPitchMG2 = jbeamData.inverterMinPitchMG2 or jbeamData.inverterMinPitch or 1
	self.inverterMaxPitchMG2 = jbeamData.inverterMaxPitchMG2 or jbeamData.inverterMaxPitch or 1

	-- Motor whine/gear sounds
	local gearWhineSoundSample = jbeamData.gearWhineSampleName or "vehicles/hybrid_common/sounds/motor_ramp.ogg"

	self.gearWhineSoundLoopMG2 = sounds.createSoundObj(gearWhineSoundSample, "AudioDefaultLoop3D", "GearWhineMG2", self.transmissionNodeID or sounds.engineNode)
	self.gearWhineSoundLoopPSD = sounds.createSoundObj(gearWhineSoundSample, "AudioDefaultLoop3D", "GearWhinePSD", self.transmissionNodeID or sounds.engineNode)

	self.gearWhineMinVolume = jbeamData.gearWhineMinVolume or 0
	self.gearWhineVolumeCoef = jbeamData.gearWhineVolumeCoef or 0.01
	self.gearWhineVolumeMinAV = jbeamData.gearWhineVolumeMinAV or 15
	self.gearWhineVolumeMaxAV = jbeamData.gearWhineVolumeMaxAV or 200
	self.gearWhinePitchMinAV = jbeamData.gearWhinePitchMinAV or 0
	self.gearWhinePitchMaxAV = jbeamData.gearWhinePitchMaxAV or 420
	self.gearWhineMinPitch = jbeamData.gearWhineMinPitch or 0.1
	self.gearWhineMaxPitch = jbeamData.gearWhineMaxPitch or 4

	-- Smoothers
	local motorAVSmoothing = jbeamData.gearWhineAVSmoothing or 50
	local motorTorqueSmoothing = jbeamData.gearWhineTorqueSmoothing or 10

	self.motorSoundAVSmootherMG1 = newExponentialSmoothing(motorAVSmoothing)
	self.motorSoundAVSmootherMG2 = newExponentialSmoothing(motorAVSmoothing)
	self.gearWhineAVSmootherPSD = newExponentialSmoothing(motorAVSmoothing)
	self.motorSoundTorqueSmootherMG1 = newExponentialSmoothing(motorTorqueSmoothing)
	self.motorSoundTorqueSmootherMG2 = newExponentialSmoothing(motorTorqueSmoothing)
	self.gearWhineTorqueSmootherPSD = newExponentialSmoothing(motorTorqueSmoothing)

	self.motorSoundAVMG1 = 0
	self.motorSoundAVMG2 = 0
	self.gearWhineAVPSD = 0
	self.motorSoundTorqueMG1 = 0
	self.motorSoundTorqueMG2 = 0
	self.gearWhineTorquePSD = 0
end

function ecvt:initTuningParameters()
	self.parkLockTuning:clearParameters()
	self.parkLockTuning:registerParameter("parkLockSpring", self.parkLockSpring or 0, 1, 100000, 100)
	self.parkLockTuning:registerParameter("parkLockFreePlay", self.parkLockFreePlay, 0, 1, 0.05)
	self.parkLockTuning:registerParameter("parkLockDampRatio", self.parkLockDampRatio, 0, 2, 0.05, function (_, newValue)
		self.parkLockDampRatio = newValue
		self.parkLockDamp = newValue * sqrt(self.parkLockSpring * self.outputInertia)
	end)
	self.parkLockTuning:registerWithUI()

	self.clutchTuning:clearParameters()
	self.clutchTuning:registerParameter("clutchStiffness", self.clutchStiffness or 0, 0, 5, 0.05)
	self.clutchTuning:registerParameter("clutchLockSpring", self.clutchLockSpring or 0, 0, 100000, 100)
	self.clutchTuning:registerParameter("clutchLockDampRatio", self.clutchLockDampRatio, 0, 2, 0.05, function (_, newValue)
		self.clutchLockDampRatio = newValue
		self.clutchLockDamp = newValue * sqrt(self.clutchLockSpring * self.clutchInertia)
	end)
	self.clutchTuning:registerWithUI()
end

function ecvt:resetSounds()
	self.motorSoundAVSmootherMG1:reset()
	self.motorSoundAVSmootherMG2:reset()
	self.gearWhineAVSmootherPSD:reset()
	self.motorSoundTorqueSmootherMG1:reset()
	self.motorSoundTorqueSmootherMG2:reset()
	self.gearWhineTorqueSmootherPSD:reset()

	self.motorSoundAVMG1 = 0
	self.motorSoundAVMG2 = 0
	self.gearWhineAVPSD = 0
	self.motorSoundTorqueMG1 = 0
	self.motorSoundTorqueMG2 = 0
	self.gearWhineTorquePSD = 0
end

function ecvt:reset()
	self.outputInertia = 1
	self.cumulativeInertia = 1
	self.cumulativeGearRatio = 1
	self.maxCumulativeGearRatio = 1

	self.clutchAngle = 0
	self.clutchRatio = 1
	self.torqueDiff = 0

	self.spragAngle = 0
	self.spragTorqueDiff = 0

	self.outputAV1 = 0
	self.inputAV = 0

	self.rawOutputTorque = 0
	self.outputTorque1 = 0
	self.lastOutputTorque = 0
	self.lastOutputTorqueFromInput = 0
	self.lastOutputTorqueFromMG = 0
	self.outputTorqueFromInput = 0
	self.outputTorqueFromMG2 = 0
	self.spentEnergyLastUpdate = 0
	self.spentEnergy = 0
	self.isBroken = false

	self.lockCoef = 1
	self.reverseGearRatioCoef = 1
	self.parkClutchAngle = 0

	table.clear(self.debugValues)

	self.psd:updateAVs(0, nil, 0)

	self.debug:clearValues()

	self:selectUpdates()
	self:initTuningParameters()
end

---@return EcvtGearbox
local function new(jbeamData)
	---@type EcvtGearbox
	local device = {
		deviceCategories = shallowcopy(M.deviceCategories),
		requiredExternalInertiaOutputs = shallowcopy(M.requiredExternalInertiaOutputs),
		outputPorts = shallowcopy(M.outputPorts),
		name = jbeamData.name,
		type = jbeamData.type,
		inputName = jbeamData.inputName,
		inputIndex = jbeamData.inputIndex,
		additionalEngineInertia = jbeamData.additionalEngineInertia or 0,
		counterRingGearRatio = jbeamData.counterRingGearRatio or (53 / 65),
		counterMG2GearRatio = jbeamData.counterMG2GearRatio or (53 / 17),
		inputGearRatio = jbeamData.inputGearRatio or 1.0,
		friction = jbeamData.friction or 5,
		dynamicFriction = jbeamData.dynamicFriction or 0,
		outputInertia = 1,
		cumulativeInertia = 1,
		cumulativeGearRatio = 1,
		maxCumulativeGearRatio = 1,
		isPhysicallyDisconnected = true,
		throttleFactor = 1,
		throttle = 0,

		clutchLockDampRatio = jbeamData.clutchLockDampRatio or 0.5, --1 is critically damped
		clutchStiffness = jbeamData.clutchStiffness or 0.9,
		clutchLockSpringBase = jbeamData.clutchLockSpring,
		clutchLockSpringCoef = jbeamData.clutchLockSpringCoef or 1,
		clutchLockTorque = jbeamData.clutchLockTorque,
		clutchAngle = 0,
		torqueDiff = 0,

		spragFriction = jbeamData.spragFriction or 5,
		spragLockDampRatio = jbeamData.spragLockDampRatio or 0.15, --1 is critically damped
		spragStiffness = jbeamData.spragStiffness or 1,
		spragLockSpringBase = jbeamData.spragLockSpring,
		spragLockSpringCoef = jbeamData.spragLockSpringCoef or 1,
		spragLockTorque = jbeamData.spragLockTorque,
		spragAngle = 0,
		spragTorqueDiff = 0,

		outputAV1 = 0,
		inputAV = 0,

		rawOutputTorque = 0,
		outputTorque1 = 0,
		outputTorqueFromInput = 0,
		outputTorqueFromMG2 = 0,
		lastOutputTorque = 0,
		lastOutputTorqueFromInput = 0,
		lastOutputTorqueFromMG2 = 0,
		spentEnergyLastUpdate = 0,
		spentEnergy = 0,
		motorGenerator1TorqueDemand = 0,
		motorGenerator1Torque = 0,
		motorGenerator1TorqueDirection = 1,
		motorGenerator2TorqueDemand = 0,
		motorGenerator2Torque = 0,

		motorGenerator1AV = 0,
		motorGenerator2AV = 0,

		motorGenerator1Power = 0,
		motorGenerator2Power = 0,

		isBroken = false,
		lockCoef = 1,
		reverseGearRatioCoef = 1,

		-- potentially needed for compatibility but not used
		gearCount = 1,
		minGearIndex = 1,
		maxGearIndex = 1,
		gearIndex = 1,

		debug = DebugModule:new(jbeamData.name or "ecvtGearbox", jbeamData.enableDebug),

		debugValues = {},
	}

	device.parkLockTuning = TuningUtility:new(("%s_parkLock"):format(jbeamData.name or "ecvtGearbox"), device)
	device.clutchTuning = TuningUtility:new(("%s_clutch"):format(jbeamData.name or "ecvtGearbox"), device)

	setmetatable(device.debugValues, {
		__index = function(t, k)
			return rawget(t, k) or 0
		end
	})

	device.visualType = "cvtGearbox"

	setmetatable(device, ecvt)

	local gearRadiusCoef = jbeamData.gearRadiusCoef or 0.0015
	local gearMassCoef = jbeamData.gearMassCoef or 50

	-- solo inertia of MG1
	device.mg1Inertia = jbeamData.mg1Inertia or 0.02

	-- solo inertia of MG2
	device.mg2Inertia = jbeamData.mg2Inertia or 0.022

	-- initialize planetary gearset
	device.psd = planetary.newPlanetaryGearset({
		sunGearTeeth = jbeamData.sunGearTeeth or 30,
		planetGearTeeth = jbeamData.planetGearTeeth or 23,
		ringGearTeeth = jbeamData.ringGearTeeth or 78,
		planetGearCount = jbeamData.planetGearCount or 4,
		gearRadiusCoef = gearRadiusCoef,
		gearMassCoef = gearMassCoef,
		additionalSunGearInertia = device.mg1Inertia,
	})

	device.gearRatio = device.inputGearRatio * device.psd.ringCarrierRatio * device.counterRingGearRatio

	--gearbox park locking clutch
	device.parkClutchAngle = 0
	device.parkLockTorque = jbeamData.parkLockTorque or 1000 --Nm
	device.parkLockFreePlay = jbeamData.parkLockFreePlay or 0 -- rad
	device.parkLockDampRatio = jbeamData.parkLockDampRatio or 0.15 -- 1 is critically damped
	device.parkLockSpringBase = jbeamData.parkLockSpring

	--electric stuff
	device.mg1ZeroSpeedMaxTorqueCoef = jbeamData.mg1ZeroSpeedMaxTorqueCoef or 3.0
	device.maxMG1AV = jbeamData.maxMG1AV or 1780 -- AV
	device.minMG1AV = jbeamData.minMG1AV or -device.maxMG1AV
	device.mg1PowerRating = jbeamData.mg1PowerRating or 40 -- kW
	device.mg1TorqueRating = jbeamData.mg1TorqueRating or 80 -- Nm
	device.mg1PeakEfficiencyAv = device.mg1PowerRating * 1000 / device.mg1TorqueRating -- av = power / torque, so peak AV = maxPower / maxTorque
	device.maxMG1PowerOverride = nil
	device.maxMG2AV = jbeamData.maxMG2AV or 1780 -- AV
	device.minMG2AV = jbeamData.minMG2AV or -device.maxMG2AV
	device.mg2PowerRating = jbeamData.mg2PowerRating or 53 -- kW
	device.mg2TorqueRating = jbeamData.mg2TorqueRating or 163 -- Nm
	device.mg2PeakEfficiencyAv = device.mg2PowerRating * 1000 / device.mg2TorqueRating -- av = power / torque, so peak AV = maxPower / maxTorque
	device.maxMG2PowerOverride = nil
	device.minPowerAV = jbeamData.minPowerAV or 15
	device.startingPowerCoef = jbeamData.startingPowerCoef or 1.5
	device.regenFadeMinAV = jbeamData.regenFadeMinAV or 40 -- AV
	device.regenFadeMaxAV = jbeamData.regenFadeMaxAV or 300 -- AV
	device.generatorMinFullLoadAV = jbeamData.generatorMinFullLoadAV or 30 -- AV

	if jbeamData.gearboxNode_nodes and type(jbeamData.gearboxNode_nodes) == "table" then
		device.transmissionNodeID = jbeamData.gearboxNode_nodes[1]
	end

	device.breakTriggerBeam = jbeamData.breakTriggerBeam
	if device.breakTriggerBeam and device.breakTriggerBeam == "" then
		--get rid of the break beam if it's just an empty string (cancellation)
		device.breakTriggerBeam = nil
	end

	device.jbeamData = jbeamData

	device:selectUpdates()
	device:initTuningParameters()

	return device
end

M.new = new

return M
