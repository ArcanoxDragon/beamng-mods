-- This Source Code Form is subject to the terms of the bCDDL, v. 1.1.
-- If a copy of the bCDDL was not distributed with this
-- file, You can obtain one at http://beamng.com/bCDDL-1.1.txt
local M = {}

local DebugModule = require("arcanox/DebugModule")
local TuningUtility = require("arcanox/TuningUtility")

---Torque Dampener (basically a torque converter with no manual lockup ratio and that doesn't expect its parent to be an engine)
---Author: Arcanox

M.outputPorts = { [1] = true }
M.deviceCategories = { shaft = true }
M.requiredExternalInertiaOutputs = { 1 }

local abs = math.abs
local min = math.min
local max = math.max
local sqrt = math.sqrt

local function updateDebug(device)
	device.maxOutputTorque = max(abs(device.outputTorque1), device.maxOutputTorque or 0)

	local avDiff = device.virtualAV - device.outputAV1
	local clutchFreePlay = device.freePlay
	local clutchAngle = device.clutchAngle
	local absFreeClutchAngle = max(abs(clutchAngle) - clutchFreePlay, 0)
	local lockDampAV = min(abs(clutchAngle) / clutchFreePlay, 1) * avDiff
	local springTorque = clamp(min(1, absFreeClutchAngle) * absFreeClutchAngle * fsign(clutchAngle) * device.lockSpring, -device.lockTorque, device.lockTorque)
	local dampingTorque = clamp(device.lockDamp * lockDampAV, -device.lockTorque, device.lockTorque)

	device.debug:putValue("Input AV", device.inputAV, 1)
	device.debug:putValue("Virtual AV", device.virtualAV, 1)
	device.debug:putValue("Output AV", device.outputAV1, 1)
	device.debug:putValue("AV Diff", device.virtualAV - device.outputAV1, 1)
	device.debug:putValue("Clutch Angle", device.clutchAngle, 1)
	device.debug:putValue("Spring Torque", springTorque, 1)
	device.debug:putValue("Damping Torque", dampingTorque, 1)
	device.debug:putValue("Output Torque", device.outputTorque1, 1)
	device.debug:putValue("Lock Coef", device.lockCoef)

	-- scaled simply so it will be more visible on the graph
	-- local scaledClutchAngle = (device.clutchAngle / device.maxClutchAngle) * device.maxOutputTorque

	-- device.debug:putGraphValue{"Virtual AV", device.virtualAV, "rad/s"}
	-- device.debug:putGraphValue{"Output AV", device.outputAV1, "rad/s"}
	-- device.debug:putGraphValue{"Clutch Angle", scaledClutchAngle, "rad"}
	-- device.debug:putGraphValue{"Spring Torque", springTorque / 100, "hNm"}
	-- device.debug:putGraphValue{"Damping Torque", dampingTorque, "Nm"}
	-- device.debug:putGraphValue{"Output Torque", device.outputTorque1 / 100, "hNm"}
end

local function updateVelocityNormal(device, dt)
	device.inputAV = device.virtualAV
	device.parent[device.parentOutputAVName] = device.inputAV
end

local function updateTorqueNormal(device, dt)
	-- simulate virtual mass when torqueDiff is low, or otherwise there's no output AV and thus no output torque
	local parentOutputTorque = device.parent.outputTorque1
	local virtualMassTorque = parentOutputTorque - device.torqueDiff

	device.virtualAV = device.virtualAV + virtualMassTorque * dt * device.invVirtualMassInertia

	local avDiff = device.virtualAV - device.outputAV1
	local clutchFreePlay = device.freePlay * device.damageClutchFreePlayCoef * device.wearClutchFreePlayCoef
	local clutchAngle = clamp(device.clutchAngle + avDiff * dt * device.stiffness, -device.maxClutchAngle - clutchFreePlay, device.maxClutchAngle + clutchFreePlay)

	local absFreeClutchAngle = max(abs(clutchAngle) - clutchFreePlay, 0)

	local lockTorque = device.lockTorque * device.damageLockTorqueCoef * device.wearLockTorqueCoef

	local lockDampAV = min(abs(clutchAngle) / clutchFreePlay, 1) * avDiff
	local torqueDiff = clamp(min(1, absFreeClutchAngle) * absFreeClutchAngle * fsign(clutchAngle) * device.lockSpring + device.lockDamp * lockDampAV, -lockTorque, lockTorque)

	device.clutchAngle = clutchAngle
	device.torqueDiff = torqueDiff
	device.outputTorque1 = torqueDiff
end

local function updateVelocityPark(device, dt)
	device.inputAV = device.outputAV1
	device.parent[device.parentOutputAVName] = device.inputAV
end

local function updateTorquePark(device, dt)
	device.outputTorque1 = device.parent[device.parentOutputTorqueName] * device.lockCoef
	device.virtualAV = device.inputAV
end

local function updateGFX(device, dt)
	device.lockCoef = clamp(device.lockCoef + (device.mode == "park" and 1 or -1) * dt * 2, 0, 1)

	if device.mode ~= device.parent.mode then
		device:setMode(device.parent.mode)
	end

	device:updateDebug()
end

local function printPowertrainInertia(device)
	local parent = device

	while parent.parent and not parent.parent.isFake do
		parent = parent.parent
	end

	local function printInertia(d, level)
		level = level or 0

		local indent = ""

		for i = 0, level do
			indent = indent .. "  "
		end

		print(string.format("%s%-" .. (40 - string.len(indent)) .. "s: inertia = %7.3f => cumulative = %7.3f", indent, d.name, d.inertia or 1, d.cumulativeInertia or 1))

		if d.children and #d.children > 0 then
			for _, child in ipairs(d.children) do
				printInertia(child, level + 1)
			end
		end
	end

	printInertia(parent)
end

local function selectUpdates(device)
	device.updateDebug = device.debug.enabled and updateDebug or nop
	device.updateGFX = updateGFX
	device.printPowertrainInertia = printPowertrainInertia

	if device.mode == "park" then
		device.velocityUpdate = updateVelocityPark
		device.torqueUpdate = updateTorquePark
	else
		device.velocityUpdate = updateVelocityNormal
		device.torqueUpdate = updateTorqueNormal
	end

	device.inputAV = device.outputAV1
	device.virtualAV = device.inputAV
	device.clutchAngle = 0
	device.torqueDiff = 0
end

local function applyDeformGroupDamage(device, damageAmount)
	device.damageClutchFreePlayCoef = device.damageClutchFreePlayCoef + linearScale(damageAmount, 0, 0.01, 0, 0.01)
	device.damageLockTorqueCoef = device.damageLockTorqueCoef - linearScale(damageAmount, 0, 0.01, 0, 0.05)

	device:calculateInertia()
end

local function setPartCondition(device, subSystem, odometer, integrity, visual)
	device.wearClutchFreePlayCoef = linearScale(odometer, 30000000, 500000000, 1, 10)
	device.wearLockTorqueCoef = linearScale(odometer, 30000000, 500000000, 1, 0.2)
	local integrityState = integrity
	if type(integrity) == "number" then
		local integrityValue = integrity
		integrityState = {
			damageClutchFreePlayCoef = linearScale(integrityValue, 1, 0, 1, 20),
			damageLockTorqueCoef = linearScale(integrityValue, 1, 0, 1, 0),
		}
	end

	device.damageClutchFreePlayCoef = integrityState.damageClutchFreePlayCoef or 1
	device.damageLockTorqueCoef = integrityState.damageLockTorqueCoef or 1

	device:calculateInertia()
end

local function getPartCondition(device)
	local integrityState = {
	  damageClutchFreePlayCoef = device.damageClutchFreePlayCoef,
	  damageLockTorqueCoef = device.damageLockTorqueCoef,
	}
	local integrityValueFreePlay = linearScale(device.damageClutchFreePlayCoef, 1, 20, 1, 0)
	local integrityValueLockTorque = linearScale(device.damageLockTorqueCoef, 1, 0, 1, 0)
	local integrityValue = min(integrityValueFreePlay, integrityValueLockTorque)

	return integrityValue, integrityState
end

local function validate(device)
	if not device.parent.deviceCategories.gearbox then
		log("E", "torqueDampener.validate", "Parent device is not a gearbox device...")
		log("E", "torqueDampener.validate", "Actual parent:")
		log("E", "torqueDampener.validate", powertrain.dumpsDeviceData(device.parent))
		return false
	end

	if type(device.lockTorque) ~= "number" then
		log("E", "torqueDampener.validate", "clutchLockTorque not specified in device JBeam")
		return false
	end

	device:initTuningParameters()

	return true
end

local function setMode(device, mode)
	device.mode = mode
	selectUpdates(device)
end

local function calculateInertia(device)
	local outputInertia = 0
	local cumulativeGearRatio = 1
	local maxCumulativeGearRatio = 1

	if device.children and #device.children > 0 then
		local child = device.children[1]
		outputInertia = child.cumulativeInertia
		cumulativeGearRatio = child.cumulativeGearRatio
		maxCumulativeGearRatio = child.maxCumulativeGearRatio
	end

	device.cumulativeInertia = outputInertia
	device.invCumulativeInertia = 1 / device.cumulativeInertia

	device.lockSpring = device.lockSpringBase or (powertrain.stabilityCoef * powertrain.stabilityCoef * device.cumulativeInertia * device.lockSpringCoef) --Nm/rad
	device.lockDamp = device.lockDampRatio * 2 * sqrt(device.lockSpring * device.cumulativeInertia)

	--^2 spring but linear spring after 1 rad
	device.maxClutchAngle = sqrt(device.lockTorque / device.lockSpring) + max(device.lockTorque / device.lockSpring - 1, 0)

	device.cumulativeGearRatio = cumulativeGearRatio
	device.maxCumulativeGearRatio = maxCumulativeGearRatio

	device:initTuningParameters()
end

function initTuningParameters(device)
	device.tuning:clearParameters()
	device.tuning:registerParameter("stiffness", device.stiffness or 0, 0, 2, 0.05)
	device.tuning:registerParameter("freePlay", device.freePlay or 0, 0, 1, 0.0125)
	device.tuning:registerParameter("lockSpring", device.lockSpring or 0, 0, 100000, 100)
	device.tuning:registerParameter("lockDampRatio", device.lockDampRatio, 0, 2, 0.05, function (_, newValue)
		device.lockDampRatio = newValue
		device.lockDamp = newValue * sqrt(device.lockSpring * device.cumulativeInertia)
	end)
	device.tuning:registerWithUI()
end

local function reset(device, jbeamData)
	device.cumulativeGearRatio = 1
	device.maxCumulativeGearRatio = 1

	device.outputAV1 = 0
	device.inputAV = 0
	device.virtualAV = 0
	device.outputTorque1 = 0
	device.clutchAngle = 0
	device.torqueDiff = 0
	device.isBroken = false

	device.damageClutchFreePlayCoef = 1
	device.damageLockTorqueCoef = 1
	device.wearClutchFreePlayCoef = 1
	device.wearLockTorqueCoef = 1

	selectUpdates(device)

	return device
end

local function new(jbeamData)
	local device = {
		deviceCategories = shallowcopy(M.deviceCategories),
		requiredExternalInertiaOutputs = shallowcopy(M.requiredExternalInertiaOutputs),
		outputPorts = shallowcopy(M.outputPorts),
		name = jbeamData.name,
		type = jbeamData.type,
		inputName = jbeamData.inputName,
		inputIndex = jbeamData.inputIndex,
		gearRatio = 1,
		cumulativeGearRatio = 1,
		maxCumulativeGearRatio = 1,
		isPhysicallyDisconnected = true,
		outputAV1 = 0,
		inputAV = 0,
		virtualAV = 0,
		outputTorque1 = 0,
		torqueDiff = 0,
		clutchAngle = 0,
		lockCoef = 0,
		damageClutchFreePlayCoef = 1,
		damageLockTorqueCoef = 1,
		wearClutchFreePlayCoef = 1,
		wearLockTorqueCoef = 1,
		isBroken = false,
		lockDampRatio = jbeamData.lockDampRatio or 0.15, --1 is critically damped
		stiffness = jbeamData.stiffness or 1,
		freePlay = jbeamData.freePlay or 0.125,
		lockSpringCoef = jbeamData.lockSpringCoef or 1,
		lockTorque = jbeamData.lockTorque,
		lockSpringBase = jbeamData.lockSpring,
		virtualMassInertia = jbeamData.virtualMassInertia or 1,
		reset = reset,
		setMode = setMode,
		validate = validate,
		calculateInertia = calculateInertia,
		applyDeformGroupDamage = applyDeformGroupDamage,
		setPartCondition = setPartCondition,
		getPartCondition = getPartCondition,
		initTuningParameters = initTuningParameters,

		debug = DebugModule:new(jbeamData.name or "torqueDampener", jbeamData.enableDebug)
	}

	device.tuning = TuningUtility:new(jbeamData.name or "torqueDampener", device)

	device.invVirtualMassInertia = 1 / device.virtualMassInertia
	device.breakTriggerBeam = jbeamData.breakTriggerBeam
	if device.breakTriggerBeam and device.breakTriggerBeam == "" then
		-- get rid of the break beam if it's just an empty string (cancellation)
		device.breakTriggerBeam = nil
	end

	selectUpdates(device)
	initTuningParameters(device)

	return device
end

M.new = new

return M
