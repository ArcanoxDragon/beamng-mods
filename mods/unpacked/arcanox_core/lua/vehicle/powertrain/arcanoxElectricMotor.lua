local M = {}
M.outputPorts = {[1] = true}
M.deviceCategories = {engine = true}
M.requiredExternalInertiaOutputs = {1}

-- imports
local mathutil = require("arcanox/mathutil")
local DebugModule = require("arcanox/DebugModule")

-- function aliases
local abs = math.abs
local min = math.min
local max = math.max
local clamp = mathutil.clamp
local sign = mathutil.sign
local maprange = mathutil.maprange

-- constants
local constants = {
	avToRPM = 9.549296596425384,
	dynamicFrictionCoef = 0.1047197177,
}

---@class ArcanoxMotor : PowertrainDevice
local Motor = {}
Motor.__index = Motor

function Motor:updateGFX(dt)
	self.spentEnergyLastUpdate = self.spentEnergy
	self.spentEnergy = 0

	if self.debug.enabled then
		self.debug:putValue("Motor RPM (AV)", { self.motorAV * constants.avToRPM, self.motorAV }, 2)
		self.debug:putValue("Motor Torque (Nm)", self.motorTorque, 3)
		self.debug:putValue("Motor Power (kW)", self.motorPower, 3)
		self.debug:putValue("Output Torque (Nm)", self.outputTorque1, 3)
		self.debug:putValue("Output Torque (Wheels, Nm)", self.outputTorque1 * self.cumulativeGearRatio, 3)
	end
end

function Motor:velocityUpdate() end -- just a nop

function Motor:torqueUpdate(dt)
	self.motorAV = self.outputAV1 * self.gearRatio

	local avSign = sign(self.motorAV)
	local outputRPM = self.motorAV * constants.avToRPM
	local motorDir = self.outputTorqueDirection
	local friction = self.friction
	local dynamicFriction = self.dynamicFriction
	local maxMotorTorque = self:getMaxMotorTorque(self.outputTorqueDemand < 0)
	local maxInstantTorque = maxMotorTorque - friction - (dynamicFriction * outputRPM * constants.dynamicFrictionCoef)
	local actualTorque

	if self.outputTorqueDemand < 0 then
		actualTorque = -avSign * min(-self.outputTorqueDemand, self:getMaxRegenTorque())
	else
		actualTorque = motorDir * min(self.outputTorqueDemand, maxMotorTorque)
	end

	actualTorque = clamp(actualTorque, -maxInstantTorque, maxInstantTorque) * (self.throttleFactor or 1)

	local frictionTorque = abs(friction * avSign + dynamicFriction * self.motorAV)

	frictionTorque = min(frictionTorque, abs(self.motorAV) * self.motorInertia * 2000) * avSign

	self.motorTorque = actualTorque
	self.rawOutputTorque = actualTorque * self.gearRatio
	self.outputTorque1 = self.rawOutputTorque - (frictionTorque * self.gearRatio)
	self.throttle = abs(actualTorque) / max(maxInstantTorque, 1e-30)

	local motorPowerCoef = self.outputTorqueDemand < 0 and 1 or self:getMotorPowerCoef(self.motorAV)
	local effectiveMotorAV = max(self.motorMinNoStallAV, abs(self.motorAV))

	self.motorPower = motorPowerCoef * sign(self.outputTorqueDemand) * abs(mathutil.torqueToKw(self.motorTorque, effectiveMotorAV))

	local grossWork = dt * self.motorPower * 1000 -- joules, not kJ, so multiply kW by 1000

	self.spentEnergy = self.spentEnergy + grossWork
end

function Motor:calculateInertia()
	local outputInertia = 0
	local cumulativeGearRatio = 1
	local maxCumulativeGearRatio = 1

	if self.children and #self.children > 0 then
		outputInertia = self.children[1].cumulativeInertia
		cumulativeGearRatio = self.children[1].cumulativeGearRatio
		maxCumulativeGearRatio = self.children[1].maxCumulativeGearRatio
	end

	self.cumulativeInertia = outputInertia / self.gearRatio / self.gearRatio
	self.cumulativeGearRatio = cumulativeGearRatio * self.gearRatio
	self.maxCumulativeGearRatio = maxCumulativeGearRatio * self.gearRatio
end

function Motor:getMaxMotorTorque(overrideOrIgnore, av)
	local motorAV = av and abs(av) or max(abs(self.motorAV), self.motorMinNoStallAV)
	local maxPower = self.motorPowerRating

	if type(overrideOrIgnore) == "number" then
		maxPower = min(overrideOrIgnore, maxPower)
	elseif (type(overrideOrIgnore) ~= "boolean" or overrideOrIgnore == false) and type(self.maxMotorPowerOverride) == "number" then
		maxPower = self.maxMotorPowerOverride
	end

	local maxTorque = mathutil.kwToTorque(maxPower, motorAV)

	return min(maxTorque, self.motorTorqueRating)
end

function Motor:getMaxRegenTorque(maxPower, av)
	local motorAV = av and abs(av) or abs(self.motorAV)
	local maxTorque = self:getMaxMotorTorque(maxPower or true)

	return maprange(motorAV, 0, self.motorMinFullRegenAV, 0, maxTorque)
end

function Motor:getMotorPowerCoef(motorAV)
	return maprange(abs(motorAV), 0, self.motorMinNoStallAV, self.motorStallPowerCoef, 1)
end

function Motor:setMotorPowerOverride(maxPowerOverride)
	self.maxMotorPowerOverride = maxPowerOverride
end

function Motor:setRequestedOutputTorque(requestedTorque)
	self.outputTorqueDemand = clamp(requestedTorque, -self.motorTorqueRating, self.motorTorqueRating)
end

function Motor:setMotorDirection(direction)
	self.outputTorqueDirection = (direction == -1) and -1 or 1
end

function Motor:validate()
	if not self.children or #self.children < 1 then
	  self.clutchChild = {torqueDiff = 0}
	elseif #self.children == 1 and self.children[1].deviceCategories.clutchlike then
	  self.clutchChild = self.children[1] ---@type ClutchDevice
	  self.invInertia = 1 / (self.motorInertia + (self.clutchChild.additionalEngineInertia or 0))
	  self.halfInvInertia = self.invInertia * 0.5
	end

	return true
end

function Motor:reset(jbeamData)
	self.gearRatio = jbeamData.gearRatio or 1
	self.friction = jbeamData.friction or 2
	self.outputAV1 = 0
	self.motorAV = 0
	self.inputAV = 0
	self.spentEnergyLastUpdate = 0
	self.spentEnergy = 0
	self.rawOutputTorque = 0
	self.outputTorque1 = 0
	self.outputTorqueDemand = 0
	self.outputTorqueDirection = 1
	self.motorPower = 0
	self.motorTorque = 0
	self.motorInertia = jbeamData.motorInertia or 0.1

	self.invGearRatio = 1 / self.gearRatio
end

---@return ArcanoxMotor
local function new(jbeamData)
	---@type ArcanoxMotor
	local device = {
		deviceCategories = shallowcopy(M.deviceCategories),
		requiredExternalInertiaOutputs = shallowcopy(M.requiredExternalInertiaOutputs),
		outputPorts = shallowcopy(M.outputPorts),
		name = jbeamData.name,
		type = jbeamData.type,

		gearRatio = jbeamData.gearRatio or 1,
		friction = jbeamData.friction or 2,
		dynamicFriction = jbeamData.dynamicFriction or 0,
		cumulativeInertia = 1,
		cumulativeGearRatio = 1,
		maxCumulativeGearRatio = 1,
		outputAV1 = 0,
		motorAV = 0,
		inputAV = 0,
		rawOutputTorque = 0,
		outputTorque1 = 0,
		outputTorqueDemand = 0,
		outputTorqueDirection = 1,
		motorPower = 0,
		motorTorque = 0,
		throttle = 0,
		spentEnergyLastUpdate = 0,
		spentEnergy = 0,

		motorTorqueRating = jbeamData.torqueRating or 60, -- Nm
		motorPowerRating = jbeamData.powerRating or 35,    -- kW
		maxMotorPowerOverride = nil,
		motorMaxAV = jbeamData.maxAV or 1780,  -- rads/s
		motorMinAV = jbeamData.minAV or -(jbeamData.maxAV or 1780), -- rad/s
		motorMinFullRegenAV = jbeamData.minFullRegenAV or 250,
		motorMinNoStallAV = jbeamData.minNoStallAV or 15,
		motorStallPowerCoef = jbeamData.stallPowerCoef or 1.5,
		motorInertia = jbeamData.motorInertia or 0.1,
		debug = DebugModule:new(jbeamData.name or "electricMotor", jbeamData.enableDebug),
	}

	setmetatable(device, Motor)

	device.visualType = "electricMotor"

	device.invGearRatio = 1 / device.gearRatio

	device.invInertia = 1 / device.motorInertia
	device.halfInvInertia = device.invInertia * 0.5

	return device
end

M.new = new

return M