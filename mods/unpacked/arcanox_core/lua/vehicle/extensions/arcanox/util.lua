---@class ArcUtilExtension
local M = {}

---@generic T: table
---@param table T
---@return T
local function withResetMethod(table)
	local initialState = {}

	for k, v in pairs(table) do
		initialState[k] = v
	end

	table.reset = function(self)
		if self ~= table then
			return
		end

		local mt = getmetatable(self)

		if type(mt) ~= "table" then
			mt = {}
		end

		for k, v in pairs(initialState) do
			local skip = false

			if type(mt.__noreset) == "table" and mt.__noreset[k] then
				skip = true
			end

			if not skip then
				table[k] = v
			end
		end
	end

	return table
end

M.withResetMethod = withResetMethod

return M
