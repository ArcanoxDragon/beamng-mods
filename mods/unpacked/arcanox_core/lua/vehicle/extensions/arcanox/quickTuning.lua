---@class ArcQuickTuningExtension
local M = {}

local registeredModules = {}
local numRegisteredModules = 0
local resetCalled = false

local weakMT = {
	__mode = "v"
}

setmetatable(registeredModules, weakMT)

local function getRegistration(module)
	local registration = {}

	registration.name = module.name
	registration.parameters = {}

	for _, param in ipairs(module:getTunableParameters()) do
		table.insert(registration.parameters, {
			id = param.id,
			name = param.name,
			value = param.value,
			min = param.min,
			max = param.max,
			step = param.step,
		})
	end

	return registration
end

local function sendRegistrationsToUI()
	local registrations = {}

	for _, module in pairs(registeredModules) do
		table.insert(registrations, getRegistration(module))
	end

	guihooks.trigger("ArcTuningRegister", registrations)
end

local function registerModule(module)
	if not module then
		return
	end

	if type(module.name) ~= "string" then
		log("E", "arcanox.extensions.quickTuning", "Module must have a \"name\" field")
		return
	end
	if type(module.updateParameter) ~= "function" then
		log("E", "arcanox.extensions.quickTuning", "Module must have an \"updateParameter\" method")
		return
	end
	if type(module.getTunableParameters) ~= "function" then
		log("E", "arcanox.extensions.quickTuning", "Module must have an \"getTunableParameters\" method")
		return
	end

	if registeredModules[module.name] == module then
		-- don't need to register the same module again
		return
	end
	if not registeredModules[module.name] then
		numRegisteredModules = numRegisteredModules + 1
	end

	registeredModules[module.name] = module

	if resetCalled then
		sendRegistrationsToUI()
	end
end

local function updateParameter(moduleName, parameterName, newValue)
	local module = registeredModules[moduleName]

	if not module then
		log("E", "arcanox.extensions.quickTuning", ("Unknown module: %q"):format(moduleName))
	end

	module:updateParameter(parameterName, newValue)
end

local function resetValues(moduleName)
	local module = registeredModules[moduleName]

	if not module then
		log("E", "arcanox.extensions.quickTuning", ("Unknown module: %q"):format(moduleName))
	end

	if type(module.resetValues) ~= "function" then
		return
	end

	module:resetValues()
	sendRegistrationsToUI()
end

local function resetValue(moduleName, parameterName)
	local module = registeredModules[moduleName]

	if not module then
		log("E", "arcanox.extensions.quickTuning", ("Unknown module: %q"):format(moduleName))
	end

	if type(module.resetValue) ~= "function" then
		return
	end

	module:resetValue(parameterName)
	sendRegistrationsToUI()
end

local function onPlayersChanged()
	sendRegistrationsToUI()
end

local function onReset()
	sendRegistrationsToUI()
	resetCalled = true
end

local function requestState()
	sendRegistrationsToUI()
end

M.onPlayersChanged = onPlayersChanged
M.onReset = onReset
M.registerModule = registerModule
M.updateParameter = updateParameter
M.resetValues = resetValues
M.resetValue = resetValue
M.requestState = requestState

return M
