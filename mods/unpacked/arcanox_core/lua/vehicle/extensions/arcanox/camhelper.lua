---@class ArcCamHelperExtension
local M = {}

-- constants
local updateInterval = 0.1

-- state
local updateTime = 0
local cameraInside = false

-- GE script snippet
local callbackScriptText = "extensions.arcanox_camhelper.updateCameraInside(%%s)"
local geScript = table.concat({
	"camPos = getCameraPosition()",
	"camInside = extensions.core_camera.isCameraInside(0, camPos)",
	-- Plop the callback script snippet into the GE script, which will then format it with the camInside value
	("callback = (%q):format(tostring(camInside))"):format(callbackScriptText),
	"be:queueObjectLua(%d, callback)",
}, "\n")

local function requestCameraState()
	local geScriptText = geScript:format(obj:getID())

	obj:queueGameEngineLua(geScriptText)
end

local function updateGFX(dt)
	updateTime = updateTime + dt

	if updateTime >= updateInterval then
		requestCameraState()

		updateTime = 0
	end
end

local function updateCameraInside(inside)
	cameraInside = inside
end

local function isCameraInside()
	return cameraInside
end

M.updateGFX = updateGFX
M.updateCameraInside = updateCameraInside
M.isCameraInside = isCameraInside

return M