---@class ArcDebugExtension
local M = {}

---@type table<string, DebugModule>
local registeredModules = {}
local numRegisteredModules = 0
local resetCalled = false

local weakMT = { __mode = "v" }

setmetatable(registeredModules, weakMT)

---Hacky helper function that allows accessing powertrain devices
-- and controllers via global shortcut objects. Useful for frequent
-- console access to stuff while debugging.
local function easyAccess()
	local function makeEasyAccess(name, table)
		local easyObj = {}

		setmetatable(easyObj, {
			__index = function(_, k)
				if type(table) == "function" then
					table = table()
				end
				if type(table) ~= "table" then
					log("E", "debug.easyAccess", ("EasyAccess base table for \"%s\" was not a table or table-returning function"):format(name))
					return nil
				end

				if table[k] then
					return table[k]
				end

				for k2, v2 in pairs(table) do
					if string.match(k2, "^" .. k) then
						return v2
					end
				end

				return nil
			end,
			__newindex = function()
				-- Don't add new stuff to the easy access object
			end,
		})

		rawset(_G, name, easyObj)
	end

	makeEasyAccess("pt", powertrain.getDevices)
	makeEasyAccess("ct", controller.getAllControllers)
	makeEasyAccess("evl", electrics.values)
end

-- NOTE: Uncomment the function call below to automatically register global easy-access obj
-- WARNING: Don't publish with this line uncommented!!!
-- easyAccess()

local function getDumpData()
	local dumpData = {}

	for tag, dbg in pairs(registeredModules) do
		if dbg.enabled then
			local values = {}

			for _, entry in pairs(dbg.debugValues) do
				if entry and not entry.destroy then
					values[entry.title] = shallowcopy(entry.value)
				end
			end

			dumpData[tag] = values
		end
	end

	return dumpData
end

local function debugDump()
	dump(getDumpData())
end

local function debugDumpToFile(filename)
	dumpToFile(filename, getDumpData())
end

local function freezeFrame()
	local time = os.time()
	local filename = os.date("debugFreezeFrame-%Y%m%d-%H%M%S.lua", time)

	debugDumpToFile(filename)
	gui.message({ txt = string.format("Freezeframe data saved to %s", filename) }, 5, "arcanox.debug.freezeframe." .. tostring(time))
end

---Sends all debug modules' statuses to the UI, optionally forcing a full update
---@param full? boolean
---| 'true' # A full update will be forced, causing all debug values to be sent, even if they have not changed since the last update
local function sendUpdateToUI(full)
	local update = {}

	for tag, dbg in pairs(registeredModules) do
		update[tag] = dbg:getUpdateStream(full)
	end

	guihooks.trigger("UpdateArcanoxDebug", update)
end

local function sendAllGraphStreams()
	if streams.willSend("profilingData") then
		local graphStream = {}
		local colorsNeeded = 0
		local modulesWithGraph = 0

		for _, dbg in pairs(registeredModules) do
			if dbg.enabled and dbg.graphValues and #dbg.graphValues > 0 then
				modulesWithGraph = modulesWithGraph + 1
			end
		end

		for tag, dbg in pairs(registeredModules) do
			if dbg.enabled then
				for i, graphData in ipairs(dbg.graphValues) do
					local entry = shallowcopy(graphData)

					if not entry.color then
						colorsNeeded = colorsNeeded + 1
					end

					if not entry.title then
						entry.title = "Row " .. tostring(i)
					end

					if modulesWithGraph > 1 then
						entry.title = "[" .. tag .. "] " .. entry.title
					end

					graphStream[#graphStream + 1] = entry
				end
			end
		end

		local curColor = 0

		for _, entry in pairs(graphStream) do
			if not entry.color then
				local color = rainbowColor(colorsNeeded, curColor, 255)

				entry.color = string.format("#%02x%02x%02x", color[1], color[2], color[3])
				curColor = curColor + 1
			end
		end

		gui.send("profilingData", graphStream)
	end
end

---Registers a DebugModule with the extension
---@param module DebugModule
local function registerModule(module)
	if not module then
		return
	end

	if type(module.logTag) ~= "string" then
		log("E", "arcanox.extensions.debug", "Module must have a \"logTag\" field")
		return
	end
	if type(module.getUpdateStream) ~= "function" then
		log("E", "arcanox.extensions.debug", "Module must have an \"getUpdateStream\" method")
		return
	end

	if registeredModules[module.logTag] == module then
		-- don't need to register the same module again
		return
	end
	if not registeredModules[module.logTag] then
		numRegisteredModules = numRegisteredModules + 1
	end

	registeredModules[module.logTag] = module

	if resetCalled then
		sendUpdateToUI(true)
	end
end

---Unregisters a DebugModule from the extension
---@param module DebugModule
local function unregisterModule(module)
	if not module.logTag or not registeredModules[module.logTag] then
		return
	end

	numRegisteredModules = numRegisteredModules - 1
	registeredModules[module.logTag] = nil

	if resetCalled then
		sendUpdateToUI(true)
	end
end

local function onReset()
	resetCalled = true
	sendUpdateToUI(true)
end

local function onPlayersChanged()
	sendUpdateToUI(true)
end

local function updateGFX()
	sendUpdateToUI()
	sendAllGraphStreams()
end

local function requestState(full)
	sendUpdateToUI(full)
end

M.easyAccess = easyAccess
M.getDumpData = getDumpData
M.debugDump = debugDump
M.debugDumpToFile = debugDumpToFile
M.freezeFrame = freezeFrame
M.onReset = onReset
M.onPlayersChanged = onPlayersChanged
M.registerModule = registerModule
M.unregisterModule = unregisterModule
M.updateGFX = updateGFX
M.requestState = requestState

return M
