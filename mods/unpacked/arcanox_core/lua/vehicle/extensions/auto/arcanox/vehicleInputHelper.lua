local M = {}

local actionMaps = { hybrid = "Arcanox_Hybrid", brakeHold = "Arcanox_BrakeHold" }
local controllerFlags = { ["arcanox/hybrid/hybridPowertrain"] = "hybrid", ["arcanox/brakeHold"] = "brakeHold" }

local function onPlayersChanged(playerSeated)
	if playerSeated then
		local vehFlags = {}

		for controllerType, flags in pairs(controllerFlags) do
			if type(flags) == "string" then
				flags = { flags }
			end

			if type(flags) == "table" then
				for _, flag in ipairs(flags) do
					local controllersOfType = controller.getControllersByType(controllerType)

					if #controllersOfType > 0 then
						vehFlags[flag] = true
					end
				end
			end
		end

		local cmd = ("arcanox_inputHelper.vehicleBecameActive(%d, %s)"):format(obj:getID(), serialize(vehFlags))

		obj:queueGameEngineLua(cmd)
	else
		local cmd = ("arcanox_inputHelper.vehicleWentInactive(%d)"):format(obj:getID())

		obj:queueGameEngineLua(cmd)
	end
end

local function onExtensionLoaded()
	for flag, actionMapName in pairs(actionMaps) do
		obj:queueGameEngineLua(("arcanox_inputHelper.registerActionMap(%q, %q)"):format(flag, actionMapName))
	end
end

M.onPlayersChanged = onPlayersChanged
M.onExtensionLoaded = onExtensionLoaded

return M
