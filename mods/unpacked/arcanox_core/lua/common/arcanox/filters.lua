local M = {}

local utils = require("arcanox/utils")

local min = math.min
local max = math.max

-- #region TimedLatch

---A `timedLatch` can be polled every update with a value, and if the value remains in one state for a
-- specific amount of time, the output of the latch will also change. It can be thought of as a
-- steady-state latch, requiring the input to remain steady for a certain amount of time before the output
-- will change at all.
---@class TimedLatch
---@field value any
local TimedLatch = {}
TimedLatch.__index = TimedLatch

---@class timedLatchConfigNamed
-- The amount of time the input must remain steady for the output to change
---@field timeToLatch number
-- The initial value of the latch
---@field initialValue any
-- An array of input values that will cause the latch's output to change immediately as opposed to waiting
-- for the `timeToLatch` to pass
---@field immediateLatchTo any[]
-- An array of values that, when the latch's output is currently one of them, will cause the latch's output
-- to change immediately on any input as opposed to waiting for the `timeToLatch` to pass
---@field immediateLatchFrom any[]

---@class timedLatchConfigPositional
-- The amount of time the input must remain steady for the output to change
---@field [1] number
-- The initial value of the latch
---@field [2] any
-- An array of input values that will cause the latch's output to change immediately as opposed to waiting
-- for the `timeToLatch` to pass
---@field [3] any[]
-- An array of values that, when the latch's output is currently one of them, will cause the latch's output
-- to change immediately on any input as opposed to waiting for the `timeToLatch` to pass
---@field [4] any[]

---@alias timedLatchConfig timedLatchConfigPositional | timedLatchConfigNamed

---@overload fun(timeToLatch: number, initialValue: any): TimedLatch
---@overload fun(config: timedLatchConfig): TimedLatch
function TimedLatch:new(...)
	local args = { ... }
	local timeToLatch
	local initialValue
	local immediateLatchTo
	local immediateLatchFrom

	if #args == 1 and type(args[1]) == "table" then
		-- Config object passed in
		local config = args[1] ---@type timedLatchConfig

		timeToLatch = config[1] or config.timeToLatch
		initialValue = config[2] or config.initialValue
		immediateLatchTo = config[3] or config.immediateLatchTo
		immediateLatchFrom = config[4] or config.immediateLatchFrom
	else
		-- Positional arguments
		timeToLatch = args[1]
		initialValue = args[2]
	end

	---@type TimedLatch
	local obj = {
		timeToLatch = timeToLatch,
		initialValue = initialValue or false,
		value = initialValue or false,
		elapsedTime = 0,
		immediateLatchToValues = {},
		immediateLatchFromValues = {},
	}

	if immediateLatchTo and type(immediateLatchTo) == "table" then
		for _, v in ipairs(immediateLatchTo) do
			obj.immediateLatchToValues[v] = true
		end
	end

	if immediateLatchFrom and type(immediateLatchFrom) == "table" then
		for _, v in ipairs(immediateLatchFrom) do
			obj.immediateLatchFromValues[v] = true
		end
	end

	setmetatable(obj, self)
	return obj
end

---Polls the latch with the provided input value and returns the output value of the latch
---@param value any The input value with which to poll the latch
---@param dt number The time delta since the last poll
---@return any # The output value of the latch
function TimedLatch:get(value, dt)
	if self.immediateLatchToValues[value] or self.immediateLatchFromValues[self.value] then
		self.value = value
	end

	if value == self.value then
		self.elapsedTime = 0
	else
		self.elapsedTime = self.elapsedTime + dt
	end

	if self.elapsedTime >= self.timeToLatch then
		self.elapsedTime = 0
		self.value = value
	end

	return self.value
end

---An alias for `:get(value, dt)`
---@param value any The input value with which to poll the latch
---@param dt number The time delta since the last poll
---@return any # The output value of the latch
function TimedLatch:test(value, dt)
	-- Just an alias for :get(); reads better when using a boolean value
	return self:get(value, dt)
end

---Resets the latch to either the provided value, or its initial value if no value is given
---@param value? any The value to which to reset the latch. Defaults to the latch's `initialValue`
function TimedLatch:reset(value)
	self.elapsedTime = 0
	self.value = value or self.initialValue
end

M.TimedLatch = TimedLatch

-- #endregion

-- #region ThresholdLatch

---A threshold latch can be `set` with a particular value, where it will remain until
-- that value exceeds the set value by a given threshold in either direction
---@class ThresholdLatch
local ThresholdLatch = {}
ThresholdLatch.__index = ThresholdLatch

---@param thresholdWidth number The amount in either direction by which the test value must exceed the set value for the latch to reset
---@return ThresholdLatch
function ThresholdLatch:new(thresholdWidth)
	---@type ThresholdLatch
	local obj = { thresholdWidth = thresholdWidth, state = false, setValue = nil }

	setmetatable(obj, self)
	return obj
end

---Sets the latch with the given value
---@param value number The initial value to which the latch is set
function ThresholdLatch:set(value)
	self.setValue = value
	self.state = true
end

---Gets the current set value for the latch
---@return number
function ThresholdLatch:getSetpoint()
	return self.setValue
end

---Gets whether the latch is active or not
---@return boolean
function ThresholdLatch:get()
	return self.state
end

---Queries the latch with the given value
---@param value number The test value with which to query
---@return
---| 'true'  # The latch is still set
---| 'false' # The latch has unlatched (i.e. the test value has exceeded the threshold relative to the set value)
function ThresholdLatch:test(value)
	if self.setValue ~= nil and math.abs(value - self.setValue) > self.thresholdWidth then
		self.state = false
		self.setValue = nil
	end

	return self.state
end

--- Resets (unsets) the latch
function ThresholdLatch:reset()
	self.state = false
	self.setValue = nil
end

M.ThresholdLatch = ThresholdLatch

-- #endregion

-- #region ThresholdGate

---A threshold gate will become enabled once the test value exceeds the "on" threshold, and
-- will remain enabled until the test value exceeds the "off" value in the opposite direction
---@class ThresholdGate
local ThresholdGate = {}
ThresholdGate.__index = ThresholdGate

---@param thresholdOn   number   The "on" threshold for the gate
---@param thresholdOff  number   The "off" threshold for the gate
---@param initialState? boolean  The initial state of the gate. Defaults to off.
---@return ThresholdGate
function ThresholdGate:new(thresholdOn, thresholdOff, initialState)
	---@type ThresholdGate
	local obj = { thresholdOn = thresholdOn, thresholdOff = thresholdOff, state = initialState or false, initialState = initialState or false }

	setmetatable(obj, self)
	return obj
end

---Queries the gate with the given value
---@param value number  The test value with which to query
---@return
---| 'true'  # The gate is still enabled
---| 'false' # The gate has become disabled
function ThresholdGate:test(value)
	local testThreshold = self.state and self.thresholdOff or self.thresholdOn

	if self.thresholdOn > self.thresholdOff then
		self.state = value >= testThreshold
	else
		self.state = value <= testThreshold
	end

	return self.state
end

---Gets whether the gate is active or not
function ThresholdGate:get()
	return self.state
end

---Resets the gate to its initial state
function ThresholdGate:reset()
	self.state = self.initialState
end

M.ThresholdGate = ThresholdGate

-- #endregion

-- #region Backlash

---@class Backlash
local Backlash = {}
Backlash.__index = Backlash

---@overload fun(window: number, initialValue?: number, min?: number, max?: number): Backlash
---@overload fun(options: { window: number, initialValue?: number, min?: number, max?: number }): Backlash
function Backlash:new(...)
	local args = utils.parseArgs({ ... }, "window", "initialValue", "min", "max")

	---@type Backlash
	local obj = {
		window = args.window or 1,
		initialValue = args.initialValue or 0,
		state = args.initialValue or 0,
		min = args.min,
		max = args.max,
	}

	obj.halfWindow = obj.window * 0.5

	setmetatable(obj, self)
	return obj
end

function Backlash:get(value)
	local st = clamp(self.state, value - self.halfWindow, value + self.halfWindow)

	if self.min then
		st = max(st, self.min)
	end
	if self.max then
		st = min(st, self.max)
	end

	self.state = st
	return st
end

function Backlash:setWindow(window)
	self.window = window
	self.halfWindow = window * 0.5
	self:get(self.state) -- Ensure state gets clamped immediately
end

function Backlash:reset()
	self.state = self.initialValue
end

M.Backlash = Backlash

-- #endregion

return M
