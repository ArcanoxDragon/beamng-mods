local DebugModule = require("arcanox/DebugModule")
local mathutil = require("arcanox/mathutil")

---@class Pid
local Pid = {}
Pid.__index = Pid;

---@class PidConfig
---@field debug? boolean
---@field deltaSmoother? any
---@field graphDebug? boolean
---@field integralLockAutoCenter? boolean
---@field integralLockDeltaThreshold? number
---@field integralLockErrorThreshold? number
---@field kD? number
---@field kI? number
---@field kIIn? number
---@field kIOut? number
---@field kP? number
---@field kPIn? number
---@field kPOut? number
---@field maxError? number
---@field maxIntegral? number
---@field maxOutput? number
---@field minIntegral? number
---@field minOutput? number
---@field name? string
---@field scale? number

---@return Pid
---@param config PidConfig
function Pid:new(config)
	config = config or {}

	---@type Pid
	local obj = {
		kPIn = config.kPIn or config.kP or 0,
		kPOut = config.kPOut or config.kP or 0,
		kIIn = config.kIIn or config.kI or 0,
		kIOut = config.kIOut or config.kI or 0,
		kD = config.kD or 0,
		minOutput = config.minOutput or -math.huge,
		maxOutput = config.maxOutput or math.huge,
		scale = config.scale or 1,
		name = config.name or "pid",
		iLockDeltaThreshold = config.integralLockDeltaThreshold,
		iLockErrorThreshold = config.integralLockErrorThreshold,
		iLockAutoCenter = config.integralLockAutoCenter == true,
		debug = config.debug == true,
		graphDebug = config.graphDebug == true,
		output = 0,

		lastValue = nil,
		lastError = nil,
		integral = 0,
		lockIntegral = false,
		dynamicIntegralLock = false,

		-- state variables to avoid a bunch of locals
		target = 0,
		actual = 0,
		err = 0,
		delta = 0,
		pFactor = 0,
		iFactor = 0,
		dFactor = 0,
		integralLocked = 0,
	}

	if config.maxError then
		obj.maxError = math.abs(config.maxError * obj.scale)
	end

	if config.deltaSmoother then
		if type(config.deltaSmoother) ~= "table" or type(config.deltaSmoother.get) ~= "function" then
			error("deltaSmoother must be a smoothing object with a :get() method")
		end

		obj.deltaSmoother = config.deltaSmoother
	end

	obj.minIntegral = config.minIntegral or obj.minOutput
	obj.maxIntegral = config.maxIntegral or obj.maxOutput
	obj.debugutil = DebugModule:new("pid_" .. obj.name, obj.debug or obj.graphDebug)

	setmetatable(obj, self)
	return obj
end

function Pid:update(target, actual, dt)
	self.target = target
	self.actual = actual

	-- calculate early P/D values
	local err = target - actual

	self.err = err * self.scale

	if self.maxError then
		self.err = mathutil.clamp(self.err, -self.maxError, self.maxError)
	end

	-- calculate delta
	self.delta = self.lastValue and ((self.actual - self.lastValue) * self.scale / dt) or 0

	if self.deltaSmoother then
		self.delta = self.deltaSmoother:get(self.delta, dt)
	end

	self.pFactor = (self.err * actual > 0 and self.kPOut or self.kPIn) * self.err
	self.dFactor = self.kD * self.delta

	-- apply integral locking/diff
	local deltaIntegralLock = self.iLockDeltaThreshold and math.abs(self.delta) > self.iLockDeltaThreshold
	local errorIntegralLock = self.iLockErrorThreshold and math.abs(err) > self.iLockErrorThreshold

	self.dynamicIntegralLock = deltaIntegralLock or errorIntegralLock
	self.integralLocked = self.lockIntegral or self.dynamicIntegralLock

	if self.dynamicIntegralLock and self.iLockAutoCenter then
		if math.abs(self.integral) > self.kIIn * dt then
			self.integral = self.integral - self.kIIn * dt * fsign(self.integral)
		else
			self.integral = 0
		end
	end

	if not self.integralLocked then
		self.integral = mathutil.clamp(self.integral + self.err * (self.err * actual > 0 and self.kIOut or self.kIIn) * dt, self.minIntegral, self.maxIntegral)
	end

	-- apply integral factor
	self.iFactor = self.integral

	-- set output and last-tick values
	self.output = mathutil.clamp(self.pFactor + self.iFactor - self.dFactor, self.minOutput, self.maxOutput)
	self.lastValue = actual
	self.lastError = self.err
end

function Pid:get(target, actual, dt)
	if target and actual and dt then -- all three parameters provided; perform an update too
		self:update(target, actual, dt)
	end

	return self.output
end

function Pid:peek()
	return self.output
end

function Pid:setConfig(config)
	config = config or {}

	if config.integralLockThreshold then
		self.iLockDeltaThreshold = config.integralLockThreshold
	end

	if config.integralLockAutoReset then
		self.iLockAutoCenter = config.integralLockAutoReset
	end

	config.integralLockThreshold = nil
	config.integralLockAutoReset = nil

	for k, v in pairs(config) do
		if v ~= nil then
			self[k] = v
		end
	end

	self.debugutil.enabled = self.debug == true
end

function Pid:reset(soft)
	if soft then
		self.integral = self.integral - (self.integral * self.kIOut)
	else
		self.integral = 0
	end

	if self.deltaSmoother then
		if type(self.deltaSmoother.reset) == "function" then
			self.deltaSmoother:reset()
		elseif type(self.deltaSmoother.set) == "function" then
			self.deltaSmoother:set(0)
		end
	end

	return self
end

function Pid:setLockIntegral(lockIntegral)
	self.lockIntegral = lockIntegral or false

	return self
end

function Pid:updateDebugValues()
	if self.debug then
		self.debugutil:putValue("Scale", self.scale, 3)
		self.debugutil:putValue("Target", self.target, 3)
		self.debugutil:putValue("Actual", self.actual, 3)
		self.debugutil:putValue("Delta (Raw, Scaled)", {self.delta, self.delta / self.scale}, 3)
		self.debugutil:putValue("Error (Raw, Scaled)", {self.err, self.err / self.scale}, 3)
		self.debugutil:putValue("Integral", self.integral, 3)
		self.debugutil:putValue("Last Value", self.lastValue, 3)
		self.debugutil:putValue("Last Error", self.lastError, 3)
		self.debugutil:putValue("P Factor", self.pFactor, 3)
		self.debugutil:putValue("I Factor", self.iFactor, 3)
		self.debugutil:putValue("D Factor", -self.dFactor, 3)
		self.debugutil:putValue("I Lock Threshold", self.iLockDeltaThreshold)
		self.debugutil:putValue("I Lock", self.integralLocked and "True" or "False")
		self.debugutil:putValue("Output", self.output, 3)
	end

	if self.graphDebug then
		self.debugutil:putGraphValue{"Target", self.target * self.scale}
		self.debugutil:putGraphValue{"Actual", self.actual * self.scale}
		self.debugutil:putGraphValue{"P", self.pFactor}
		self.debugutil:putGraphValue{"I", self.iFactor}
		self.debugutil:putGraphValue{"D", -self.dFactor}
		self.debugutil:putGraphValue{"Output", self.output}
	end
end

return Pid