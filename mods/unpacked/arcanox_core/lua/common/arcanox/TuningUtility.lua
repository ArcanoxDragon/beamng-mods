---@class TuningUtility
local TuningUtility = {}
TuningUtility.__index = TuningUtility

---@alias ParameterHandler fun(name: string, newValue: any)

---@class ParameterDescription
---@field id            number            The ID of the parameter
---@field name          string            The name of the parameter
---@field value         any               The current value of the parameter
---@field min           number            The minimum value of the parameter
---@field max           number            The maximum value of the parameter
---@field step          number            The smallest increment by which the parameter may be changed in a tuning UI

---@class Parameter : ParameterDescription
---@field initialValue  any               The initial value of the parameter
---@field handler       ParameterHandler  The update handler for the parameter

---@return ParameterHandler
local function getGenericParameterHandler(receiver)
	return function(name, value)
		receiver[name] = value
	end
end

---@param name string     The name of this tuning utility
---@param receiver table  The receiver that will receive parameter changes
---@return TuningUtility
function TuningUtility:new(name, receiver)
	---@type TuningUtility
	local obj = { name = name, receiver = receiver }

	obj.parameters = {} ---@type table<string, Parameter>
	obj.numParameters = 0

	setmetatable(obj, self)
	return obj
end

---Clears all registered parameters from this tuning utility
function TuningUtility:clearParameters()
	self.parameters = {}
	self.numParameters = 0
end

---Registers a new parameter to be tunable by this tuning utility
---@param name          string            The name of the parameter (as it will be displayed on tuning clients)
---@param initialValue  any               The initial value of the parameter (the value it will be set to on a reset)
---@param min           number            The minimum value of the parameter
---@param max           number            The maximum value of the parameter
---@param step          number            The smallest increment by which the parameter may be changed in a tuning UI
---@param handler?      ParameterHandler  An optional function that will be called when the parameter value changes.
-- If no handler is provided, the field matching the parameter name on this utility's receiver will be
-- directly set to the new value.
function TuningUtility:registerParameter(name, initialValue, min, max, step, handler)
	if self.parameters[name] then
		log("E", "arcanox.TuningUtility.registerParameter", ("A parameter named %q is already registered"):format(name))
		return
	end
	if type(initialValue) ~= "number" or type(min) ~= "number" or type(max) ~= "number" or type(step) ~= "number" then
		log("E", "arcanox.TuningUtility.registerParameter", "Invalid arguments given to registerParameter")
		log("E", "arcanox.TuningUtility.registerParameter", debug.traceback())
		return
	end
	if type(handler) ~= "function" then
		handler = getGenericParameterHandler(self.receiver)
	end

	self.numParameters = self.numParameters + 1

	---@type Parameter
	self.parameters[name] = {
		id = self.numParameters,
		name = name,
		initialValue = initialValue,
		value = initialValue,
		min = min,
		max = max,
		step = step,
		handler = handler,
	}
end

---Gets an array of all tunable parameters supported by this tuning utility
---@return ParameterDescription[]
function TuningUtility:getTunableParameters()
	---@type ParameterDescription[]
	local parameters = {}

	for name, parameter in pairs(self.parameters) do
		---@type ParameterDescription
		local parameterDesc = { id = parameter.id, name = name, min = parameter.min, max = parameter.max, step = parameter.step, value = parameter.value }

		table.insert(parameters, parameterDesc)
	end

	return parameters
end

---Updates the value of the parameter with the provided name
---@param name   string  The name of the parameter to update
---@param value  any     The new value for the parameter
function TuningUtility:updateParameter(name, value)
	local parameter = self.parameters[name]

	if not parameter then
		return
	end

	local number = tonumber(value)

	if not number then
		log("E", "arcanox.TuningUtility.updateParameter", ("Got non-number value %q when setting parameter %q in tuning utility %q!"):format(value, name, self.name))
		return
	end

	parameter.handler(name, value)
end

---Resets the parameter with the provided name to its initial value
---@param name string  The name of the parameter to reset
function TuningUtility:resetValue(name)
	for name, parameter in pairs(self.parameters) do
		self:updateParameter(name, parameter.initialValue)
	end
end

---Resets all parameters for this tuning utility to their initial values
function TuningUtility:resetValues()
	for name, parameter in pairs(self.parameters) do
		self:updateParameter(name, parameter.initialValue)
	end
end

---Registers this tuning utility and all its currently-registered parameters with the currently available tuning UI(s)
-- Note that any parameters registered after this method is called will not show up in the UI unless this method is called again.
function TuningUtility:registerWithUI()
	extensions.arcanox_quickTuning.registerModule(self)
end

return TuningUtility
