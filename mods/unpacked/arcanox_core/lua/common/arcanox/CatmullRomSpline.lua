local mathutil = require("arcanox/mathutil")

local floor = math.floor
local min = math.min
local clamp = mathutil.clamp

---@class Segment
---@field a          LuaVec3
---@field b          LuaVec3
---@field c          LuaVec3
---@field d          LuaVec3
---@field start      LuaVec3
---@field finish     LuaVec3
---@field previous?  Segment
---@field next?      Segment
local Segment = {}
Segment.__index = Segment

function Segment:new(a, b, c, d)
	---@type Segment
	local segment = { a = a, b = b, c = c, d = d }

	setmetatable(segment, self)

	return segment
end

---Returns the point along the segment at time `t`
---@param t number The time value at which to get a point (in the range [0..1])
---@return LuaVec3
function Segment:get(t)
	local t2 = t ^ 2
	local t3 = t ^ 3

	return self.a * t3 + self.b * t2 + self.c * t + self.d
end

---Calculates one segment of a Catmull-Rom spline
---@param points   LuaVec3[]  An array of control points representing a superset of the spline segment
---@param index    number     The index of the segment along the spline, with 0 being the first
---@param alpha    number     An optional alpha value for the Catmull-Rom interpolation algorithm (defaults to 0.5)
---@param tension  number	 An optional tension value for the Catmull-Rom interpolation algorithm (defaults to 0.0)
---@return Segment
local function calculateSegment(points, index, alpha, tension)
	if index < 1 or index >= #points - 2 then
		error("Index must be between 1 and two less than the number of control points")
	end

	index = floor(index) -- Just to be safe

	local i0 = index - 1
	local i1 = index
	local i2 = index + 1
	local i3 = index + 2

	local p0 = points[i0 + 1]
	local p1 = points[i1 + 1]
	local p2 = points[i2 + 1]
	local p3 = points[i3 + 1]

	local a2 = alpha / 2 -- precalculated power such that x ^ a2 = sqrt(x) ^ alpha
	local t0 = 0
	local t1 = t0 + p0:squaredDistance(p1) ^ a2
	local t2 = t1 + p1:squaredDistance(p2) ^ a2
	local t3 = t2 + p2:squaredDistance(p3) ^ a2

	---@type LuaVec3
	local m1 = (1.0 - tension) * (t2 - t1) *
		           ((p1 - p0) / (t1 - t0) - (p2 - p0) / (t2 - t0) + (p2 - p1) /
			           (t2 - t1))
	---@type LuaVec3
	local m2 = (1.0 - tension) * (t2 - t1) *
		           ((p2 - p1) / (t2 - t1) - (p3 - p1) / (t3 - t1) + (p3 - p2) /
			           (t3 - t2))

	---@type LuaVec3
	local a = 2 * p1 - 2 * p2 + m1 + m2
	---@type LuaVec3
	local b = -3 * p1 + 3 * p2 - 2 * m1 - m2
	---@type LuaVec3
	local c = m1
	---@type LuaVec3
	local d = p1

	local segment = Segment:new(a, b, c, d)

	segment.start = p1
	segment.finish = p2

	return segment
end

---@class CatmullRom
---@field private segments Segment[]
local CatmullRom = {}
CatmullRom.__index = CatmullRom

--- Creates a new instance of a CatmullRom spline
---@param points   LuaVec3[]  An array of control points defining the spline's curve
---@param alpha    number     An optional alpha value for the Catmull-Rom interpolation algorithm (defaults to 0.5)
---@param tension  number	 An optional tension value for the Catmull-Rom interpolation algorithm (defaults to 0.0)
function CatmullRom:new(points, alpha, tension)
	if #points < 2 then
		error("At least two control points are required")
	end

	points = shallowcopy(points)
	alpha = clamp(alpha or 0.5, 0, 1)
	tension = clamp(tension or 0.0, 0, 1)

	-- Must insert additional control points at start and end of spline
	---@type LuaVec3
	local p0 = 2 * (points[1] - points[2])
	---@type LuaVec3
	local pN = 2 * (points[#points] - points[#points - 1])

	table.insert(points, 1, p0)
	table.insert(points, pN)

	local segments = {} ---@type Segment[]
	local lastSegment ---@type Segment

	for i = 1, #points - 3 do
		local segment = calculateSegment(points, i, alpha, tension)

		segment.previous = lastSegment

		if lastSegment then
			lastSegment.next = segment
		end

		table.insert(segments, segment)
		lastSegment = segment
	end

	---@type CatmullRom
	local spline = { segments = segments }

	setmetatable(spline, self)

	return spline
end

---Returns the segment on which the time value `t` falls
---@param t number
---@return Segment
function CatmullRom:segment(t)
	local segmentFrac = t * #self.segments
	local segmentIndex = min(floor(segmentFrac) + 1, #self.segments)

	return self.segments[segmentIndex]
end

---Returns a point along the spline at time `t`
---@param t number The time value at which to get a point (in the range [0..1])
---@return LuaVec3
function CatmullRom:get(t)
	local segmentFrac = t * #self.segments
	local segmentTime = t == 1 and 1 or segmentFrac % 1 -- 1 % 1 == 0, but we want "t = 1" to result in the end of the last segment

	return self:segment(t):get(segmentTime)
end

return CatmullRom
