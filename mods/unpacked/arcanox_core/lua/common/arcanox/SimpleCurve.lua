local mathutil = require("arcanox/mathutil")
local CatmullRom = require("arcanox/CatmullRomSpline")

local min = math.min
local max = math.max
local floor = math.floor
local ceil = math.ceil
local maprange = mathutil.maprange
local clamp = mathutil.clamp
local blend = mathutil.blend

---@class SimpleCurveConfig
---@field [1]?        table   Positional argument for `table` when not specified by key
---@field [2]?        string  Positional argument for `xAxisName` when not specified by key
---@field [3]?        string  Positional argument for `yAxisName` when not specified by key
---@field [4]?        table   Positional argument for `default` when not specified by key
---@field table       table   A Jbeam table of points from which to create the curve
---@field xAxisName   string  The name of the header for the X-axis values
---@field yAxisName   string  The name of the header for the Y-axis values
---@field name?       string  An optional name for the curve itself
---@field default?    table   An optional table to be used in place of `table` if it is nil
---@field resolution? number  An optional number to specify how many interpolated points are created when the curve is compiled
---@field alpha?      number  An optional value to use as the `alpha` value in the Catmull-Rom formula
---@field tension?    number  An optional value to use as the `tension` value in the Catmull-Rom formula

--- A class that takes a JBeam table of points and interpolates smoothly
--- between them. Supports curves with small X-values, unlike the game's
--- built-in createCurve function
---@class SimpleCurve
---@field public   minX       number  The minimum X value on the curve
---@field public   minY       number  The maximum X value on the curve
---@field public   maxX       number  The minimum Y value on the curve
---@field public   maxY       number  The maximum Y value on the curve
---@field public   xAxisName  string  The name of the X-axis column header on the table from which the curve was created
---@field public   yAxisName  string  The name of the Y-axis column header on the table from which the curve was created
---@field public   name?      string  An optional name for the curve itself
---@field private  resolution number
---@field private  points     LuaVec3[]
---@field private  values     table<number, number>
---@field private  loopsBack  boolean
local SimpleCurve = {}

---Creates a new `SimpleCurve` instance
---@param config SimpleCurveConfig
---@return SimpleCurve
function SimpleCurve:new(config)
	config = config or {}

	local headerTable = config[1] or config.table
	local xAxisName = config[2] or config.xAxisName
	local yAxisName = config[3] or config.yAxisName
	local default = config[4] or config.default
	local name = config.name
	local resolution = config.resolution or 100
	local alpha = config.alpha
	local tension = config.tension

	---@type SimpleCurve
	local curve = { xAxisName = xAxisName, yAxisName = yAxisName, name = name, resolution = resolution, loopsBack = false }

	setmetatable(curve, self)

	headerTable = headerTable or default

	if not headerTable or type(headerTable) ~= "table" then
		error("The \"table\" configuration property must be a JBeam table [" .. curve .. "]")
	end

	local workingTable = tableFromHeaderTable(headerTable)
	local controlPoints = {} ---@type LuaVec3[]
	local minX = math.huge
	local minY = math.huge
	local maxX = -math.huge
	local maxY = -math.huge
	local lastX = -math.huge

	for i, v in ipairs(workingTable) do
		if not alpha and type(v.alpha) == "number" then
			alpha = v.alpha
		end
		if not tension and type(v.tension) == "number" then
			tension = v.tension
		end

		local x = v[xAxisName]
		local y = v[yAxisName]

		if not x or not y then
			error("Curve table contained a point without an x or a y value (index " .. tostring(i) .. ") [" .. curve .. "]")
		end

		if x < lastX then
			curve.loopsBack = true
		end

		minX = math.min(minX, x)
		minY = math.min(minY, y)
		maxX = math.max(maxX, x)
		maxY = math.max(maxY, y)
		lastX = x

		table.insert(controlPoints, vec3(x, y))
	end

	if #controlPoints < 2 then
		error("Curve table must contain at least 2 points")
	end

	-- Create a Catmull-Rom spline from the points, which we will use in a moment to precalculate the whole curve
	local spline = CatmullRom:new(controlPoints, alpha, tension)

	local pointCache = {} ---@type LuaVec3[]
	local valueCache = {} ---@type table<number, number>

	for i = 0, resolution do
		local progress = i / resolution
		local point = spline:get(progress) -- when interpolating the spline, "progress" is the time value

		table.insert(pointCache, point)

		if not curve.loopsBack then
			-- Calculate the x-value for "resolution" number of evenly-spaced x-values

			local x = blend(minX, maxX, progress) -- when calculating x-times, "progress" is the X value coefficient
			local segment ---@type Segment

			for _, s in ipairs(spline.segments) do -- find segment along which "X" falls
				if s.start.x <= x then
					segment = s
				end
			end

			if segment then
				local time = maprange(x, segment.start.x, segment.finish.x)

				valueCache[i] = segment:get(time).y
			end
		end
	end

	curve.minX = minX
	curve.minY = minY
	curve.maxX = maxX
	curve.maxY = maxY
	curve.points = pointCache
	curve.values = valueCache

	return curve
end

---Gets the point on the curve at the given `time` value
---@param time number The time value along the curve, in the range [0..1]
---@return LuaVec3
function SimpleCurve:get(time)
	if time == 0 then
		return self.points[1]
	elseif time == 1 then
		return self.points[#self.points]
	else
		local fracIndex = time * self.resolution
		local i1 = floor(fracIndex)
		local i2 = min(self.resolution, i1 + 1)
		local p1 = self.points[i1 + 1]
		local p2 = self.points[i2 + 1]

		return blend(p1, p2, fracIndex - i1)
	end
end

---Gets the Y-value on the curve at the provided X-value
---@param x            number   The x-value at which to retrieve the y-value on the curve
---@param clampValue?  boolean  Optional value indicating whether or not to clamp the returned y-value between the curve's minY and maxY values (defaults to `true`)
---@return number
function SimpleCurve:getValue(x, clampValue)
	if self.loopsBack then
		error("getValue is not supported for curves that loop back on themselves. Each X value must be greater than the last. [" .. self .. "]")
	end

	clampValue = clampValue ~= false

	local xProg = maprange(x, self.minX, self.maxX, 0, self.resolution) -- where X lies, between 0 and "resolution"
	local step0 = floor(xProg)
	local step1 = ceil(xProg)
	local val0 = self.values[step0]
	local val1 = self.values[step1]

	if clampValue then
		return clamp(blend(val0, val1, xProg - step0), self.minY, self.maxY)
	else
		return blend(val0, val1, xProg - step0)
	end
end

function SimpleCurve:__index(k)
	if type(k) == "number" then
		return self:get(k)
	end

	return rawget(SimpleCurve, k)
end

function SimpleCurve:__tostring()
	return self.name or ("{ xAxisName = \"" .. self.xAxisName .. "\", yAxisName = \"" .. self.yAxisName .. "\" }")
end

---@param lhs string | SimpleCurve
---@param rhs string | SimpleCurve
---@return string
function SimpleCurve.__concat(lhs, rhs)
	if type(lhs) == "string" then
		return lhs .. tostring(rhs)
	elseif type(rhs) == "string" then
		return tostring(lhs) .. rhs
	end
	return nil
end

-- WARNING: Super hacky debug method that temporarily hijacks controller.updateGFX. Use at own risk.
function SimpleCurve:debug(period, mode)
	if self.debugging then
		log("W", "simplecurve.debug", "Debugging is already happening!")
		return
	end

	mode = mode or "time"

	self.debugging = { time = 0, period = period or 10, util = require("arcanox/DebugModule"):new("simplecurve_" .. self.xAxisName .. "_" .. self.yAxisName, true) }

	local curve = self
	local debug = self.debugging
	local prevUpdateGFX = controller.updateGFX

	controller.updateGFX = function(dt)
		prevUpdateGFX(dt)

		local t = debug.time / debug.period
		local y

		if mode == "x" then
			local x = blend(curve.minX, curve.maxX, t)

			y = curve:getValue(x)
		else
			local p = curve:get(t)

			y = p.y
		end

		debug.util:putGraphValue{ "Curve Value", y }
		debug.time = debug.time + dt

		if t >= 1 then
			debug.util:dispose()
			curve.debugging = nil
			controller.updateGFX = prevUpdateGFX
			log("I", "simplecurve.debug", "Curve is done debugging.")
		end
	end

	log("I", "simplecurve.debug", "Curve is debugging! Will be done in " .. debug.period .. "s.")
end

return SimpleCurve
