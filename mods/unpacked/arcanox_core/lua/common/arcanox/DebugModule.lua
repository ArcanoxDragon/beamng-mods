---Provides utilities for debugging vehicles and controllers through the Arcanox Debug UI app
---@class DebugModule
---@field logTag string
---@field enabled boolean Whether or not this `DebugUtility` is enabled
---@field private debugValues table
local DebugModule = {}
DebugModule.__index = DebugModule

local M = DebugModule -- alias

-- Requires
local mathutil = require("arcanox/mathutil")

-- #region Static Methods

function M.formatNumber(number, places)
	if type(number) ~= "number" then
		return "nil"
	end
	if type(places) ~= "number" then
		places = 2
	end

	return tostring(mathutil.round(number, places))
end

local debugValueMetatable = {
	__tostring = function(t)
		if #t == 0 then
			return "[]"
		end

		local s = "["

		for i, v in ipairs(t) do
			if i > 1 then
				s = s .. ", "
			end

			s = s .. string.format("%s %s", type(v), tostring(v))
		end

		s = s .. "]"

		return s
	end,
	__eq = function(a, b)
		if #a ~= #b then
			return false
		end

		for i = 1, #a do
			if a[i] ~= b[i] then
				return false
			end
		end

		return true
	end,
}

function M.formatValue(value, places)
	if type(value) == "number" then
		return mathutil.round(value, places)
	elseif type(value) == "table" and #value > 0 then
		setmetatable(value, debugValueMetatable)

		for i, v in ipairs(value) do
			value[i] = M.formatValue(v, places)
		end
	end

	return value
end

-- #endregion

---@return DebugModule
---@param logTag string A tag describing the debug module when it writes values to the log or profiling graph
---@param enable boolean Whether or not the debug module should send updates to the UI
function DebugModule:new(logTag, enable)
	---@type DebugModule
	local obj = {
		logTag = logTag,
		enabled = enable or false,
		debugValues = {},
	}

	setmetatable(obj, M)

	-- initialize the collections
	obj:clearValues()

	-- register with the extension
	extensions.arcanox_debug.registerModule(obj)

	return obj
end

---@overload fun(self: DebugModule, text: string)
---@overload fun(level: string, text: string)
---@overload fun(text: string)
function DebugModule.log(self, level, text)
	if type(self) ~= "table" then
		-- shift arguments >> 1; called as static function
		self, level, text = nil, self, level
	end

	if self and not self.enabled then
		return
	end

	if not text then
		-- shift arguments >> 1; no level provided
		level, text = "D", level
	end

	log(level, "arcanox.debug." .. self.logTag, text)
end

function DebugModule:putValue(title, value, places)
	if not self.enabled then
		return
	end

	if title == "disabled" then
		M.log("E", "Invalid key: " .. title)
		return
	end

	places = places or 2

	local id = self.debugIds[title]

	if not id then
		self.numDebugValues = self.numDebugValues + 1
		id = self.numDebugValues
		self.debugIds[title] = id
	end

	value = M.formatValue(value, places)

	if not value then
		self.debugValues[id] = { id = id, destroy = true }
		return
	end

	local entry = self.debugValues[id]

	if entry and not entry.destroy then
		if value == entry.value then
			return
		end

		entry.value = value
		entry.dirty = true
	else
		self.debugValues[id] = { id = id, title = title, value = value, dirty = true }
	end
end

function DebugModule:traceValue(title, value, places)
	local caller = debug.getinfo(2, "nl")
	local source = string.format("%s:%d", caller.name, caller.currentline)

	self:putValue(title .. " " .. source, value, places)
end

---@class GraphValueArgsNamed
---@field title? string The title of the graph value shown in the UI
---@field value? number The numeric value of this graph value at the current moment in time
---@field unit? string A unit string displayed in the graph legend UI next to the title
---@field color? string A color for this value's graph line. A unique color will be generated if this is omitted.

---@class GraphValueArgsPositional
---@field [1]? string The title of the graph value shown in the UI
---@field [2]? number The numeric value of this graph value at the current moment in time
---@field [3]? string A unit string displayed in the graph legend UI next to the title
---@field [4]? string A color for this value's graph line. A unique color will be generated if this is omitted.

---@param args GraphValueArgsNamed | GraphValueArgsPositional
function DebugModule:putGraphValue(args)
	if not self.enabled then
		return
	end

	-- parse args
	local title = args.title or args[1]
	local value = args.value or args[2]
	local unit = args.unit or args[3]
	local color = args.color or args[4]

	local id = self.graphIds[title]

	if not id then
		self.numGraphValues = self.numGraphValues + 1
		id = self.numGraphValues
		self.graphIds[title] = id
	end

	self.graphValues[id] = { title = title, value = value, unit = unit, color = color }
end

function DebugModule:clearValues()
	if self.debugIds then
		for _, id in pairs(self.debugIds) do
			self.debugValues[id] = { id = id, destroy = true }
		end
	end

	self.debugIds = {}
	self.numDebugValues = 0

	self.graphIds = {}
	self.graphValues = {}
	self.numGraphValues = 0
end

function DebugModule:getUpdateStream(all)
	if self.enabled then
		local stream = {}

		for key, entry in pairs(self.debugValues) do
			if entry then
				if entry.destroy then
					-- Tell the app the value was removed, and then remove it from the debug module too

					stream[key] = { id = entry.id, destroy = true }
					self.debugValues[key] = nil
				elseif all or entry.dirty then
					-- Tell the app the value changed, and then mark it clean in the debug module

					stream[key] = shallowcopy(entry)
					entry.dirty = false
				end
			end
		end

		return stream
	else
		return { disabled = true }
	end
end

function DebugModule:dispose()
	extensions.arcanox_debug.unregisterModule(self)
end

return M
