local M = {}

local mathutil = require("arcanox/mathutil")

-- Planetary gearset class
local planetaryGearset = {}
planetaryGearset.__index = planetaryGearset

local function newPlanetaryGearset(config)
	local gearset = {
		sunGearTeeth = config.sunGearTeeth,
		planetGearTeeth = config.planetGearTeeth,
		ringGearTeeth = config.ringGearTeeth,
		planetGearCount = config.planetGearCount or 4,

		ringCarrierRatio = config.ringGearTeeth / (config.ringGearTeeth + config.sunGearTeeth),
		sunCarrierRatio = config.sunGearTeeth / (config.ringGearTeeth + config.sunGearTeeth),
		sunRingRatio = config.sunGearTeeth / config.ringGearTeeth,

		additionalSunGearInertia = config.additionalSunGearInertia or 0,
		additionalRingGearInertia = config.additionalRingGearInertia or 0,
		additionalCarrierInertia = config.additionalCarrierInertia or 0,

		sunGearAV = 0,
		ringGearAV = 0,
		carrierAV = 0,
	}

	setmetatable(gearset, planetaryGearset)

	-- Calculate gear radii/masses
	local sunGearRadius = config.sunGearTeeth * config.gearRadiusCoef
	local sunGearMass = sunGearRadius * sunGearRadius * config.gearMassCoef
	local planetGearRadius = config.planetGearTeeth * config.gearRadiusCoef
	local planetGearMass = planetGearRadius * planetGearRadius * config.gearMassCoef
	local ringGearInnerRadius = config.ringGearTeeth * config.gearRadiusCoef
	local ringGearOuterRadius = ringGearInnerRadius * 1.2
	local ringGearMass = (ringGearOuterRadius * ringGearOuterRadius * config.gearMassCoef) - (ringGearInnerRadius * ringGearInnerRadius * config.gearMassCoef) -- ring gear is hollow
	local carrierRadius = sunGearRadius + planetGearRadius
	local carrierMass = config.additionalCarrierMass or planetGearMass -- assume one planet gear's worth of mass on the carrier itself by default

	gearset.sunGearRadius = sunGearRadius
	gearset.sunGearMass = sunGearMass
	gearset.planetGearRadius = planetGearRadius
	gearset.planetGearMass = planetGearMass
	gearset.ringGearOuterRadius = ringGearOuterRadius
	gearset.ringGearInnerRadius = ringGearInnerRadius
	gearset.ringGearMass = ringGearMass
	gearset.carrierRadius = carrierRadius
	gearset.carrierMass = carrierMass

	return gearset
end

-- AV EQUATIONS

function planetaryGearset:getRingAV(sunAV, carrierAV)
	sunAV = sunAV or self.sunGearAV
	carrierAV = carrierAV or self.carrierAV

	return ((self.sunGearTeeth + self.ringGearTeeth) * carrierAV - self.sunGearTeeth * sunAV) / self.ringGearTeeth
end

function planetaryGearset:getCarrierAV(sunAV, ringAV)
	sunAV = sunAV or self.sunGearAV
	ringAV = ringAV or self.ringGearAV

	return (self.sunGearTeeth * sunAV + self.ringGearTeeth * ringAV) / (self.sunGearTeeth + self.ringGearTeeth)
end

function planetaryGearset:getSunAV(ringAV, carrierAV)
	ringAV = ringAV or self.ringGearAV
	carrierAV = carrierAV or self.carrierAV

	return ((self.sunGearTeeth + self.ringGearTeeth) * carrierAV - self.ringGearTeeth * ringAV) / self.sunGearTeeth
end

-- TORQUE EQUATIONS

function planetaryGearset:getRingTorqueFromSunTorque(sunTorque)
	return sunTorque / self.sunRingRatio
end

function planetaryGearset:getRingTorqueFromCarrierTorque(carrierTorque)
	return -carrierTorque * self.ringCarrierRatio
end

function planetaryGearset:getCarrierTorqueFromSunTorque(sunTorque)
	return -sunTorque / self.sunCarrierRatio
end

function planetaryGearset:getCarrierTorqueFromRingTorque(ringTorque)
	return -ringTorque / self.ringCarrierRatio
end

function planetaryGearset:getSunTorqueFromRingTorque(ringTorque)
	return ringTorque * self.sunRingRatio
end

function planetaryGearset:getSunTorqueFromCarrierTorque(carrierTorque)
	return -carrierTorque * self.sunCarrierRatio
end

-- INERTIA EQUATIONS

function planetaryGearset:calculateInertia()
	-- Calculate inertias
	self.sunGearInertia = mathutil.getSolidCylinderInertia(self.sunGearMass, self.sunGearRadius) + self.additionalSunGearInertia
	self.ringGearInertia = mathutil.getHollowCylinderInertia(self.ringGearMass, self.ringGearInnerRadius, self.ringGearOuterRadius) + self.additionalRingGearInertia

	local planetsOrbitalInertia = self.planetGearCount * (self.planetGearMass * self.carrierRadius * self.carrierRadius)
	local soloCarrierInertia = mathutil.getSolidCylinderInertia(self.carrierMass, self.carrierRadius * 0.95, self.carrierRadius * 1.05)

	self.carrierInertia = planetsOrbitalInertia + soloCarrierInertia + self.additionalCarrierInertia
end

function planetaryGearset:getTotalCarrierInertia()
	local fromSun = self.sunGearInertia / self.sunCarrierRatio / self.sunCarrierRatio
	local fromRing = self.ringGearInertia / self.ringCarrierRatio / self.ringCarrierRatio

	return self.carrierInertia + fromSun + fromRing
end

function planetaryGearset:getTotalSunGearInertia()
	local fromCarrier = self.carrierInertia * self.sunCarrierRatio * self.sunCarrierRatio
	local fromRing = self.ringGearInertia * self.sunRingRatio * self.sunRingRatio

	return self.sunGearInertia + fromCarrier + fromRing
end

function planetaryGearset:getTotalRingGearInertia()
	local fromSun = self.sunGearInertia / self.sunRingRatio / self.sunRingRatio
	local fromCarrier = self.carrierInertia * self.ringCarrierRatio * self.ringCarrierRatio

	return self.ringGearInertia + fromSun + fromCarrier
end

-- UPDATE FUNCTIONS
function planetaryGearset:updateAVs(sunGearAV, carrierAV, ringGearAV)
	if sunGearAV and carrierAV and ringGearAV then
		error("Only two of three values may be provided to updateAV")
	end

	if not sunGearAV then
		self.carrierAV = carrierAV
		self.ringGearAV = ringGearAV
		self.sunGearAV = ((self.sunGearTeeth + self.ringGearTeeth) * carrierAV - self.ringGearTeeth * ringGearAV) / self.sunGearTeeth
	elseif not carrierAV then
		self.ringGearAV = ringGearAV
		self.sunGearAV = sunGearAV
		self.carrierAV = (self.sunGearTeeth * sunGearAV + self.ringGearTeeth * ringGearAV) / (self.sunGearTeeth + self.ringGearTeeth)
	else
		self.sunGearAV = sunGearAV
		self.carrierAV = carrierAV
		self.ringGearAV = ((self.sunGearTeeth + self.ringGearTeeth) * carrierAV - self.sunGearTeeth * sunGearAV) / self.ringGearTeeth
	end
end

M.newPlanetaryGearset = newPlanetaryGearset

return M