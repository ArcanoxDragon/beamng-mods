local M = {}

---Parses arguments from a function, allowing them to be provided either as
---positional arguments or a table of options
---@param args table An arguments table from a function
local function parseArgs(args, ...)
    if type(args) ~= "table" then
        error("Invalid arguments table provided")
    end

    local argKeys = {...}
    local argKeyLookup = tableValuesAsLookupDict(argKeys)

    if #argKeys == 0 then
        error("At least one argument key must be provided")
    end

    for i, v in ipairs(argKeys) do
        if type(v) ~= "string" then
            error(("Argument key at position %d was not a string"):format(i))
        end
    end

    local ret = {}

    if #args == 1 and type(args[1]) == "table" then
        -- Presumably parsing an options table

        local hadArgKey = false

        for key, value in pairs(args[1]) do
            if argKeyLookup[key] then
                hadArgKey = true
                ret[key] = value
            end
        end

        if not hadArgKey then
            -- Likely a single positional table parameter passed, not an options table.
            -- Just return the table in the first parameter name instead.

            ret[argKeys[1]] = args[1]
        end
    elseif #args >= 1 then
        -- Parsing positional arguments

        for i, key in ipairs(argKeys) do
            if i > #args then -- no need to keep iterating
                break
            end

            ret[key] = args[i]
        end
    end

    return ret
end

M.parseArgs = parseArgs

return M