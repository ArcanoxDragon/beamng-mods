local M = {}

--- Represents a state machine with a strictly-defined set of states,
--- each with its own unique logic.
---@class StateMachine
---@field current string
---@field previous? string
---@field states table<string, StateMachineState>
---@field currentState? StateMachineState
local StateMachine = M
StateMachine.__index = StateMachine

local validStateFunctions = { init = true, canEnter = true, enter = true, leave = true, update = true, updateGFX = true }

---@class StateMachineState
---@field machine StateMachine
---@field init? fun(self: StateMachineState)
---@field canEnter? fun(self: StateMachineState): boolean
---@field enter? fun(self: StateMachineState, previous?: string)
---@field leave? fun(self: StateMachineState, next: string)
---@field update? fun(self: StateMachineState, dt: number)
---@field updateGFX? fun(self: StateMachineState, dt: number)
---@field _isValidState boolean
local StateMachineState = {
    __newindex = function(t, k, v)
        if k == "machine" or k == "_isValidState" or validStateFunctions[k] then
            -- don't allow setting these
            return
        end

        rawset(t, k, v)
    end
}
StateMachineState.__index = StateMachineState

-- Special indexer function, so that if a state doesn't define a function, it will
-- simply default to a nop function (unless StateMachineState itself explicilty defines a default)
setmetatable(StateMachineState, {
	__index = function(t, k)
		if validStateFunctions[k] then
			-- default to a nop function; store it on the table for faster access next time
			t[k] = function()
			end
			return t[k]
		end
	end,
})

function StateMachineState:canEnter()
	return true
end

---@param states table<string, StateMachineState>
local function validateStatesTable(states)
	if type(states) ~= "table" then
		error(("Invalid parameter \"states\" to StateMachine:new. Expected table, got %s"):format(type(states)))
	end

	if type(states["default"]) ~= "table" then
		log("W", "StateMachine.new", "States table did not contain a state named \"default\". Using dummy state.")
		states.default = {}
	end

	for stateName, state in pairs(states) do
		if type(state) ~= "table" then
			error(("State %q was not a table"):format(stateName))
		end

		for funcName, func in pairs(state) do
			if not validStateFunctions[funcName] then
				error(("State %q had invalid function %q"):format(stateName, funcName))
			end
			if type(func) ~= "function" then
				error(("Field %q on state %q was not a function"):format(funcName, stateName))
			end
		end
	end
end

---@param states table<string, StateMachineState>
---@return StateMachine
function StateMachine:new(states)
	validateStatesTable(states)

	---@type StateMachine
	local obj = { states = states }
	setmetatable(obj, self)

	-- Set up state metatable (we give each state some common readonly fields, and then the StateMachineState class prototype behind that)
	local stateMt = {
        __index = setmetatable({
            machine = obj,
            _isValidState = true,
        }, StateMachineState)
    }

	-- Initialize states
	for _, state in pairs(obj.states) do
		setmetatable(state, stateMt)
        state:init()
	end

	-- Begin default state
	obj:changeTo("default")

	return obj
end

--- Transitions the state machine to a new state
---@param state string The name of the next state for the state machine
---@return boolean # Whether or not the state change was successful
function StateMachine:changeTo(state)
	if self.current == state then
		-- No sense switching states if we're already in the desired state
		return false
	end

	local nextState = self.states[state]

	if type(nextState) ~= "table" or not nextState._isValidState then
		log("E", "StateMachine.changeTo", ("Unknown state %q requested. Aborting state change."):format(state))
		return false
	end

	if not nextState:canEnter() then
		return false
	end

	if self.currentState then
		self.currentState:leave(state)
	end

	self.previous = self.current
	self.current = state
	self.currentState = nextState

	nextState:enter(self.previous)

	return true
end

--- Performs a physics update on the StateMachine's current state
---@param dt number
function StateMachine:update(dt)
	if self.currentState then
		self.currentState:update(dt)
	end
end

--- Performs a graphics update on the StateMachine's current state
---@param dt number
function StateMachine:updateGFX(dt)
	if self.currentState then
		self.currentState:updateGFX(dt)
	end
end

return M
