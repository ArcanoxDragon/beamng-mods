local M = {}
M.version = "5.2.0"

local function onExtensionLoaded()
	log("D", "arcanoxCore.onExtensionLoaded", ("Arcanox Core version %s loaded"):format(M.version))
end

M.onExtensionLoaded = onExtensionLoaded

return M
