--#region LuaVec3

---@class LuaVec3
---@field x number
---@field y number
---@field z? number
---@field distance function
local LuaVec3 = {}

---@overload fun(x: number, y: number, z?: number): LuaVec3
---@overload fun(values: { x: number, y: number, z?: number }): LuaVec3
---@overload fun(values: number[]): LuaVec3
function vec3(...)
end

---Gets the squared-distance between this vec3 and another one
---@param other LuaVec3
---@return number
function LuaVec3:squaredDistance(other)
end

---Gets the x-normal (i.e. time value) along the line between vectors `a` and `b`
---@param a LuaVec3
---@param b LuaVec3
---@return number
function LuaVec3:xnormOnLine(a, b)
end

--#endregion