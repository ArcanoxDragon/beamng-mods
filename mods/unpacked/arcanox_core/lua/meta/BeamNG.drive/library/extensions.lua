---@class Extensions
extensions = {}

---@type ArcCamHelperExtension
extensions.arcanox_camhelper = {}

---@type ArcDebugExtension
extensions.arcanox_debug = {}

---@type ArcQuickTuningExtension
extensions.arcanox_quickTuning = {}

---@type ArcUtilExtension
extensions.arcanox_util = {}