---@alias Curve table<number, number>

---@class PowertrainDevice
---@field children? PowertrainDevice[]
---@field cumulativeGearRatio number
---@field cumulativeInertia number
---@field deviceCategories? table<string, boolean>
---@field maxCumulativeGearRatio number
---@field name? string
---@field outputAV1? number
---@field outputAV2? number
---@field outputAV3? number
---@field outputAV4? number
---@field outputTorque1? number
---@field outputTorque2? number
---@field outputTorque3? number
---@field outputTorque4? number
---@field parent? PowertrainDevice
---@field throttleFactor? number
---@field type string

---@class ClutchDevice : PowertrainDevice
---@field additionalEngineInertia? number

---@class EngineDevice : PowertrainDevice
---@field combustionTorque number
---@field engineLoad number
---@field engineNodeID number
---@field hasFuel boolean
---@field idleAVStartOffsetSmoother table
---@field idleRPM number
---@field inertia number
---@field isBroken boolean
---@field isStalled boolean
---@field maxAV number
---@field starterEngagedCoef number
---@field throttleSmoother table
---@field torqueData? EngineTorqueData
local EngineDevice = {}

---@class EngineTorqueData
---@field curves table<string, Curve>
---@field deviceName string
---@field finalCurveName string
---@field maxPower number
---@field maxPowerRPM number
---@field maxRPM number
---@field maxTorque number
---@field maxTorqueRPM number
---@field vehicleID number

---@return EngineTorqueData
function EngineDevice:getTorqueData()
end

function EngineDevice:activateStarter()
end

function EngineDevice:deactivateStarter()
end

function EngineDevice:disable()
end