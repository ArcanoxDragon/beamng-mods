local M = {}

local max = math.max

local flagActionMaps = {}
local activeVehicleFlags = {}
local activeFlagCounts = {}

local function enableNecessaryActionMaps()
	for flag, actionMapName in pairs(flagActionMaps) do
		local count = activeFlagCounts[flag]

		if count and count > 0 then
			-- log("D", "arcanoxInput.enableNecessaryActionMaps", ("Enabling action map %q"):format(actionMapName))
			pushActionMap(actionMapName)
		else
			-- log("D", "arcanoxInput.enableNecessaryActionMaps", ("Disabling action map %q"):format(actionMapName))
			popActionMap(actionMapName)
		end
	end
end

local function vehicleWentInactive(vehId)
	local vehFlags = activeVehicleFlags[vehId]

	if not vehFlags then
		return
	end

	for flag in pairs(vehFlags) do
		activeFlagCounts[flag] = max(0, (activeFlagCounts[flag] or 1) - 1)
	end

	activeVehicleFlags[vehId] = nil
	enableNecessaryActionMaps()
end

local function vehicleBecameActive(vehId, vehFlags)
	if activeVehicleFlags[vehId] then -- duplicate activation; deactivate it first
		vehicleWentInactive(vehId)
	end

	for flag in pairs(vehFlags) do
		activeFlagCounts[flag] = (activeFlagCounts[flag] or 0) + 1
	end

	activeVehicleFlags[vehId] = vehFlags
	enableNecessaryActionMaps()
end

local function registerActionMap(flag, actionMapName)
	if type(flag) ~= "string" or type(actionMapName) ~= "string" then
		log("E", "arcanoxInput.registerActionMap", "Invalid parameters passed to function")
		return
	end

	if flagActionMaps[flag] then -- don't overwrite
		return
	end

	flagActionMaps[flag] = actionMapName
	enableNecessaryActionMaps()
end

local function onInputBindingsChanged()
	enableNecessaryActionMaps()
end

M.vehicleWentInactive = vehicleWentInactive
M.vehicleBecameActive = vehicleBecameActive
M.registerActionMap = registerActionMap
M.onInputBindingsChanged = onInputBindingsChanged

return M
