class TemporalSmoothing {
	constructor(inRate, outRate, startingValue) {
		this.inRate = inRate || 1;
		this.outRate = outRate || this.inRate;
		this.state = startingValue || 0;
	}

	get(sample, dt) {
		let diff = sample - this.state;
		let rate = diff * this.state >= 0 ? this.outRate : this.inRate;

		this.state = this.state + diff * Math.min(rate * dt, 1);
		return this.state;
	}
}

function clamp(val, min, max) {
	if (max < min) {
		[min, max] = [max, min];
	}

	return Math.min(max || 1.0, Math.max(min || 0.0, val));
}

function maprange(val, srcMin, srcMax, dstMin, dstMax, clampOutput) {
	if (clampOutput !== false) clampOutput = true;
	if (typeof dstMin !== "number") dstMin = 0.0;
	if (typeof dstMax !== "number") dstMax = 1.0;

	let normalized = (val - srcMin) / (srcMax - srcMin); // 0.0 to 1.0 when inside[ srcMin, srcMax ]

	let dstRange = dstMax - dstMin;
	let output = dstRange * normalized + dstMin;

	if (clampOutput) {
		let cMin = Math.min(dstMin, dstMax);
		let cMax = Math.max(dstMin, dstMax);

		output = clamp(output, cMin, cMax);
	}

	return output;
}

function blend(val1, val2, amount) {
	return clamp(amount * (val2 - val1) + val1, val1, val2);
}

function parseColor(str) {
	let match = str.match(/^#([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})/i);

	if (!match) {
		return null;
	}

	let [, rS, gS, bS] = match;
	let r = parseInt(rS, 16);
	let g = parseInt(gS, 16);
	let b = parseInt(bS, 16);

	if (isNaN(r) || isNaN(g) || isNaN(b)) {
		return null;
	}

	return [r, g, b];
}

function colorToString(rgb) {
	if (!Array.isArray(rgb) || rgb.length < 3 || rgb.some(isNaN)) {
		return null;
	}

	let [r, g, b] = rgb;
	let rS = r.toString(16).padLeft(2, "0");
	let gS = g.toString(16).padLeft(2, "0");
	let bS = b.toString(16).padLeft(2, "0");

	return `#${rS}${gS}${bS}`;
}

function blendColors(col1, col2, amount) {
	let rgb1 = parseColor(col1);
	let rgb2 = parseColor(col2);

	if (!rgb1 || !rgb2) {
		return col1;
	}

	let [r1, g1, b1] = rgb1;
	let [r2, g2, b2] = rgb2;
	let r = blend(r1, r2, amount);
	let g = blend(g1, g2, amount);
	let b = blend(b1, b2, amount);

	return colorToString([r, g, b]);
}

angular
	.module("beamng.apps")
	.directive("arcanoxHybrid", function () {
		return {
			templateUrl: "/ui/modules/apps/ArcanoxHybrid/arc-hybrid.html",
			replace: true,
			restrict: "EA",
			link: function (scope, element, attrs) {
				StreamsManager.add(["hybrid"]);

				scope.$on("$destroy", function () {
					StreamsManager.remove(["hybrid"]);
				});

				let appElement = element[0];
				let svgElement = appElement.querySelector(".hybrid-app-svg");

				svgElement.data += "?t=" + new Date().valueOf(); // bust the cache
				svgElement.addEventListener("load", function () {
					let svg = svgElement.contentDocument;
					let batteryLevelGauge = {
						valOver: svg.getElementById("val-bat-over"),
						valUnder: svg.getElementById("val-bat-under"),
						clip: {
							rect: svg.getElementById("clip-bat-rect"),
							height: +svg.getElementById("clip-bat-rect").getAttribute("height"),
							y: +svg.getElementById("clip-bat-rect").getAttribute("y"),
						},
					};
					let batteryPowerIndicator = {
						group: svg.getElementById("bat-power-delta"),
						glyph: svg.getElementById("bat-power-delta-glyph"),
						drainColor: "#ffc82e",
						chargeColor: "#00ebff",
					};
					let powerDrawGauge = {
						valOver: svg.getElementById("val-kw-over"),
						valUnder: svg.getElementById("val-kw-under"),
						fillPos: svg.getElementById("fill-kw-pos"),
						clip: {
							rect: svg.getElementById("clip-kw-rect"),
							height: +svg.getElementById("clip-kw-rect").getAttribute("height"),
							y: +svg.getElementById("clip-kw-rect").getAttribute("y"),
						},
						ecoColor: "#00ff00",
						normalColor: "#ffc82e",
					};
					let ecoBar = {
						element: svg.getElementById("eco-bar"),
						origPos: {
							y: +svg.getElementById("eco-bar").getAttribute("y"),
						},
					};
					let readyIcon = {
						element: svg.getElementById("icon-ready"),
						onColor: "#00ebff",
						offColor: "#aaaaaa88",
					};
					let evIcon = {
						element: svg.getElementById("icon-ev-mode"),
						onColor: "#00ff00",
						offColor: "#aaaaaa88",
					};
					let modeText = svg.getElementById("text-drive-mode");

					let updating = false;
					let updateInterval = null;
					let socSmoothing = new TemporalSmoothing(2.0);
					let propulsionPowerSmoothing = new TemporalSmoothing(5.0);
					let batteryPowerDrawSmoothing = new TemporalSmoothing(5.0);
					let batteryPowerDrawScaleSmoothing = new TemporalSmoothing(25.0);
					let ecoBarPowerSmoothing = new TemporalSmoothing(5.0);
					let ecoBarOpacitySmoothing = new TemporalSmoothing(5.0);
					let time = new Date().valueOf();
					let dt = 0;
					let stateOfCharge = 0;
					let minStateOfCharge = 0;
					let maxStateOfCharge = 1;
					let propulsionPower = 0;
					let batteryPower = 0;
					let ecoThreshold = 0;
					let lastNonZeroEcoThreshold = 0;
					let maxPowerDraw = 1;
					let maxRegen = 1;
					let ready = false;
					let ev = false;
					let mode = "Off";

					function updateScreen() {
						let newTime = new Date().valueOf();

						dt = (newTime - time) / 1000.0;
						time = newTime;

						// Update battery level meter
						let normalizedStateOfCharge = socSmoothing.get(maprange(stateOfCharge, minStateOfCharge, maxStateOfCharge), dt);
						let batteryLevelHeight = normalizedStateOfCharge * batteryLevelGauge.clip.height;

						batteryLevelGauge.valOver.innerHTML = `${Math.round(stateOfCharge * 100)}%`;
						batteryLevelGauge.valUnder.innerHTML = batteryLevelGauge.valOver.innerHTML;
						batteryLevelGauge.clip.rect.setAttribute("height", batteryLevelHeight);
						batteryLevelGauge.clip.rect.setAttribute("y", batteryLevelGauge.clip.y + (batteryLevelGauge.clip.height - batteryLevelHeight));

						// Update battery power draw indicator
						if (maxPowerDraw === 0 || maxRegen === 0) {
							batteryPowerIndicator.glyph.opacity = 0;
						} else {
							let batteryBarHalfHeight = batteryLevelGauge.clip.height / 2.0;
							let smoothedBatteryPower = batteryPowerDrawSmoothing.get(batteryPower, dt);
							let batteryPowerCoef = smoothedBatteryPower / (smoothedBatteryPower >= 0 ? maxPowerDraw : maxRegen);
							let batteryPowerPos = clamp(batteryPowerCoef, -1, 1) * batteryBarHalfHeight * 0.98;
							let batteryPowerVisibility = clamp((Math.abs(batteryPowerCoef) - 0.05) / 0.05);
							let batteryPowerScale = batteryPowerDrawScaleSmoothing.get(Math.sign(batteryPowerCoef), dt);

							batteryPowerIndicator.glyph.style.opacity = batteryPowerVisibility;
							batteryPowerIndicator.glyph.style.fill = batteryPower >= 0 ? batteryPowerIndicator.drainColor : batteryPowerIndicator.chargeColor;
							batteryPowerIndicator.glyph.setAttribute("transform", `translate(0, ${(batteryPowerPos).toFixed(3)}) scale(1, ${batteryPowerScale.toFixed(3)})`);
						}

						// Update power meter
						let smoothedPropulsionPower = propulsionPowerSmoothing.get(propulsionPower, dt);
						let isEco = ev && ecoThreshold > 0 && smoothedPropulsionPower <= ecoThreshold;

						powerDrawGauge.valOver.innerHTML = `${Math.round(smoothedPropulsionPower)} kW`;
						powerDrawGauge.valUnder.innerHTML = powerDrawGauge.valOver.innerHTML;
						powerDrawGauge.fillPos.style.fill = isEco ? powerDrawGauge.ecoColor : powerDrawGauge.normalColor;

						if (maxPowerDraw === 0 || maxRegen === 0) {
							powerDrawGauge.clip.rect.setAttribute("y", powerDrawGauge.clip.y);
							powerDrawGauge.clip.rect.setAttribute("height", 0);
						} else {
							let powerBarHalfHeight = powerDrawGauge.clip.height / 2.0;

							if (smoothedPropulsionPower >= 0) {
								let powerDrawPosNormalized = isNaN(smoothedPropulsionPower) ? 0 : clamp(smoothedPropulsionPower / maxPowerDraw, 0, 1);
								let powerDrawHeight = Math.abs(powerDrawPosNormalized) * powerBarHalfHeight;

								powerDrawGauge.clip.rect.setAttribute("y", powerDrawGauge.clip.y + (powerBarHalfHeight - powerDrawHeight));
								powerDrawGauge.clip.rect.setAttribute("height", powerDrawHeight);
							} else {
								let powerDrawNegNormalized = isNaN(smoothedPropulsionPower) ? 0 : clamp(smoothedPropulsionPower / maxRegen, -1, 0);
								let powerDrawHeight = Math.abs(powerDrawNegNormalized) * powerBarHalfHeight;

								powerDrawGauge.clip.rect.setAttribute("y", powerDrawGauge.clip.y + powerBarHalfHeight);
								powerDrawGauge.clip.rect.setAttribute("height", powerDrawHeight);
							}

							// Update eco bar
							let ecoBarOpacity = ecoBarOpacitySmoothing.get((ready && ev && ecoThreshold > 0) ? 1 : 0, dt);
							let ecoBarPower = ecoBarPowerSmoothing.get(lastNonZeroEcoThreshold, dt);
							let ecoBarPosNormalized = ecoBarPower / maxPowerDraw;

							ecoBar.element.style.opacity = ecoBarOpacity;
							ecoBar.element.setAttribute("y", ecoBar.origPos.y - ecoBarPosNormalized * powerBarHalfHeight);
						}

						// Update info section
						readyIcon.element.style.fill = ready ? readyIcon.onColor : readyIcon.offColor;
						evIcon.element.style.fill = ev ? evIcon.onColor : evIcon.offColor;
						modeText.innerHTML = mode;
					}

					scope.$on("streamsUpdate", function (event, streams) {
						const hybrid = streams ? streams.hybrid : null;

						if (hybrid) {
							appElement.style.opacity = 1.0;
							stateOfCharge = isNaN(hybrid.soc) ? 0.0 : hybrid.soc;
							minStateOfCharge = isNaN(hybrid.minSoc) ? 0.0 : hybrid.minSoc;
							maxStateOfCharge = isNaN(hybrid.maxSoc) ? 1.0 : hybrid.maxSoc;
							propulsionPower = isNaN(hybrid.kw) ? 0.0 : hybrid.kw;
							batteryPower = isNaN(hybrid.batteryKw) ? 0.0 : hybrid.batteryKw;
							ecoThreshold = isNaN(hybrid.ecoKw) ? 0.0 : hybrid.ecoKw;
							maxPowerDraw = isNaN(hybrid.maxKw) ? 0.0 : hybrid.maxKw;
							maxRegen = isNaN(hybrid.regenKw) ? maxPowerDraw : hybrid.regenKw;
							ready = !!hybrid.powerOn;
							ev = !!hybrid.ev;
							mode = hybrid.mode;
							lastNonZeroEcoThreshold = ready ? (ecoThreshold > 0.0 ? ecoThreshold : lastNonZeroEcoThreshold) : 0;

							if (!updating) {
								updating = true;
								updateInterval = setInterval(updateScreen, 1000 / 60); // 60 fps
							}
						} else {
							appElement.style.opacity = 0.0;

							if (updating) {
								updating = false;
								clearInterval(updateInterval);
							}
						}
					});
				});
			}
		};
	});
