angular
	.module("beamng.apps")
	.directive("arcQuickTuning", [function () {
		function Parameter(descriptor) {
			Object.assign(this, descriptor);
		}
		
		Parameter.prototype = Object.create(Object.prototype, {
			// Clamped parameter value since we don't know if the Lua code will give it to us in-bounds
			clampedValue: {
				get() {
					return Math.max(Math.min(this.value, this.max), this.min);
				},
				set(value) {
					this.value = Math.max(Math.min(value, this.max), this.min);
				},
			}
		})
		
		return {
			templateUrl: "/ui/modules/apps/ArcanoxQuickTuning/arc-quick-tuning.html",
			replace: true,
			restrict: "E",
			scope: true,
			controllerAs: "app",
			controller: ["RateLimiter", "$scope", function (RateLimiter, $scope) {
				let vm = this;

				$scope.tuningModules = null;

				vm.reset = function () {
					$scope.$evalAsync(function () {
						$scope.tuningModules = null;
						bngApi.activeObjectLua(`extensions.arcanox_quickTuning.requestState()`);
					});
				};

				$scope.resetValues = function (e, module) {
					e.stopPropagation();

					$scope.$evalAsync(function () {
						bngApi.activeObjectLua(`extensions.arcanox_quickTuning.resetValues("${module.name}")`);
					});
				};

				$scope.resetValue = function (e, module, parameter) {
					e.stopPropagation();

					$scope.$evalAsync(function () {
						bngApi.activeObjectLua(`extensions.arcanox_quickTuning.resetValue("${module.name}", "${parameter.name}")`);
					});
				};

				$scope.parameterChanged = RateLimiter.debounce(function (module, parameter) {
					$scope.$evalAsync(function () {
						bngApi.activeObjectLua(`extensions.arcanox_quickTuning.updateParameter("${module.name}", "${parameter.name}", ${parameter.value})`);
					});
				}, 50);
			}],
			link: function (scope, element, attrs) {		
				let app = scope.app;

				scope.$on("ArcTuningRegister", function (_, registrations) {
					scope.$evalAsync(function () {
						if (!Array.isArray(registrations)) {
							scope.tuningModules = null;
							return;
						}

						for (let registration of registrations) {
							if (!Array.isArray(registration.parameters)) {
								continue;
							}
							
							registration.parameters = registration.parameters.map(p => new Parameter(p));
						}

						scope.tuningModules = registrations;
					});
				});

				scope.$on("VehicleReset", function () {
					app.reset();
				});

				scope.$on("VehicleFocusChanged", function () {
					app.reset();
				});

				app.reset();
			}
		};
	}]);
