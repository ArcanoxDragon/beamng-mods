angular
	.module("beamng.apps")
	.directive("arcDebug", [function () {
		return {
			templateUrl: "/ui/modules/apps/ArcanoxDebug/arc-debug.html",
			replace: true,
			restrict: "E",
			scope: true,
			controller: ["$scope", function ($scope) {
				$scope.Math = Math;

				let checkInvalidModuleInterval = null;
				let requestedFullUpdate = false;
				let currentModuleName = localStorage.getItem("apps:ArcanoxDebug.currentModuleName");
				let debugModuleNames = [];
				let dataCaches = {};
				let debugData = [];
				let numValueColumns = 1;

				function requestFullUpdate() {
					$scope.$evalAsync(function () {
						bngApi.activeObjectLua("extensions.arcanox_debug.requestState(true)");
					});
				}

				function resetDebug() {
					debugModuleNames = [];
					dataCaches = {};
					debugData = [];
					numValueColumns = 1;
					requestedFullUpdate = false;

					$scope.debugData = null;
					$scope.numValueColumns = numValueColumns;
				}

				function updateScopeData() {
					if (!currentModuleName)
						return;

					let dataCache = dataCaches[currentModuleName];

					debugData = [];
					numValueColumns = 1;

					if (!dataCache)
						return;

					for (let key of Object.keys(dataCache)) {
						let entry = dataCache[key];

						if (!entry || typeof entry !== "object")
							continue;

						let { id, title: name, value } = entry;

						if (value === false) {
							continue;
						}

						if (Array.isArray(value)) {
							numValueColumns = Math.max(numValueColumns, value.length);
						} else {
							value = [value];
						}

						debugData.push({ id, name, value });
					}

					debugData.sort((a, b) => a.id - b.id);

					$scope.currentModuleName = currentModuleName;
					$scope.debugData = debugData;
					$scope.numValueColumns = numValueColumns;
				}

				$scope.changeModule = function () {
					requestFullUpdate();

					if (debugModuleNames.length === 0) {
						currentModuleName = null;
						delete $scope.debugData;

						if (checkInvalidModuleInterval) {
							clearInterval(checkInvalidModuleInterval);
							checkInvalidModuleInterval = null;
						}
						return;
					}

					let curIndex = debugModuleNames.indexOf(currentModuleName);

					// Delete the existing debug data to prepare for the new module
					delete $scope.debugData;

					if (curIndex < 0) {
						currentModuleName = debugModuleNames[0];
						return;
					}

					curIndex = (curIndex + 1) % debugModuleNames.length;
					currentModuleName = debugModuleNames[curIndex];
					localStorage.setItem("apps:ArcanoxDebug.currentModuleName", currentModuleName);

					updateScopeData();
				};

				function checkInvalidModule() {
					if (!currentModuleName || !(currentModuleName in dataCaches) || dataCaches[currentModuleName].disabled) {
						$scope.changeModule();
					}
				}

				$scope.$on("$destroy", function () {
					clearInterval(checkInvalidModuleInterval);
					checkInvalidModuleInterval = null;
				});

				$scope.captureFreezeframe = function (e) {
					e.stopPropagation();

					$scope.$evalAsync(function () {
						bngApi.activeObjectLua("extensions.arcanox_debug.freezeFrame()");
					});
				};

				$scope.$on("UpdateArcanoxDebug", function (_, updates) {
					$scope.$evalAsync(function () {
						if (!requestedFullUpdate) {
							requestFullUpdate();
							requestedFullUpdate = true;
						}

						for (let moduleName of Object.keys(updates)) {
							let update = updates[moduleName];

							if (update.disabled === true) {
								if (moduleName in dataCaches) {
									delete dataCaches[moduleName];
								}

								continue;
							}

							let cache = dataCaches[moduleName];

							if (!cache) {
								cache = dataCaches[moduleName] = {};
							}

							for (let key of Object.keys(update)) {
								let entry = update[key];

								if (!entry) {
									continue;
								}

								let { id } = entry;

								if (entry.destroy === true) {
									if (id in cache)
										delete cache[id];
								} else {
									cache[id] = entry;
								}
							}

							if (Object.keys(cache).length === 0) {
								if (moduleName in dataCaches) {
									delete dataCaches[moduleName];
								}
							}
						}

						debugModuleNames = Object.keys(dataCaches);
						updateScopeData();

						if (currentModuleName && currentModuleName in dataCaches && !dataCaches[currentModuleName].disabled) {
							// Restart the timeout to check for an invalid module if we have a valid one right now

							if (checkInvalidModuleInterval) {
								clearInterval(checkInvalidModuleInterval);
								checkInvalidModuleInterval = null;
							}
						}

						if (!checkInvalidModuleInterval) {
							checkInvalidModuleInterval = setInterval(checkInvalidModule, 3000);
						}
					});
				});

				$scope.$on("VehicleReset", resetDebug);
				$scope.$on("VehicleChange", resetDebug);
				$scope.$on("VehicleFocusChanged", resetDebug);
			}]
		};
	}]);
