local M = {}
M.type = "auxiliary"
M.relevantDevice = nil
M.defaultOrder = 2000

local mathutil = require("arcanox/mathutil")
local DebugModule = require("arcanox/DebugModule")

-- devices
local engine
local autoStop

-- upvalue functions
local abs = math.abs
local blend = mathutil.blend
local clamp = mathutil.clamp

-- state
local lastPower = false
local isInStartupAnim = false
local startupAnimTime = 0

local function updateGFX(dt)
	local rpmDisplay
	local speedDisplay

	if (electrics.values.ignition and engine.ignitionCoef > 0.01) or (autoStop and autoStop.isAutoStopped) then
		if not lastPower then
			isInStartupAnim = true
			startupAnimTime = 0
			lastPower = true
		end

		if isInStartupAnim then
			local movingAnimTime = M.totalAnimationTime - 0.5 -- half second pause at end
			local animTimeWithPause = movingAnimTime + 0.1
			local gaugeCoef = clamp(animTimeWithPause - abs(2 * animTimeWithPause * startupAnimTime - animTimeWithPause), 0, 1) -- fancy time function to animate up and back down

			rpmDisplay = blend(M.minRpm, M.maxRpm, gaugeCoef)
			speedDisplay = blend(M.minSpeed, M.maxSpeed, gaugeCoef)
			startupAnimTime = startupAnimTime + dt

			if startupAnimTime >= M.totalAnimationTime then
				isInStartupAnim = false
				startupAnimTime = 0
			end
		else
			-- Push correct value to tacho
			rpmDisplay = electrics.values.rpm
			speedDisplay = electrics.values.wheelspeed
		end
	else
		-- Make tacho needle drop to "Off"
		rpmDisplay = M.minRpm
		speedDisplay = M.minSpeed
		lastPower = false
	end

	electrics.values.rpmDisplay = M.rpmDisplaySmoother:get(rpmDisplay, dt)
	electrics.values.speedDisplay = M.speedDisplaySmoother:get(speedDisplay, dt)

	M.debug:putValue("Startup Animation", isInStartupAnim)
	M.debug:putValue("Startup Anim. Time", startupAnimTime, 3)
	M.debug:putValue("RPM Display", rpmDisplay, 3)
	M.debug:putValue("Speed Display", speedDisplay, 3)
end

local function init(jbeamData)
	M.jbeamData = jbeamData

	M.debug = DebugModule:new("autoStopGauges", jbeamData.enableDebug)

	M.minRpm = jbeamData.minRpm or -500
	M.maxRpm = jbeamData.maxRpm or 7500
	M.minSpeed = jbeamData.minSpeed or 0
	M.maxSpeed = jbeamData.maxSpeed or 72.222 -- m/s
	M.totalAnimationTime = jbeamData.totalAnimationTime or 2.0
	M.rpmDisplaySmoother = newTemporalSmoothingNonLinear(jbeamData.rpmSmoothing or 10)
	M.speedDisplaySmoother = newTemporalSmoothingNonLinear(jbeamData.speedSmoothing or 10)
	M.kwDisplaySmoother = newTemporalSmoothingNonLinear(jbeamData.kwSmoothing or 10)
end

local function initSecondStage()
	engine = powertrain.getDevice("mainEngine")
	autoStop = controller.getController("arcanox/autoStop")

	if not engine then
		log("E", "etkAutoStopGauges", "Main engine device not found")
		M.updateGFX = nop
	end

	if not autoStop then
		log("W", "etkAutoStopGauges", "Auto-stop controller not found")
		return
	end
end

local function reset()
	init(M.jbeamData)
	initSecondStage()
end

M.updateGFX = updateGFX
M.init = init
M.initSecondStage = initSecondStage
M.reset = reset

return M