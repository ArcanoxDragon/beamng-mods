local M = {}
M.type = "auxiliary"
M.relevantDevice = nil
M.defaultOrder = 1000

-- Required modules
local DebugModule = require("arcanox/DebugModule")

-- Constants/helpers
local supportedGearboxes = {
	cvtGearbox = true,
	automaticGearbox = true
}
local mpsToMph = 2.23694

-- Controllers and devices
local engine = nil
local gearbox = nil

-- Initial data
local initialEngineStarterTorque = 0
local initialEngineStarterMaxAV = 0
local initialEngineStarterTime = 0
local initialEngineStartCoef = 0

-- State variables
local state = "init"
local nextState = nil
local timeSinceWheelsStopped = 0
local timeSinceAutoStopped = 0
local timeSinceEngineRestart = 0
local timeSinceEngineStalled = 0
local hasMovedSinceRestart = false

-- Easy-access upvalues
local watertemp = 0
local engineRunning = false
local speedMph = 0
local gear = nil
local brake = 0

local function why(str)
	M.debug:putValue("Auto-stop Disabled Reason", str)
end

local function canAutoStop()
	-- Check criteria for auto-stop

	if watertemp < M.autoStopCoolantThreshold then
		-- Engine must be warm enough
		why("coolant temp")
		return false
	end
	if gear ~= "D" then
		-- Must be in Drive
		why("gear")
		return false
	end
	if brake < M.brakeThreshold then
		-- Must have foot on brake
		why("brake")
		return false
	end
	if speedMph > M.autoStopSpeedThreshold or timeSinceWheelsStopped < M.autoStopMinStoppedTime then
		-- Must be stopped
		why("speed")
		return false
	end
	if timeSinceAutoStopped > M.autoStopMaxTime then
		-- Can't be auto-stopped longer than the set timeout
		why("timeout")
		return false
	end

	-- TODO: check starter battery level once implemented by BeamNG

	return true
end

--- Overrides some electrics and controller values so that things like Daytime Running Lights
--- and the "Engine Start Stop" button's light in the GUI stay on, and the "check engine" light
--- on the car's dashboard stays off, whenever the engine stops during an auto-stop event.
local function overrideEngineVisualState()
	electrics.values.checkengine = false -- Keep this from coming on (usually tied to engine.isDisabled)
	electrics.values.ignition = true -- Keep things like DRLs on
	electrics.values.running = true
	controller.mainController.engineInfo[18] = 1
end

local function processState(dt)
	M.debug:putValue("Water temp", watertemp, 1)
	M.debug:putValue("Gear", gear)
	M.debug:putValue("Brake", brake, 1)
	M.debug:putValue("Speed", speedMph, 1)
	M.debug:putValue("Time auto-stopped", timeSinceAutoStopped, 2)
	M.debug:putValue("Time since restart", timeSinceEngineRestart, 2)
	M.debug:putValue("State", state)

	if state == "init" then
		M.isAutoStopped = false
		timeSinceWheelsStopped = 0
		timeSinceAutoStopped = 0
		timeSinceEngineRestart = 0
		timeSinceEngineStalled = 0
		hasMovedSinceRestart = false

		if engineRunning then
			nextState = "idle"
		else
			nextState = "shutdown"
		end
	elseif state == "idle" then
		-- Check if we can auto-stop

		M.isAutoStopped = false

		if engineRunning then
			timeSinceEngineStalled = 0

			if speedMph <= M.autoStopSpeedThreshold then
				timeSinceWheelsStopped = math.min(timeSinceWheelsStopped + dt, 999.0)
			else
				timeSinceWheelsStopped = 0
			end

			if speedMph > M.autoStopReenableSpeedThreshold then
				hasMovedSinceRestart = true
			end

			if gear ~= "D" then
				-- Can't transition into auto-stop just by shifting to drive
				hasMovedSinceRestart = false
			end

			if canAutoStop() and hasMovedSinceRestart then
				timeSinceAutoStopped = 0
				nextState = "autostop"
			end
		else
			-- Engine stalled or was shut down by driver

			timeSinceEngineStalled = timeSinceEngineStalled + dt

			if timeSinceEngineStalled > M.engineStallDetectTime then nextState = "shutdown" end
		end
	elseif state == "shutdown" then
		-- See if engine is started so we can resume logic

		electrics.values.ignition = false
		electrics.values.running = false

		if engineRunning then
			nextState = "idle"
		end
	elseif state == "autostop" then
		-- Handle stopping engine and checking if we need to restart

		M.isAutoStopped = true
		engine.ignitionCoef = 0 -- Kill engine
		engine.isDisabled = true -- Prevent the user from manually restarting the engine
		overrideEngineVisualState()
		timeSinceAutoStopped = timeSinceAutoStopped + dt
		hasMovedSinceRestart = false

		if not canAutoStop() then
			nextState = "restart"
		end
	elseif state == "restart" then
		engine.isDisabled = false
		engine.starterTorque = initialEngineStarterTorque * 2
		engine.starterMaxAV = engine.idleAV * 0.5
		engine.starterThrottleKillTime = initialEngineStarterTime / 2 -- Restarting is faster
		engine.idleStartCoef = 0.85
		timeSinceEngineRestart = 0
		gearbox:setMode("neutral") -- Temporarily disengage transmission
		engine:activateStarter()
		overrideEngineVisualState()

		nextState = "restarting"
	elseif state == "restarting" then
		engine.ignitionCoef = M.startingIgnitionCoef
		overrideEngineVisualState()
		electrics.values.brake = 1.0
		electrics.values.brakelights = 1

		if engineRunning and engine.starterEngagedCoef < 0.1 then
			timeSinceEngineRestart = timeSinceEngineRestart + dt
			engine.starterTorque = initialEngineStarterTorque
			engine.starterMaxAV = initialEngineStarterMaxAV
			engine.starterThrottleKillTime = initialEngineStarterTime

			-- Reengage transmission
			if gear == "P" then
				gearbox:setMode("park")
			elseif gear ~= "N" then
				gearbox:setMode("drive")
			end

			electrics.values.brake = brake
		end

		if timeSinceEngineRestart > M.engineRestartDelayTime then -- Restart complete
			engine.ignitionCoef = 1.0
			engine.idleStartCoef = initialEngineStartCoef
			nextState = "idle"
		end
	end
end

local function updateGFX(dt)
	watertemp = electrics.values.watertemp
	engineRunning = not (engine.isStalled or engine.ignitionCoef < 0.01)
	speedMph = (electrics.values.wheelspeed or 0) * mpsToMph
	gear = electrics.values.gear or "P"
	brake = input.brake or 0

	processState(dt)

	if nextState then
		state = nextState
		nextState = nil
	end
end

local function init(jbeamData)
	M.debug = DebugModule:new("autoStop", jbeamData.enableDebug)

	M.autoStopMaxTime = jbeamData.autoStopMaxTime or 60 -- seconds
	M.brakeThreshold = jbeamData.brakeThreshold or 0.15
	M.startingIgnitionCoef = jbeamData.startingIgnitionCoef or 0.2
	M.autoStopCoolantThreshold = jbeamData.autoStopCoolantThreshold or 50 -- degrees Celsius
	M.autoStopSpeedThreshold = jbeamData.autoStopSpeedThreshold or 0.1 -- MPH
	M.autoStopReenableSpeedThreshold = jbeamData.autoStopReenableSpeedThreshold or 3.0 -- MPH
	M.autoStopMinStoppedTime = jbeamData.autoStopMinStoppedTime or 0.2 -- seconds
	M.engineRestartDelayTime = jbeamData.engineRestartDelayTime or 0.55 -- seconds
	M.engineStallDetectTime = jbeamData.engineStallDetectTime or 0.5

	M.isAutoStopped = false
end

local function initSecondStage()
	engine = powertrain.getDevice("mainEngine")
	gearbox = powertrain.getDevice("gearbox")

	if engine and gearbox and supportedGearboxes[gearbox.type or "none"] then
		initialEngineStarterTorque = engine.starterTorque
		initialEngineStarterMaxAV = engine.starterMaxAV
		initialEngineStarterTime = engine.starterThrottleKillTime
		initialEngineStartCoef = engine.idleStartCoef

		M.updateGFX = updateGFX
	end
end

local function reset()
	state = "init"
end

M.updateGFX = nop
M.init = init
M.initSecondStage = initSecondStage
M.reset = reset

return M
