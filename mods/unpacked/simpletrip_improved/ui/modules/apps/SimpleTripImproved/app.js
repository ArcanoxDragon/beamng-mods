(function () {
	"use strict";
	
	const Modes = [
		{
			id: 0,
			text: "Total Distance",
		},
		{
			id: 1,
			text: "AVG Speed",
		},
		{
			id: 2,
			text: "AVG Fuel Economy",
		},
		{
			id: 3,
			text: "Fuel Economy",
		},
		{
			id: 4,
			text: "Range",
		},
		{
			id: 5,
			text: "Total Fuel Consumed",
		}
	];

	const ModeIds = {
		TotalDistance: 0,
		AverageSpeed: 1,
		AverageFuelConsumption: 2,
		FuelConsumption: 3,
		Range: 4,
		TotalFuelConsumed: 5,
	};
	
	const NumVisibleModes = 2;
	const DefaultModes = [ModeIds.AverageFuelConsumption, ModeIds.FuelConsumption];

	angular.module("beamng.apps").directive("simpleTripImproved", ["logger", function (logger) {
		return {
			templateUrl: "/ui/modules/apps/SimpleTripImproved/app.html",
			link: function (scope, element, attrs) {
				let streamsList = ["electrics", "engineInfo"];

				StreamsManager.add(streamsList);

				scope.$on("$destroy", function () {
					StreamsManager.remove(streamsList);
				});

				let timer = 0;
				let prevTime = performance.now();
				let curTime = prevTime;
				let count = 0;
				let totalTime = 0;
				let totalDistance = 0;
				let range = 0;
				let avgSpeed = 0;
				let fuelConsumptionRate = 0;
				let avgFuelConsumptionRate = 0;
				let previousFuel = 0;
				let startingFuel = -1;
				let pastFuelConsumption = 0;
				let avgFuelConsumptionSum = 0;
				let totalFuelConsumption = 0;

				scope.reset = function ($event) {
					logger.debug("<simple-trip> resetting trip computer");
					timer = 0;
					prevTime = performance.now();
					curTime = prevTime;
					count = 0;
					totalTime = 0;
					totalDistance = 0;
					range = 0; 2;
					avgSpeed = 0;
					fuelConsumptionRate = 0;
					avgFuelConsumptionRate = 0;
					previousFuel = 0;
					startingFuel = -1;
					pastFuelConsumption = 0;
					avgFuelConsumptionSum = 0;
					totalFuelConsumption = 0;

					$event.stopPropagation();
				};

				// default if localStorage isn't present
				let modes = DefaultModes;

				scope.modes = modes.map(() => {
					return { display: "", text: "" };
				});

				try {
					let storedModes = JSON.parse(localStorage.getItem("apps:simpleTripImproved.modes"));

					if (Array.isArray(storedModes)) {
						modes.splice(0, storedModes.length, ...storedModes);
					}
					
					if (modes.length < NumVisibleModes) {
						modes = DefaultModes;
					}
				} catch (e) {}

				scope.changeMode = function (index, targetMode) {
					if (index < 0 || index >= modes.length) {
						return;
					}

					let newMode = targetMode || (modes[index] + 1) % Modes.length;

					if (!Modes[newMode]) {
						newMode = Modes[0].id;
					}

					modes[index] = newMode;

					scope.modes[index].display = Modes[newMode].text;

					localStorage.setItem("apps:simpleTripImproved.modes", JSON.stringify(modes));
				};


				scope.$emit("requestPhysicsState");

				// if physics is resumed, throw away timestamp from before, since the vehicle didn't move in the mean time
				// => no streamsupdate
				// => no new timestamp in the meantime
				// => curTime === timestamp from when physics was paused
				scope.$on("updatePhysicsState", function (event, data) {
					if (data) {
						curTime = performance.now();
					}
				});

				scope.$on("streamsUpdate", function (event, streams) {
					if (streams.electrics && streams.engineInfo) {
						let wheelSpeed = streams.electrics.wheelspeed;
						let fuelVolume = streams.engineInfo[11];
						let fuelConsumption = previousFuel - fuelVolume;

						if (startingFuel < 0) {
							// initialize starting fuel volume
							startingFuel = fuelVolume;
						}

						if (fuelConsumption < -0.1) {
							// tank volume increased by a lot (refuel or vehicle reset)
							// adjust total consumption
							pastFuelConsumption += (startingFuel - previousFuel);
							startingFuel = fuelVolume;
							fuelConsumption = 0;
						}

						prevTime = curTime;
						previousFuel = fuelVolume;

						curTime = performance.now();

						let elapsed = (curTime - prevTime) / 1000; // seconds

						timer -= elapsed;
						totalTime += elapsed;
						totalDistance += wheelSpeed * elapsed; // m/s * s = m
						totalFuelConsumption += fuelConsumption;
						avgFuelConsumptionSum += fuelConsumption;

						if (timer < 0) {
							count++;
							avgSpeed = totalDistance / totalTime; // m/s

							if (avgFuelConsumptionSum > 0.0002) {
								fuelConsumptionRate = avgFuelConsumptionSum / ((1 - timer) * streams.electrics.wheelspeed); // l/(s*(m/s)) = l/m
							} else {
								fuelConsumptionRate = 0;
							}

							range = avgFuelConsumptionRate > 0
								? UiUnits.buildString("distance", fuelVolume / avgFuelConsumptionRate, 2)
								: (streams.electrics.wheelspeed > 0.5
									? "Infinity"
									: UiUnits.buildString("distance", 0));
							avgFuelConsumptionSum = 0;
							avgFuelConsumptionRate = totalFuelConsumption / Math.max(totalDistance, 1e-30); // l/m
							timer = 1;
						}

						scope.$evalAsync(() => {
							for (let i = 0; i < modes.length; i++) {
								switch (modes[i]) {
									case ModeIds.TotalDistance:
										scope.modes[i].text = UiUnits.buildString("distance", totalDistance, 1);
										break;

									case ModeIds.AverageSpeed:
										scope.modes[i].text = UiUnits.buildString("speed", avgSpeed, 1);
										break;

									case ModeIds.AverageFuelConsumption:
										scope.modes[i].text = UiUnits.buildString("consumptionRate", avgFuelConsumptionRate, 1);
										break;

									case ModeIds.FuelConsumption:
										if (UiUnits.uiUnits.uiUnitEnergy === "imperial" && wheelSpeed > 0.5 && fuelConsumptionRate === 0) {
											scope.modes[i].text = "999.9 MPG";
										} else {
											scope.modes[i].text = UiUnits.buildString("consumptionRate", fuelConsumptionRate, 1);
										}
										break;

									case ModeIds.Range:
										scope.modes[i].text = range;
										break;
										
									case ModeIds.TotalFuelConsumed:
										scope.modes[i].text = UiUnits.buildString("volume", totalFuelConsumption, 3);
										break;
								}
							}
						});
					}
				});

				// run on launch
				for (let i = 0; i < modes.length; i++)
					scope.changeMode(i, modes[i]);

				setTimeout(function () {
					bngApi.engineLua("settings.requestState()");
				}, 500);
			}
		};
	}]);
})();