angular.module('gaugesScreen', [])
	.controller('GaugesScreenController', function ($scope, $window) {
		let kw = null;
		let powerOn = false;
		let ready = false;
		let ev = false;
		let regenKw = 80;
		let ecoKw = 0;
		let maxKw = 120;
		let batteryLevel = 0;
		let init = true;
		let animInterval = null;

		// We need access to the some SVG elements so that we can animate it
		let eff;
		let ecoMark;
		let battBars;

		function maprange(x, fromStart, fromEnd, toStart, toEnd, clamp) {
			toStart = toStart || 0;
			toEnd = toEnd || 1;
			clamp = clamp !== false;

			let ret = ((x - fromStart) / (fromEnd - fromStart) * (toEnd - toStart)) + toStart;

			if (clamp) {
				let cMin = Math.min(toStart, toEnd);
				let cMax = Math.max(toStart, toEnd);

				ret = Math.max(Math.min(ret, cMax), cMin);
			}

			return ret;
		}

		animInterval = setInterval(function () {
			let alternate = (performance.now() % 500) <= 250;

			if (powerOn && !ready) {
				$scope.data.ready = alternate;
			}
		}, 1 / 60);

		$scope.$on("$destroy", function () {
			clearInterval(animInterval);
		});

		$scope.data = {};

		$scope.svgLoaded = function () {
			eff = document.getElementById("efficiency");
			ecoMark = document.getElementById('markTwo');

			let battBarsGroup = document.getElementById("g_battbars");

			if (battBarsGroup) {
				let battBarsArray = Array(battBarsGroup.childElementCount).fill(null);

				for (let child of battBarsGroup.children) {
					let idMatch = child.id.match(/^battbar_(\d+)$/i);

					if (!idMatch || isNaN(+idMatch[1])) {
						continue;
					}

					let id = +idMatch[1];

					if (id >= 0 && id < battBarsArray.length) {
						battBarsArray[id] = child;
					}
				}

				battBars = battBarsArray;
			}
		};

		$window.setup = data => {
			$scope.units = data.unit;
		};

		$window.updateData = data => {
			$scope.$evalAsync(function() {
				const {
					customModules: {
						etkHybridGauges = {
							fixedAvgFuelConsumption: 0,
							params: {
								maxKw: 0,
								ecoKw: 0,
								regenKw: 0,
							},
							kw: 0,
							powerOn: false,
							ready: false,
							ev: false,
							batteryLevel: 0,
							init: false,
						},
					},
				} = data;
				const { params } = etkHybridGauges;

				maxKw = params.maxKw;
				ecoKw = params.ecoKw;
				regenKw = params.regenKw;

				kw = etkHybridGauges.kw;
				powerOn = etkHybridGauges.powerOn;
				ready = etkHybridGauges.ready;
				ev = etkHybridGauges.ev;
				batteryLevel = etkHybridGauges.batteryLevel;
				init = etkHybridGauges.init;

				if (eff) {
					// dash array needs to be the same as the circumference of the circle (radius * 2 * PI)
					eff.style.strokeDasharray = 342;

					let isEco = ev && ecoKw > 0 && kw <= ecoKw;
					let pwrBarStrokeOffset;

					if (kw >= 0) {
						if (ecoKw > 0) {
							pwrBarStrokeOffset = maprange(kw, 0, 1, 0, 4.5) + maprange(kw, 1, ecoKw, 0, 19.5) + maprange(kw, ecoKw, maxKw, 0, 42.5);
						} else {
							pwrBarStrokeOffset = maprange(kw, 0, 1, 0, -2) + maprange(kw, 1, maxKw, 0, 69);
						}

						eff.style.stroke = isEco ? "#4EFF4E" : "#EFBE49";
					} else {
						pwrBarStrokeOffset = maprange(kw, -1, 0) + maprange(kw, -1, -regenKw, 0, -21);

						eff.style.stroke = "#00e4ff";
					}

					// We need to add the value to the negative circumference of the circle so that we can fill up the bar
					eff.style.strokeDashoffset = -342 + pwrBarStrokeOffset;
				}

				if (+data.electrics.gear === -1) {
					$scope.data.gear = "R";
				} else if (+data.electrics.gear === 0) {
					$scope.data.gear = "N";
				} else {
					$scope.data.gear = data.electrics.gear;
				}

				$scope.data.gaugesVisible = powerOn || false;
				$scope.data.time = init ? "--:--" : data.time;
				$scope.data.ev = ev ? "EV" : "";

				if (powerOn && ready) {
					$scope.data.ready = true;
				} else if (!powerOn && !ready) {
					$scope.data.ready = false;
				} // otherwise, animation handler will animate it above

				const temp = data.customModules.environmentData.temperatureEnv;

				// checking if metric gauge cluster is being used
				if ($scope.units === "metric") {
					$scope.data.speedUnit = "km/h";
					$scope.data.tempUnit = "°C";
					$scope.data.consumptionUnit = "l/100km";

					if (init) {
						$scope.data.speedVal = "---";
						$scope.data.tempVal = "---";
						$scope.data.consumptionVal = "---";
					} else {
						$scope.data.speedVal = (data.electrics.wheelspeed * 3.6).toFixed(0);

						if (temp.toFixed(1) > 99.9 || temp.toFixed(1) < -99.9) {
							$scope.data.tempVal = "---";
						} else {
							$scope.data.tempVal = temp.toFixed(1);
						}
						if (etkHybridGauges.fixedAvgFuelConsumption === 0) {
							$scope.data.consumptionVal = "---";
						} else {
							$scope.data.consumptionVal = Math.min(99.9, etkHybridGauges.fixedAvgFuelConsumption.toFixed(1));
						}
					}
				} else {
					$scope.data.speedUnit = "mph";
					$scope.data.tempUnit = "°F";
					$scope.data.consumptionUnit = "mpg";

					if (init) {
						$scope.data.speedVal = "---";
						$scope.data.tempVal = "---";
						$scope.data.consumptionVal = "---";
					} else {
						$scope.data.speedVal = (data.electrics.wheelspeed * 2.23694).toFixed(0);
						$scope.data.tempVal = (temp * 1.8 + 32).toFixed(0);
						if (etkHybridGauges.fixedAvgFuelConsumption < 0.01) {
							$scope.data.consumptionVal = "---";
						} else {
							$scope.data.consumptionVal = Math.min(99.9, 235 / etkHybridGauges.fixedAvgFuelConsumption).toFixed(1);
						}
					}
				}

				if (ecoMark) {
					// set text of gauge markings depending on if we have an "ecoKw" set or not
					ecoMark.textContent = ecoKw > 0 ? "eco" : "";
				}

				if (battBars) {
					let numBarsVisible = Math.round(batteryLevel * battBars.length);

					for (let i = 0; i < battBars.length; i++) {
						battBars[i].style.opacity = i <= numBarsVisible ? 1 : 0;
					}
				}
			});
		};
	});