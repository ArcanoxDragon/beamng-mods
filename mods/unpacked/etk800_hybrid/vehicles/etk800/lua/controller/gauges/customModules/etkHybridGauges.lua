local M = {}
M.type = "auxiliary"
M.relevantDevice = nil
M.defaultOrder = 2000

-- local htmlTexture = require("htmlTexture")
local mathutil = require("arcanox/mathutil")
local DebugModule = require("arcanox/DebugModule")

-- upvalue functions
local min = math.min
local max = math.max
local blend = mathutil.blend
local clamp = mathutil.clamp
local maprange = mathutil.maprange

-- other parts
local hybrid
local gearbox

-- constants
local fuelEconomyUpdateInterval = 4

local function updateGaugesAnim(dt)
	if not hybrid or not gearbox then
		return 0, 0, 0, 0, 0, 0
	end

	local rpmDisplay = M.minRpm
	local speedDisplay = M.minSpeed
	local fuelDisplay = 0
	local oiltempDisplay = obj:getEnvTemperature() - 273.15 -- default to ambient
	local batteryDisplay = 0
	local kwDisplay = 0

	if hybrid.powerOn or hybrid.powerSwitch then
		if not M.lastPower then
			M.isInStartupAnim = true
			M.currentAnimTime = 0
			M.lastPower = true
		end

		if M.isInStartupAnim then
			local sweepTime = M.sweepAnimationTime
			local topPauseTime = M.sweepTopPauseTime
			local time = M.currentAnimTime
			local slopeStart = min(1, 2 * time / (sweepTime - topPauseTime)) -- fancy time function to animate up and back down
			local slopeEnd = min(1, 2 * (sweepTime - time) / (sweepTime - topPauseTime))
			local gaugeCoef = clamp(min(slopeStart, slopeEnd), 0, 1)

			rpmDisplay = blend(M.minRpm, M.maxRpm, gaugeCoef)
			speedDisplay = blend(M.minSpeed, M.maxSpeed, gaugeCoef)
			fuelDisplay = gaugeCoef
			oiltempDisplay = blend(M.minOiltemp, M.maxOiltemp, gaugeCoef)
			batteryDisplay = gaugeCoef

			if time >= sweepTime then -- 3rd segment
				kwDisplay = maprange(time, sweepTime, M.totalAnimationTime, -M.lastRegenKw, 0)
			elseif time <= sweepTime / 2 then -- 1st segment
				kwDisplay = maprange(time, sweepTime / 2, 0, M.lastMaxKw)
			else -- 2nd segment
				kwDisplay = maprange(time, sweepTime / 2, sweepTime, M.lastMaxKw, -M.lastRegenKw)
			end

			M.currentAnimTime = M.currentAnimTime + dt

			if M.currentAnimTime >= M.totalAnimationTime then
				M.isInStartupAnim = false
				M.currentAnimTime = 0
			end
		else
			-- Push correct value to tacho
			rpmDisplay = electrics.values.rpm or M.minRpm
			speedDisplay = electrics.values.wheelspeed or M.minSpeed
			fuelDisplay = electrics.values.fuel or 0
			oiltempDisplay = electrics.values.oiltemp or M.minOiltemp
			batteryDisplay = electrics.values.batteryLevel or 0
			kwDisplay = electrics.values.kw or 0
		end
	else
		M.lastPower = false
	end

	return rpmDisplay, speedDisplay, fuelDisplay, oiltempDisplay, batteryDisplay, kwDisplay
end

local function updateFuelEconomy(dt)
	local wheelspeed = electrics.values.wheelspeed or 0
	local fuelVolume = electrics.values.fuelVolume or 0
	local fuelConsumption = M.lastFuel - fuelVolume -- L

	if fuelConsumption > -0.1 then -- if the fuel volume *increases* by a notable amount, don't update
		local distanceTraveled = dt * wheelspeed -- s * m/s = m

		M.fuelSum = M.fuelSum + fuelConsumption
		M.distanceSum = M.distanceSum + distanceTraveled
	end

	M.lastFuel = fuelVolume

	return clamp(100 * M.fuelSum / max(1e-30, M.distanceSum / 1000), 0, 1e4) -- l/km * 100 = l/100km
end

local function updateGFX(dt)
	local rpmDisplay, speedDisplay, fuelDisplay, oiltempDisplay

	rpmDisplay, speedDisplay, fuelDisplay, oiltempDisplay, M.batteryDisplay, M.kwDisplay = updateGaugesAnim(dt)

	M.batteryDisplay = M.batterySmoother:getCapped(M.batteryDisplay, dt)
	M.kwDisplay = M.kwSmoother:get(M.kwDisplay, dt)

	local avgFuelConsumption = updateFuelEconomy(dt)

	M.timeSinceFuelEconomyUpdate = M.timeSinceFuelEconomyUpdate + dt

	if M.timeSinceFuelEconomyUpdate >= fuelEconomyUpdateInterval then
		M.averageFuelConsumption = avgFuelConsumption
		M.timeSinceFuelEconomyUpdate = 0
	end

	electrics.values.rpmDisplay = M.rpmDisplaySmoother:get(rpmDisplay, dt)
	electrics.values.speedDisplay = M.speedDisplaySmoother:get(speedDisplay, dt)
	electrics.values.fuelDisplay = M.fuelDisplaySmoother:get(fuelDisplay, dt)
	electrics.values.oiltempDisplay = M.oiltempDisplaySmoother:get(oiltempDisplay, dt)

	if M.debugEnabled then
		M.debug:putValue("RPM Display", rpmDisplay, 3)
		M.debug:putValue("Speed Display", speedDisplay, 3)
	end
end

local function setupGaugeData()
	hybrid = controller.getController("hybridPowertrain")
	gearbox = powertrain.getDevice("gearbox")

	if not hybrid then
		log("E", "etkHybridGauges", "Hybrid controller not found")
		M.updateGaugeData = nop
		return
	end

	if not gearbox or gearbox.type ~= "ecvtGearbox" then
		log("E", "etkHybridGauges", "Could not find eCVT gearbox")
		M.updateGaugeData = nop
		return
	end
end

local function updateGaugeData(moduleData)
	local maxKw, ecoKw, regenKw = hybrid.getPowerLimits()
	local powerOn = hybrid.powerSwitch or false
	local ready = hybrid.powerSwitch and hybrid.powerOn
	local ev = (electrics.values.ev or 0) > 0

	moduleData.fixedAvgFuelConsumption = M.averageFuelConsumption
	moduleData.params = { maxKw = maxKw, ecoKw = ecoKw, regenKw = regenKw }
	moduleData.kw = M.kwDisplay
	moduleData.powerOn = powerOn
	moduleData.ready = ready
	moduleData.ev = ev
	moduleData.batteryLevel = M.batteryDisplay
	moduleData.init = M.isInStartupAnim

	M.lastMaxKw = maxKw
	M.lastEcoKw = ecoKw
	M.lastRegenKw = regenKw

	if M.debug.enabled then
		M.debug:putValue("Startup Animation", M.isInStartupAnim)
		M.debug:putValue("Startup Anim. Time", M.currentAnimTime, 3)
	end
end

local function init(jbeamData)
	M.jbeamData = jbeamData

	M.debug = DebugModule:new("hybridGauges", jbeamData.enableDebug)

	M.minRpm = jbeamData.minRpm or -500
	M.maxRpm = jbeamData.maxRpm or 7500
	M.minSpeed = jbeamData.minSpeed or 0
	M.maxSpeed = jbeamData.maxSpeed or 72.222 -- m/s
	M.minOiltemp = jbeamData.minOiltemp or 71 -- °C
	M.maxOiltemp = jbeamData.maxOiltemp or 171 -- °C
	M.sweepAnimationTime = jbeamData.sweepAnimationTime or 1.5
	M.sweepTopPauseTime = jbeamData.sweepTopPauseTime or 0.25
	M.sweepEndPauseTime = jbeamData.sweepEndPauseTime or 0.5
	M.totalAnimationTime = M.sweepAnimationTime + M.sweepEndPauseTime
	M.rpmDisplaySmoother = newTemporalSmoothingNonLinear(jbeamData.gaugeSmoothing or 10)
	M.speedDisplaySmoother = newTemporalSmoothingNonLinear(jbeamData.gaugeSmoothing or 10)
	M.fuelDisplaySmoother = newTemporalSmoothingNonLinear(jbeamData.gaugeSmoothing or 10)
	M.oiltempDisplaySmoother = newTemporalSmoothingNonLinear(jbeamData.gaugeSmoothing or 10)
	M.batterySmoother = newTemporalSmoothing(jbeamData.batterySmoothing or 2)
	M.kwSmoother = newTemporalSmoothingNonLinear(jbeamData.kwSmoothing or 10)

	M.fuelSum = 0
	M.distanceSum = 0
	M.averageFuelConsumption = 0
	M.timeSinceFuelEconomyUpdate = 0
	M.isInStartupAnim = false
	M.currentAnimTime = 0

	M.batteryDisplay = 0
	M.kwDisplay = 0

	M.lastFuel = 0
	M.lastPower = false
	M.lastMaxKw = 0
	M.lastEcoKw = 0
	M.lastRegenKw = 0
end

local function reset()
	init(M.jbeamData)
end

M.init = init
M.reset = reset
M.updateGFX = updateGFX

M.setupGaugeData = setupGaugeData
M.updateGaugeData = updateGaugeData

return M
