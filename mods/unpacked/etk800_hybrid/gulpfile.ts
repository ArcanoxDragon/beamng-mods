import { promises as fs } from "fs";
import { task, src, dest, parallel } from "gulp";

import path from "path";
import zip from "gulp-zip";

const PublishConfigFile = ".publishfiles";

async function parsePublishFileList() {
    let publishFiles = await fs.readFile( path.join( __dirname, PublishConfigFile ), { encoding: "utf8" } );
    let lines = publishFiles.split( /[\r\n]+/ );
    let globs = lines.map( line => line.endsWith( "/" ) ? `${line}**/*` : line );

    return globs;
}

task( "publish", async () => {
    let globs = await parsePublishFileList();
    let zipName = path.basename( path.resolve( __dirname ) );

    return src( globs, { base: __dirname } )
        .pipe( zip( `${zipName}.zip` ) )
        .pipe( dest( __dirname ) );
} );

task( "default", parallel( "publish" ) );