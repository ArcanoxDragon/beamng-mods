local M = {}
local logTag = "autonomousManager"

local function triggerVehicleEvent(vehicleID, eventName, eventData)
    eventData = eventData or ""
    
    local vehicle = be:getObjectByID(vehicleID)
    
    if vehicle then
        vehicle:queueLuaCommand("controller.onGameplayEvent('" .. eventName .. "', " .. serialize(eventData) .. ")")
    else
        log("E", logTag, "The vehicle ID \"" .. vehicleID .. "\" is invalid")
    end
end

local function onInit()
    log("I", logTag, "Autonomous extension initialized")
end

local function onBeamNGTrigger(data)
    if not data.type then return end
    
    local command = nil
    
    if data.type == "speedlimit" then
        command = "apm_changeSpeedLimit"
    elseif data.type == "guides" then
        command = "apm_extendRetractGuides"
    elseif data.type == "switch" then
        command = "apm_switch"
    end
    
    if command then
        triggerVehicleEvent(data.subjectID, command, data)
    end
end

M.onInit = onInit
M.onBeamNGTrigger = onBeamNGTrigger

return M