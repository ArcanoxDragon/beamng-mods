{
    "apm400_powertrain": {
        "information": {
            "name": "APM400 Powertrain",
            "author": "Arcanox"
        },
        "slotType": "apm400_powertrain",
        "slots": [
            ["type", "default", "description"],
            ["apm400_differential_F", "apm400_differential_F", "Front Differential", {"coreSlot": true}],
            ["apm400_differential_R", "apm400_differential_R", "Rear Differential", {"coreSlot": true}],
            ["apm400_transfercase", "apm400_transfercase", "Transfer Case", {"coreSlot": true}],
            ["apm400_motor", "apm400_motor", "Motor", {"coreSlot": true}],
            ["apm400_battery", "apm400_battery", "Battery", {"coreSlot": true}],
        ],
        "powertrain" : [
            ["type", "name", "inputName", "inputIndex"],
            //front-front left axle
            ["shaft", "wheelaxle_FFL", "differential_FF", 1, {"connectedWheel":"FFL", "uiName":"Front-Front Left Axle", "friction":10}],
            //front-front right axle
            ["shaft", "wheelaxle_FFR", "differential_FF", 2, {"connectedWheel":"FFR", "uiName":"Front-Front Right Axle", "friction":10}],
            //front-rear left axle
            ["shaft", "wheelaxle_FRL", "differential_FR", 1, {"connectedWheel":"FRL", "uiName":"Front-Rear Left Axle", "friction":10}],
            //front-rear right axle
            ["shaft", "wheelaxle_FRR", "differential_FR", 2, {"connectedWheel":"FRR", "uiName":"Front-Rear Right Axle", "friction":10}],
            //rear-front left axle
            ["shaft", "wheelaxle_RFL", "differential_RF", 1, {"connectedWheel":"RFL", "uiName":"Rear-Front Left Axle", "friction":10}],
            //rear-front right axle
            ["shaft", "wheelaxle_RFR", "differential_RF", 2, {"connectedWheel":"RFR", "uiName":"Rear-Front Right Axle", "friction":10}],
            //rear-rear left axle
            ["shaft", "wheelaxle_RRL", "differential_RR", 1, {"connectedWheel":"RRL", "uiName":"Rear-Rear Left Axle", "friction":10}],
            //rear-rear right axle
            ["shaft", "wheelaxle_RRR", "differential_RR", 2, {"connectedWheel":"RRR", "uiName":"Rear-Rear Right Axle", "friction":10}],
        ],
    },
    "apm400_differential_F": {
        "information": {
            "name": "APM400 Front Differential",
            "author": "Arcanox"
        },
        "slotType": "apm400_differential_F",
        "powertrain": [
            ["type", "name", "inputName", "inputIndex"],
            ["differential", "differential_F", "transfercase", 2, {"diffType":"viscous", "viscousCoef":5, "viscousTorque":200, "diffTorqueSplit":0.5, "friction":5, "uiName":"Front Differential","defaultVirtualInertia":0.1}],
            ["differential", "differential_FF", "differential_F", 1, {"uiName": "Front-Front Differential"}],
            ["differential", "differential_FR", "differential_F", 2, {"uiName": "Front-Rear Differential"}],
        ],
    },
    "apm400_differential_R": {
        "information": {
            "name": "APM400 Rear Differential",
            "author": "Arcanox"
        },
        "slotType": "apm400_differential_R",
        "powertrain": [
            ["type", "name", "inputName", "inputIndex"],
            ["differential", "differential_R", "transfercase", 1, {"diffType":"viscous", "viscousCoef":5, "viscousTorque":200, "diffTorqueSplit":0.5, "friction":5, "uiName":"Rear Differential","defaultVirtualInertia":0.1}],
            ["differential", "differential_RF", "differential_R", 1, {"uiName": "Rear-Front Differential"}],
            ["differential", "differential_RR", "differential_R", 2, {"uiName": "Rear-Rear Differential"}],
        ],
    },
    "apm400_transfercase": {
        "information": {
            "name": "APM400 Transfer Case",
            "author": "Arcanox"
        },
        "slotType": "apm400_transfercase",
        "powertrain": [
            ["type", "name", "inputName", "inputIndex"],
            ["differential", "transfercase", "mainMotor", 1, {"gearRatio": 4.5, "diffType":"viscous", "viscousCoef":5, "viscousTorque":200, "diffTorqueSplit":0.5, "friction":5, "uiName":"Center Differential","defaultVirtualInertia":0.1}],
        ]
    },
    "apm400_motor": {
        "information": {
            "name": "APM400 Motor",
            "author": "Arcanox"
        },
        "slotType": "apm400_motor",
        "powertrain": [
            ["type", "name", "inputName", "inputIndex"],
            ["inductionMotor", "mainMotor", "dummy", 1],
        ],
        "mainMotor": {
            "torque":[
                ["rpm", "torque"],
                [0, 6000],
                [2000, 9200],
                [4800, 4000],
                [5500, 0],
            ],
            "maxRPM": 5000,
            "inertia": 1.4,
            "friction": 5,
            "electricalEfficiency": 0.98,
            "dynamicFriction": 0.0239,
            "particulates": 0,
            "energyStorage": "mainBattery",
            "requiredEnergyType": "electricEnergy",
            "thermalsEnabled": false,
            "headGasketDamageThreshold":1500000,
            "pistonRingDamageThreshold":1500000,
            "connectingRodDamageThreshold":2000000,
            "soundConfig": "soundConfig",
            "torqueReactionNodes": ["chl272", "chl176", "chr282"]
        },
        "soundConfig": {
            "sampleName": null,
        },
        "vehicleController": {
            "shiftLogicName": "electricMotor",
            "automaticModes": "RND",
            "defaultAutomaticMode": "N",
        }
    },
    "apm400_battery": {
        "information": {
            "name": "2000 kWh Battery",
            "author": "Arcanox"
        },
        "slotType": "apm400_battery",
        "variables": [
            ["name", "type", "unit", "category", "default", "min", "max", "title", "description"],
            ["$charge", "range", "%", "Chassis", 100, 0, 100, "Battery Charge", "Initial state-of-charge of traction battery", {"stepDis":0.5}]
        ],
        "energyStorage": [
            ["type", "name"],
            ["electricBattery", "mainBattery"]
        ],
        "mainBattery": {
            "energyType": "electricity",
            "batteryCapacity": "2000", // kWh
            "startingFuelCapacity": "$=$charge/100*2000",
            "fuel": {"[engineGroup]:": ["fuel"]},
        }
    },
}