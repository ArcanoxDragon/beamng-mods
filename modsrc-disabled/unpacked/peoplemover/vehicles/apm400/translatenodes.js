const fs = require( "fs" );
const stringify = require( "json-stringify-pretty-compact" );
const jsonc = require( "jsonc-parser" );
const path = require( "path" );

if ( process.argv.length < 6 ) {
    console.error( "Usage: translatenodes <path to .jbeam> <x y z>" );
    process.exit( 1 );
}

let [ _, __, jbeamPath, sX, sY, sZ ] = process.argv;
let [ x, y, z ] = [ +sX, +sY, +sZ ];
let jbeamFullPath = path.join( __dirname, jbeamPath )
let jbeamText = fs.readFileSync( jbeamFullPath, { encoding: "utf8" } );
let jbeam = jsonc.parse( jbeamText );

if ( isNaN( x ) || isNaN( y ) || isNaN( z ) ) {
    console.error( "Invalid coordinates" );
    process.exit( 1 );
}

for ( let partName of Object.keys( jbeam ) ) {
    let part = jbeam[ partName ];

    console.log( `Processing part "${partName}"...` );

    // Process nodes
    part.nodes = part.nodes.map( node => {
        if ( !Array.isArray( node ) || node.length < 4 )
            return node;

        if ( typeof node[ 0 ] !== "string" || typeof node[ 1 ] !== "number" || typeof node[ 2 ] !== "number" || typeof node[ 3 ] !== "number" )
            return node;

        let [ name, nx, ny, nz, ...rest ] = node;

        nx += x;
        ny += y;
        nz += z;
        
        // Fix rounding errors
        nx = Math.floor( nx * 1000 ) / 1000;
        ny = Math.floor( ny * 1000 ) / 1000;
        nz = Math.floor( nz * 1000 ) / 1000;

        return [ name, nx, ny, nz, ...rest ];
    } );
}

fs.writeFileSync( jbeamFullPath, stringify( jbeam, { indent: "\t", margins: true } ) );