const fs = require( "fs" );
const stringify = require( "json-stringify-pretty-compact" );
const jsonc = require( "jsonc-parser" );
const path = require( "path" );

function areFuzzyEqual( arr1, arr2 ) {
    return arr1.every( e => arr2.includes( e ) );
}

function alreadyExists( array, child ) {
    return array.some( e => areFuzzyEqual( e, child ) );
}

function makeFilter( preCheck ) {
    let seen = [];

    return item => {
        if ( typeof preCheck === "function" && !preCheck( item ) )
            return true;

        if ( alreadyExists( seen, item ) )
            return false;

        seen.push( item );
        return true;
    };
}

if ( process.argv.length < 3 ) {
    console.error( "Usage: removedupes <path to .jbeam>" );
}

let [ _, __, jbeamPath ] = process.argv;
let jbeamFullPath = path.join( __dirname, jbeamPath )
let jbeamText = fs.readFileSync( jbeamFullPath, { encoding: "utf8" } );
let jbeam = jsonc.parse( jbeamText );

for ( let partName of Object.keys( jbeam ) ) {
    let part = jbeam[ partName ];

    console.log( `Processing part "${partName}"...` );

    // Process beams
    part.beams = part.beams.filter( makeFilter( item => Array.isArray( item ) ) );
}

fs.writeFileSync( jbeamFullPath, stringify( jbeam, { indent: "\t", margins: true, maxLength: 140 } ) );