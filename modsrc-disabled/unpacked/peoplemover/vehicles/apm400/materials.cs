singleton Material(apm400_glass) {
    mapTo = "apm400_glass";
    reflectivityMap[0] = "vehicles/common/glass_base.dds";
    diffuseMap[0] = "vehicles/apm400/textures/GlassD.png";
    opacityMap[0] = "vehicles/apm400/textures/GlassD.png";
    diffuseMap[1] = "vehicles/apm400/textures/GlassDA.png";
    specularMap[0] = "vehicles/common/null.dds";
    normalMap[0] = "vehicles/common/null_n.dds";
    diffuseColor[1] = "0.5 0.5 0.5 0.75";
    specularPower[0] = "128";
    pixelSpecular[0] = "1";
    diffuseColor[0] = "1 1.5 1.5 1";
    useAnisotropic[0] = "1";
    castShadows = "0";
    translucent = "1";
    alphaTest = "0";
    alphaRef = "0";
    dynamicCubemap = true;
    materialTag0 = "beamng"; 
    materialTag1 = "vehicle";
};

singleton Material(apm400_ceil)
{
   mapTo = "apm400_ceil";
   diffuseMap[0] = "vehicles/apm400/textures/CeilD.png";
   specularPower[0] = "15";
   useAnisotropic[0] = "1";
   castShadows = "1";
   translucent = "0";
   alphaTest = "0";
   alphaRef = "0";
};

singleton Material(apm400_floor)
{
   mapTo = "apm400_floor";
   diffuseMap[0] = "vehicles/apm400/textures/FloorD.png";
   specularPower[0] = "15";
   useAnisotropic[0] = "1";
   castShadows = "1";
   translucent = "0";
   alphaTest = "0";
   alphaRef = "0";
};

singleton Material(apm400_body)
{
   mapTo = "apm400_body";
   diffuseMap[0] = "vehicles/apm400/textures/BodyD.png";
   specularPower[0] = "15";
   useAnisotropic[0] = "1";
   castShadows = "1";
   translucent = "0";
   alphaTest = "0";
   alphaRef = "0";
};

singleton Material(apm400_grabpole)
{
   mapTo = "apm400_grabpole";
   diffuseMap[0] = "vehicles/apm400/textures/GrabpoleD.png";
   specularPower[0] = "15";
   useAnisotropic[0] = "1";
   castShadows = "1";
   translucent = "0";
   alphaTest = "0";
   alphaRef = "0";
};

singleton Material(apm400_bogie_main)
{
   mapTo = "apm400_bogie_main";
   diffuseMap[0] = "vehicles/apm400/textures/BogieD.png";
   specularPower[0] = "15";
   useAnisotropic[0] = "1";
   castShadows = "1";
   translucent = "0";
   alphaTest = "0";
   alphaRef = "0";
};

singleton Material(apm400_bogie_support)
{
   mapTo = "apm400_bogie_support";
   diffuseMap[0] = "vehicles/apm400/textures/BogieSupportD.png";
   specularPower[0] = "15";
   useAnisotropic[0] = "1";
   castShadows = "1";
   translucent = "0";
   alphaTest = "0";
   alphaRef = "0";
};