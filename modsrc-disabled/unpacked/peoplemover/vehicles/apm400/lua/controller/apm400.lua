local M = {}

M.type = "auxiliary"
M.relevantDevice = nil
M.defaultOrder = 1000

-- Constants
local constants = {
    -- Conversion rates
    mpsToMph = 2.23694,
    -- Defaults
    defaultSpeedLimit = 10,
}
local modes = {
    init = "init",
    drive = "drive",
    disabled = "disabled",
}
local updateFunctions = {}

-- Required modules
local debugutil = require("arcanox/debugutil")("apm", true)
local mathutil = require("arcanox/mathutil")
local pid = require("arcanox/pid")

-- Shortcut functions
local abs = math.abs
local max = math.max
local min = math.min
local clamp = mathutil.clamp
local maprange = mathutil.maprange

-- State variables
local lastState = {}
local state = {
    mode = modes.disabled,
    enabled = false,
    curSpeed = 0,
    speedLimit = 10,
    targetSpeed = 0,
    throttle = 0,
    brake = 1,
    parkingbrake = 0,
    switching = 0,
    guides_retract = 0,
}

local function updateValues(dt)
    tableMerge(lastState, state)
    
    state.enabled = (electrics.values.autonomous or 0) > 0.5
    state.curSpeed = electrics.values.wheelspeed * constants.mpsToMph
    state.switching = electrics.values.switching or 0
    state.guides_retract = electrics.values.guides_retract or 0
    
    if state.enabled ~= lastState.enabled then
        gui.message({txt = "Autonomous mode " .. (state.enabled and "enabled" or "disabled")}, 2, "vehicle.apm.enabled")
        
        state.throttle = 0
        state.brake = 1
        state.targetSpeed = 0
        state.parkingbrake = 0
        
        M.throttleSmoother:reset()
        M.throttlePid:reset()
        
        if state.enabled then
            state.speedLimit = constants.defaultSpeedLimit
            state.mode = modes.init
        else
            state.mode = modes.disabled
        end
    end
    
    if state.guides_retract > 0.5 and lastState.guides_retract <= 0.5 then
        gui.message({txt = "Switch guides retracted"}, 2, "vehicle.apm.guides")
    elseif state.guides_retract < 0.5 and lastState.guides_retract >= 0.5 then
        gui.message({txt = "Switch guides extended"}, 2, "vehicle.apm.guides")
    else
        -- Only display switch direction messages if guides are not being extended/retracted
        
        if state.switching < -0.5 and lastState.switching >= -0.5 then
            gui.message({txt = "Switch changed to left path"}, 2, "vehicle.apm.switch")
        elseif state.switching > 0.5 and lastState.switching <= 0.5 then
            gui.message({txt = "Switch changed to right path"}, 2, "vehicle.apm.switch")
        end
    end
end

local function updateGUI(dt)
    debugutil.putValue("Autonomous Enabled", state.enabled and "Yes" or "No")
    debugutil.putValue("Mode", state.mode)
    debugutil.putValue("Speed Limit", state.speedLimit)
    debugutil.putValue("Target Speed", state.targetSpeed)
    debugutil.putValue("Auto Throttle", state.throttle)
    debugutil.putValue("Auto Brake", state.brake)
    
    debugutil.sendStreams()
end

local function onGameplayEvent(eventName, eventData)
    if eventName == "apm_changeSpeedLimit" then
        state.speedLimit = tonumber(eventData.speed)
    elseif eventName == "apm_extendRetractGuides" then
        electrics.values.guides_retract = eventData.action == "retract" and 1 or 0
    elseif eventName == "apm_switch" then
        local switchDirection = eventData.direction == "left" and -1 or 1
        
        if gear == "R" then switchDirection = -switchDirection end
        
        electrics.values.switching = switchDirection
    end
end

-- #region State Functions

local function driveTargetSpeed(dt)
    local rawThrottle = M.throttlePid:get(state.targetSpeed, state.curSpeed, dt)
    local smoothThrottle = M.throttleSmoother:get(rawThrottle, dt)
    
    state.throttle = maprange(smoothThrottle, 0, 1, 0, M.maxThrottle)
    state.brake    = maprange(smoothThrottle, 0, -1, 0, M.maxBrake)
end

local function initState(dt)
    state.mode = modes.drive
end

local function updateDrive(dt)
    state.targetSpeed = state.speedLimit
    
    driveTargetSpeed(dt)
end

-- #endregion State Functions

local function updateGFX(dt)
    updateValues()
    
    if state.enabled then
        if updateFunctions[state.mode] then
            updateFunctions[state.mode](dt)
        else
            log("E", "autonomous", "Unknown state mode: " .. state.mode)
        end
        
        if electrics.values.gear == "N" then
            electrics.values.autonomous = 0
            
            gui.message({txt = "Autonomous mode not available in N gear"}, 2, "vehicle.apm.forcedisabled")
        end
        
        input.event("throttle",     state.throttle, FILTER_DIRECT)
        input.event("brake",        state.brake, FILTER_DIRECT)
        input.event("parkingbrake", state.parkingbrake, FILTER_DIRECT)
    elseif lastState.enabled then
        -- Last tick before disabling
        
        input.event("throttle", 0, FILTER_DIRECT)
        input.event("brake",    1, FILTER_DIRECT)
    end
    
    updateGUI(dt) 
end

local function reset()
    -- Reload GE extension
    obj:queueGameEngineLua("extensions.reload('arcanox_autonomousManager')")
    
    electrics.values.autonomous = 0
    electrics.values.guides_retract = 1 -- retract
    
    updateValues(0)
end

local function initDebug()
    debugutil.putValue("Autonomous Enabled", "No")
    debugutil.putValue("Mode", "")
    debugutil.putValue("Speed Limit", 0)
    debugutil.putValue("Target Speed", 0)
    debugutil.putValue("Auto Throttle", 0)
    debugutil.putValue("Auto Brake", 0)
end

local function init(jbeamData)
    -- Load GE extension
    obj:queueGameEngineLua("extensions.load('arcanox_autonomousManager')")
    
    M.maxThrottle = jbeamData.maxThrottle or 1
    M.maxBrake = jbeamData.maxBrake or 1
    
    M.throttleSmoother = newTemporalSmoothingNonLinear(
        jbeamData.throttleSmoothingIn or 3.5,
        jbeamData.throttleSmoothingOut or 3.5
    )
    M.throttlePid = pid.newPid(
        jbeamData.throttlePidP or 1/5,
        jbeamData.throttlePidI or 1/30,
        jbeamData.throttlePidD or 1/400
    ):clampOutput(-1, 1)
    
    updateFunctions = {
        init = initState,
        drive = updateDrive,
        disabled = nop,
    }
    
    initDebug()
end

local function initSecondStage()
    -- initialize electrics
    electrics.values.guides_retract = 1 -- retract
end

M.updateGFX = updateGFX
M.reset = reset
M.init = init
M.initSecondStage = initSecondStage
M.onGameplayEvent = onGameplayEvent

return M