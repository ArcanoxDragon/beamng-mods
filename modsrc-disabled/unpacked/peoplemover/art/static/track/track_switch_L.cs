function trackSwitchLDae::onLoad(%this)
{
   %this.addSequence("ambient", "straight", "0", "60");
   %this.setSequenceCyclic("straight", "0");
   %this.addSequence("ambient", "side", "61", "120");
   %this.setSequenceCyclic("side", "0");
   %this.setSequenceCyclic("ambient", "0");
}

singleton TSShapeConstructor(trackSwitchLDae)
{
   baseShape = "./track_switch_L.dae";
};
