local M = {}

M.type = "auxiliary"
M.relevantDevice = nil
M.defaultOrder = 1000

local logFile
local runningTimer = 0
local timeSinceLastLog = 0

-- Helper Functions

local function logLine(...)
    local args = {...}

    for k,v in ipairs(args) do
        logFile:write(tostring(v))
    
        if k ~= #args then
            logFile:write(",")    
        end
    end
    
    logFile:write("\r\n")
end

local function initLogFile()
    if logFile then
        logFile:close()
        logFile = nil
    end
    
    local timestamp = os.date("%Y%m%d_%H%M%S")
    local filename = M.logFileName .. "_" .. timestamp .. ".csv"
    
    logFile =  io.open(filename, "w")
    runningTimer = 0
    
    -- Write header:
    logLine("time", "speed", "throttle", "brake", "parkingbrake", "clutch", "steering")
end

-- BeamNG Controller Functions

function M.init(jbeamData)
    M.logFileName = jbeamData.logFileName or "datalog"
    M.minimumLoggingInterval = jbeamData.minimumLoggingInterval or 0.25 -- 4 times per second by default
end

function M.initSecondStage()
    initLogFile()
end

function M.reset()
    initLogFile()
end

function M.updateGFX(dt)
    timeSinceLastLog = timeSinceLastLog + dt
    runningTimer = runningTimer + dt
    
    if timeSinceLastLog >= M.minimumLoggingInterval then
        timeSinceLastLog = 0
        
        -- log a data line
        logLine(
            runningTimer,
            electrics.values.wheelspeed,
            input.throttle,
            input.brake,
            electrics.values.parkingbrake,
            input.clutch,
            input.steering
        )
    end
end

return M